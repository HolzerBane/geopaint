// WinGUI.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "WinGUI.h"
#include "NewTerrainDialog.h"

// This is an example of an exported variable
WINGUI_API int nWinGUI=0;

// This is an example of an exported function.
WINGUI_API int fnWinGUI(void)
{
	return 42;
}

// This is the constructor of a class that has been exported.
// see WinGUI.h for the class definition
CWinGUI::CWinGUI()
{
	return;
}

bool CWinGUI::NewTerrainDialog( const HINSTANCE hInst, const int nCmdShow, TerrainGeneratorStruct::Data& returnStruct  )
{
	return WinGUI::NewTerrainDialog::ShowDialog( hInst, nCmdShow, returnStruct );
}

