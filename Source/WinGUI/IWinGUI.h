#pragma once

#ifdef WINGUI_EXPORTS
#define WINGUI_API __declspec(dllexport)
#else
#define WINGUI_API __declspec(dllimport)
#endif

namespace TerrainGeneratorStruct
{
	struct Data;
}

class WINGUI_API IWinGUI
{
public:
	virtual bool NewTerrainDialog( const HINSTANCE hInst, const int nCmdShow, TerrainGeneratorStruct::Data& returnStruct  ) = 0;
	virtual ~IWinGUI(){ };
};
