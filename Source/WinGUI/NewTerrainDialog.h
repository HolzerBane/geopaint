#pragma once
#include <windows.h>
#include "TerrainGeneratorStruct.h"

#pragma comment(linker, \
  "\"/manifestdependency:type='Win32' "\
  "name='Microsoft.Windows.Common-Controls' "\
  "version='6.0.0.0' "\
  "processorArchitecture='*' "\
  "publicKeyToken='6595b64144ccf1df' "\
  "language='*'\"")

namespace WinGUI
{

class NewTerrainDialog
{
public:
	static bool ShowDialog( const HINSTANCE hInst, const int nCmdShow, TerrainGeneratorStruct::Data& returnStruct );
	static NewTerrainDialog& Instance();

	void onBrowsePressed( const unsigned browseId );
	void onOkayPressed();
	bool isOkPressed() const { return m_okPressed; }

private:
	NewTerrainDialog( );
	void fillReturndata( TerrainGeneratorStruct::Data& data );
	void initControls();

	bool m_okPressed;
	HWND hDlg;
};

}