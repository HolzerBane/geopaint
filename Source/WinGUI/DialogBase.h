#pragma once
#include <windows.h>
#include "NewTerrainReturnStruct.h"

#pragma comment(linker, \
  "\"/manifestdependency:type='Win32' "\
  "name='Microsoft.Windows.Common-Controls' "\
  "version='6.0.0.0' "\
  "processorArchitecture='*' "\
  "publicKeyToken='6595b64144ccf1df' "\
  "language='*'\"")

namespace WinGUI
{

class DialogBase
{
public:
	static bool ShowDialog( const HINSTANCE hInst, const int nCmdShow, NewTerrainReturn::Data& returnStruct );
	static DialogBase& Instance();

	void onBrowsePressed( const unsigned browseId );
	void onOkayPressed();
	bool isOkPressed() const { return m_okPressed; }

private:
	DialogBase( );
	void fillReturndata( NewTerrainReturn::Data& data );
	void initControls();

	bool m_okPressed;
	HWND hDlg;
};

}