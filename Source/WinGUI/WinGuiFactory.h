#pragma once

#ifdef WINGUI_EXPORTS
#define WINGUI_API __declspec(dllexport)
#else
#define WINGUI_API __declspec(dllimport)
#endif

class IWinGUI;

class WINGUI_API WinGuiFactory
{
public:
	static IWinGUI* CreateWinGUI();
};

