// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the WINGUI_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// WINGUI_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
/*
#ifdef WINGUI_EXPORTS
#define WINGUI_API __declspec(dllexport)
#else
#define WINGUI_API __declspec(dllimport)
#endif
*/
#pragma comment(lib, "ComCtl32.lib")
#pragma warning (error: 4715)

#include "IWinGUI.h"

namespace TerrainGeneratorStruct
{
	struct Data;
}
// This class is exported from the WinGUI.dll
class /*WINGUI_API*/ CWinGUI : public IWinGUI {
public:
	CWinGUI(void);
	bool NewTerrainDialog( const HINSTANCE hInst, const int nCmdShow, TerrainGeneratorStruct::Data& returnStruct );
	// TODO: add your methods here.
};

extern WINGUI_API int nWinGUI;

WINGUI_API int fnWinGUI(void);
