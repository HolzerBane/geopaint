#include "stdafx.h"
#include <CommCtrl.h>
#include <CommDlg.h>
#include <stdlib.h>

#include "DialogBase.h"
#include "resource.h"
#include "DialogUtils.h"

using namespace WinGUI;

INT_PTR CALLBACK DialogProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
  switch(uMsg)
  {
  case WM_COMMAND:
    switch(LOWORD(wParam))
    {

	case IDC_BUTTON_BROWSE1:
	case IDC_BUTTON_BROWSE2:
	case IDC_BUTTON_BROWSE3:
	case IDC_BUTTON_BROWSE4:
		{
			DialogBase::Instance().onBrowsePressed( wParam - IDC_BUTTON_BROWSE1 );
		}
		break;

	case IDOK:
		DialogBase::Instance().onOkayPressed();
		return TRUE;
    case IDCANCEL:
      SendMessage(hDlg, WM_CLOSE, 0, 0);
      return TRUE;
    }
    break;

  case WM_CLOSE:
    if(MessageBox(hDlg, TEXT("Cancel changes? Really?"), TEXT("Close"),
      MB_ICONQUESTION | MB_YESNO) == IDYES)
    {
      DestroyWindow(hDlg);
    }
    return TRUE;

  case WM_DESTROY:
    PostQuitMessage(0);
    return TRUE;
  }

  return FALSE;
}

DialogBase::DialogBase( )
{

}

bool DialogBase::ShowDialog( const HINSTANCE hInst, const int nCmdShow, NewTerrainReturn::Data& returnStruct )
{
	InitCommonControls();
	// does registerclass help this?
	Instance().hDlg = CreateDialogParam(hInst, MAKEINTRESOURCE(IDD_DIALOG1), 0, DialogProc, 0);
	DWORD error = ::GetLastError();
	if ( error )
	{
		MessageBox(Instance().hDlg, TEXT("Could not load dialog"), TEXT("Error"),
			MB_ICONERROR | MB_OK);

		return false;
	}

	Instance().initControls();

	MSG msg;
	BOOL ret;

	ShowWindow(Instance().hDlg, nCmdShow);

	while((ret = GetMessage(&msg, 0, 0, 0)) != 0) {
		if(ret == -1)
		{
			return false;
		}
			
		if ( Instance().isOkPressed())
		{
			Instance().fillReturndata(returnStruct);
			DestroyWindow(Instance().hDlg);
			return true;
		}

		if(!IsDialogMessage(DialogBase::Instance().hDlg, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return false;
}
DialogBase& DialogBase::Instance()
{
	static DialogBase dialog;
	return dialog;
}

void DialogBase::onBrowsePressed( const unsigned browseId )
{
	OPENFILENAME ofn={0};
	wchar_t szFileName[MAX_PATH]={0};
	ofn.lStructSize=sizeof(OPENFILENAME);
	ofn.Flags=OFN_ALLOWMULTISELECT|OFN_EXPLORER;
	ofn.lpstrFilter=L"BMP Files (*.BMP)\0*.BMP\0";
	ofn.lpstrFile=szFileName;
	ofn.nMaxFile=MAX_PATH;

	if (::GetOpenFileName(&ofn))
	{
		::SetWindowTextW(::GetDlgItem(hDlg, IDC_EDIT_LAYER1 + browseId ), ofn.lpstrFile );
	}
}

void DialogBase::onOkayPressed()
{
	m_okPressed = true;
}

void DialogBase::fillReturndata( NewTerrainReturn::Data& data )
{
	data.layerCount = 4;
	data.layers = new NewTerrainReturn::Layer[data.layerCount];

	// assume 4 lines for now
	unsigned heightBoxIndex = IDC_EDIT_HEIGHT_R1;
	for ( int index = 0; index < 4; ++index )
	{
		NewTerrainReturn::Layer& layer = data.layers[index];

		DialogUtils::getString( hDlg, IDC_EDIT_LAYER1 + index, layer.filePath, NewTerrainReturn::Layer::max_path );

		for ( int heightIndex = 0; heightIndex < 4; ++heightIndex )
		{
			DialogUtils::getInt( hDlg, heightBoxIndex++, layer.heightContribution[heightIndex] );
		}
	}

	// get the dimensions
	DialogUtils::getInt( hDlg, IDC_EDIT_HEIGHT, data.height );
	DialogUtils::getInt( hDlg, IDC_EDIT_WIDTH,  data.width );
	DialogUtils::getInt( hDlg, IDC_EDIT_LENGTH, data.length );
}

void DialogBase::initControls()
{
	for ( int index = 0; index < 16; ++index )
	{
		DialogUtils::setInt( hDlg, IDC_EDIT_HEIGHT_R1 + index, 0 );
	}
}
