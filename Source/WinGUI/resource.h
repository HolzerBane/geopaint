//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by WinGUI.rc
//
#define IDD_NEW_TERRAIN                 9
#define IDD_DIALOG1                     101
#define IDC_EDIT_LAYER1                 1001
#define IDC_EDIT_LAYER2                 1002
#define IDC_EDIT_LAYER3                 1003
#define IDC_EDIT_LAYER4                 1004
#define IDC_BUTTON_BROWSE1              1005
#define IDC_BUTTON_BROWSE2              1006
#define IDC_BUTTON_BROWSE3              1007
#define IDC_BUTTON_BROWSE4              1008
#define IDC_EDIT_WIDTH                  1009
#define IDC_EDIT_HEIGHT                 1010
#define IDC_EDIT_LENGTH                 1011
#define IDC_EDIT_LAYER5                 1012
#define IDC_EDIT_LAYER6                 1013
#define IDC_EDIT_LAYER7                 1014
#define IDC_EDIT_LAYER8                 1015
#define IDC_BUTTON_BROWSE5              1016
#define IDC_BUTTON_BROWSE6              1017
#define IDC_BUTTON_BROWSE7              1018
#define IDC_BUTTON_BROWSE8              1019
#define IDC_BUTTON1                     1023
#define IDC_EDIT_HEIGHT_R1              1024
#define IDC_EDIT_HEIGHT_G1              1025
#define IDC_EDIT_HEIGHT_B1              1026
#define IDC_EDIT_HEIGHT_A1              1027
#define IDC_EDIT_HEIGHT_R2              1028
#define IDC_EDIT_HEIGHT_G2              1029
#define IDC_EDIT_HEIGHT_B2              1030
#define IDC_EDIT_HEIGHT_A2              1031
#define IDC_EDIT_HEIGHT_R3              1032
#define IDC_EDIT_HEIGHT_G3              1033
#define IDC_EDIT_HEIGHT_B3              1034
#define IDC_EDIT_HEIGHT_A3              1035
#define IDC_EDIT_HEIGHT_R4              1036
#define IDC_EDIT_HEIGHT_G4              1037
#define IDC_EDIT_HEIGHT_B4              1038
#define IDC_EDIT_HEIGHT_A4              1039
#define IDC_LIST1                       1040
#define IDC_EDIT_HEIGHT_L1              1041
#define IDC_EDIT_HEIGHT_L2              1042
#define IDC_EDIT_HEIGHT_L3              1043
#define IDC_EDIT_HEIGHT_L4              1044

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1042
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
