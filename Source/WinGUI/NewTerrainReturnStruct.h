#pragma once

#ifdef WINGUI_EXPORTS
#define WINGUI_API __declspec(dllexport)
#else
#define WINGUI_API __declspec(dllimport)
#endif

namespace NewTerrainReturn
{

struct WINGUI_API Layer
{
	static const size_t max_path = 255;

	wchar_t  filePath[max_path];
	unsigned heightContribution[4];
};

struct WINGUI_API Data
{
	Layer*	 layers;
	unsigned layerCount;

	unsigned width;
	unsigned length;
	unsigned height;
	 
 	Data() : layers(nullptr) { }
	~Data() { if(layers) delete[] layers; }

private:
 	Data( const Data& other) { }

};

}