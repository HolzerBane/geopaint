#pragma once
#include <stdlib.h>
#include <windows.h>

namespace DialogUtils
{
	void static getInt( const HWND dialogId, const unsigned controlId, unsigned& retVal )
	{
		HWND numBox = ::GetDlgItem(dialogId, controlId );
		LPSTR buffer = new CHAR[4];
		::GetWindowTextA( numBox, buffer, 8);
		retVal = atoi(buffer);
		delete buffer;
	}

	void static setInt( const HWND dialogId, const unsigned controlId, const unsigned val )
	{
		HWND numBox = ::GetDlgItem(dialogId, controlId );
		char* buffer = new char[4];
		_itoa(val, buffer, 10 );
		::SetWindowTextA( numBox, buffer );
		delete buffer;
	}

	void static getString( const HWND dialogId, const unsigned controlId, wchar_t* retVal, unsigned size )
	{
		HWND textBox = ::GetDlgItem(dialogId, controlId );
		::GetWindowTextW( textBox, retVal, size );
	}

	void static getString( const HWND dialogId, const unsigned controlId, char* retVal, unsigned size )
	{
		HWND textBox = ::GetDlgItem(dialogId, controlId );
		::GetWindowTextA( textBox, retVal, size );
	}
}