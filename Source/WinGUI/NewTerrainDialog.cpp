#include "stdafx.h"
#include <CommCtrl.h>
#include <CommDlg.h>
#include <stdlib.h>

#include "NewTerrainDialog.h"
#include "resource.h"
#include "DialogUtils.h"

using namespace WinGUI;

INT_PTR CALLBACK DialogProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
  switch(uMsg)
  {
  case WM_COMMAND:
    switch(LOWORD(wParam))
    {

	case IDC_BUTTON_BROWSE1:
	case IDC_BUTTON_BROWSE2:
	case IDC_BUTTON_BROWSE3:
	case IDC_BUTTON_BROWSE4:
		{
			NewTerrainDialog::Instance().onBrowsePressed( wParam - IDC_BUTTON_BROWSE1 );
		}
		break;

	case IDOK:
		NewTerrainDialog::Instance().onOkayPressed();
		return TRUE;
    case IDCANCEL:
      SendMessage(hDlg, WM_CLOSE, 0, 0);
      return TRUE;
    }
    break;

  case WM_CLOSE:
	  if( NewTerrainDialog::Instance().isOkPressed() || MessageBox(hDlg, TEXT("Cancel changes? Really?"), TEXT("Close"),
      MB_ICONQUESTION | MB_YESNO) == IDYES)
    {
      DestroyWindow(hDlg);
    }
    return TRUE;

  case WM_DESTROY:
    PostQuitMessage(0);
    return TRUE;
  }

  return FALSE;
}

NewTerrainDialog::NewTerrainDialog( )
{

}

bool NewTerrainDialog::ShowDialog( const HINSTANCE hInst, const int nCmdShow, TerrainGeneratorStruct::Data& returnStruct )
{
	InitCommonControls();
	// does registerclass help this?
	Instance().hDlg = CreateDialogParam(hInst, MAKEINTRESOURCE(IDD_NEW_TERRAIN), 0, DialogProc, 0);
	DWORD error = ::GetLastError();
	if ( error )
	{
		MessageBox(Instance().hDlg, TEXT("Could not load dialog"), TEXT("Error"),
			MB_ICONERROR | MB_OK);

		return false;
	}

	Instance().initControls();

	MSG msg;
	BOOL ret;

	::SetWindowPos( Instance().hDlg, HWND_TOP, 300, 100, 396, 206, 0 );
	ShowWindow(Instance().hDlg, nCmdShow);

	while((ret = GetMessage(&msg, 0, 0, 0)) != 0) {
		if(ret == -1)
		{
			return false;
		}

		if ( Instance().isOkPressed())
		{
			Instance().fillReturndata(returnStruct);
			//DestroyWindow(Instance().hDlg);
			SendMessage(Instance().hDlg, WM_CLOSE, 0, 0);
		}

		if(!IsDialogMessage(NewTerrainDialog::Instance().hDlg, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return Instance().isOkPressed();
}
NewTerrainDialog& NewTerrainDialog::Instance()
{
	static NewTerrainDialog dialog;
	return dialog;
}

void NewTerrainDialog::onBrowsePressed( const unsigned browseId )
{
	OPENFILENAME ofn={0};
	wchar_t szFileName[MAX_PATH]={0};
	ofn.lStructSize=sizeof(OPENFILENAME);
	ofn.Flags=OFN_ALLOWMULTISELECT|OFN_EXPLORER;
	ofn.lpstrFilter=L"BMP Files (*.BMP)\0*.BMP\0";
	ofn.lpstrFile=szFileName;
	ofn.nMaxFile=MAX_PATH;

	if (::GetOpenFileName(&ofn))
	{
		::SetWindowTextW(::GetDlgItem(hDlg, IDC_EDIT_LAYER1 + browseId ), ofn.lpstrFile );
	}
}

void NewTerrainDialog::onOkayPressed()
{
	m_okPressed = true;
}

void NewTerrainDialog::fillReturndata( TerrainGeneratorStruct::Data& data )
{
	data.layerCount = 4;
	data.layers = new TerrainGeneratorStruct::Layer[data.layerCount];

	// assume 4 lines for now
	unsigned heightBoxIndex = IDC_EDIT_HEIGHT_L1;
	for ( int index = 0; index < 4; ++index )
	{
		TerrainGeneratorStruct::Layer& layer = data.layers[index];

		DialogUtils::getString( hDlg, IDC_EDIT_LAYER1 + index, layer.texturePath, TerrainGeneratorStruct::Layer::max_path );
		DialogUtils::getString( hDlg, IDC_EDIT_LAYER5 + index, layer.texturePath, TerrainGeneratorStruct::Layer::max_path );

		DialogUtils::getInt( hDlg, heightBoxIndex + index, layer.heightContribution );
	}

	// get the dimensions
	DialogUtils::getInt( hDlg, IDC_EDIT_HEIGHT, data.height );
	DialogUtils::getInt( hDlg, IDC_EDIT_WIDTH,  data.width );
	DialogUtils::getInt( hDlg, IDC_EDIT_LENGTH, data.length );
}

void NewTerrainDialog::initControls()
{
	m_okPressed = false;
	for ( int index = 0; index < 4; ++index )
	{
		DialogUtils::setInt( hDlg, IDC_EDIT_HEIGHT_L1 + index, 0 );
	}
}
