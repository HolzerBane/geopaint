#pragma once

#ifdef WINGUI_EXPORTS
#define WINGUI_API __declspec(dllexport)
#else
#define WINGUI_API __declspec(dllimport)
#endif

namespace TerrainGeneratorStruct
{

struct WINGUI_API Layer
{
	static const size_t max_path = 255;

	wchar_t  texturePath[max_path];
	wchar_t  attributePath[max_path];
	unsigned heightContribution;
};

struct WINGUI_API Data
{
	Layer*	 layers;
	unsigned layerCount;

	unsigned width;
	unsigned length;
	unsigned height;
	 
 	Data() : layers(nullptr) { }
	~Data() { if(layers) delete[] layers; }

private:
 	Data( const Data& other) { }

};

}