#pragma once
#include <vector>
#include "AttributeLayer.h"
#include "AttributeTypes.h"

namespace AttributeTool
{

template < class TValue >
class AttributeMap
{
public:
	typedef AttributeLayer<TValue> AttributeLayer;
	typedef std::vector< AttributeLayer > LayerVector;	

	AttributeMap( unsigned int layerCount);
	~AttributeMap();

	LayerIndex addLayer( AttributeLayer&& );
	AttributeLayer& accessLayer( LayerIndex );
	AttributeLayer& operator[]( LayerIndex );
	const AttributeLayer& getLayer( LayerIndex ) const;
	void clear() { m_layerVector.clear(); }
	unsigned size() const { return m_layerVector.size(); }

private:
	LayerVector m_layerVector;

};


}