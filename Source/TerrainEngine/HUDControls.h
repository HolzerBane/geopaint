#pragma once

// these two in this order are necessary
#include <DXUT.h>
#include <DXUTgui.h>
#include <string>
#include "..\WinGUI\TerrainGeneratorStruct.h"
#include "Utility\Event.h"

namespace TerrainTools
{
	struct PaintBrush;
}

namespace HUD
{

class HUDControls 
{
public:
	enum ControlID
	{
		B_NewTerrain,
		B_SaveTerrain,
	
		CB_BrushLayer,
		S_BrushSize,
		S_BrushBias,
		s_BrushOpacity,
		CB_BrushEraser,
		CB_TerrainMode
	};

	Event<TerrainGeneratorStruct::Data&>	NewTerrainRequested;
	Event<TerrainGeneratorStruct::Data&>	SaveTerrainRequested;
	Event<const unsigned>			BrushLayerChanged;
	Event<const unsigned>			BrushSizeChanged;
	Event<const unsigned>			BrushBiasChanged;
	Event<const unsigned>			BrushOpacityChanged;
	Event<const bool>				BrushEraserChanged;

	
	static HUDControls& Instance();

	void initialize( CDXUTDialogResourceManager& manager, 
		const unsigned width, const unsigned height,  
		const unsigned posX, const unsigned posY
		);

	void initializeControls( const TerrainTools::PaintBrush& brush );

	bool processMessage( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
	void render( float dt );
	void setCallback( PCALLBACKDXUTGUIEVENT ); 
	void setBackGround( const D3DCOLOR& color );
	void addLayer( const unsigned id, const std::wstring& name );
	void clearLayers();

	void processGUIEvent( const int controlId, const int eventId );

private:
	HUDControls();
	HUDControls( const HUDControls& hudControls){ };

	void CALLBACK onGUIEvent( UINT nEvent, int nControlID, CDXUTControl* pControl, void* pUserContext );

	CDXUTDialog		m_dialog;

	// owned
	CDXUTComboBox*	m_brushLayer;
	CDXUTSlider*	m_brushSize;
	CDXUTSlider*	m_brushBias;
	CDXUTSlider*	m_brushOpacity;
	CDXUTCheckBox*  m_brushEraser;

	CDXUTButton*    m_newTerrain;
	CDXUTButton*    m_saveTerrain;
};

}