#pragma once

#include "ShaderBase.h"
#include "VertexType.h"
#include "Utility\EffectVariable.h"
#include "Utility\CDLOD\RegularGrid.h"

#include <fstream>
#include <vector>
#include <array>
#include <string>

#include <D3DX10.h>

class ModelClass;
struct ID3D10Device;
struct ID3D10Blob;
struct ID3D10EffectVariable;
struct ID3D10EffectTechnique;
struct ID3D10InputLayout;

namespace TerrainModel
{
	class MapModel;
}

class CDLODQuadTree;
class FreeCamera;

class TerrainShader : public Shader::ShaderBase
{
public:

	struct TextureIndexType
	{
		D3DXVECTOR4 index[7]; 
	};

	TerrainShader();
	TerrainShader(const TerrainShader&);
	~TerrainShader();


	void setInputParams(
		  const bool showNormals
		, const FreeCamera& camera 
		);

	void setCDLODParameters( const CDLODQuadTree& cdlodQuadTree, const RegularGrid<VertexType>& gridMesh );
	void setQuadRenderingParameters( const D3DXVECTOR4& offset,  const D3DXVECTOR4& scale );
	void setModelParameters( TerrainModel::MapModel& model);
	void bindEffectVars( const ModelClass& );

	void SetConstShaderParameters( );
	void SetPerFrameShaderParameters( );

protected:
		bool virtual initializeTechniques( ID3D10Device* device ) override;
		void virtual shutdownShader() override;
public:
//private:

	ID3D10EffectMatrixVariable* m_worldMatrix;
	ID3D10EffectMatrixVariable* m_viewMatrix;
	ID3D10EffectMatrixVariable* m_projMatrix;

	// CDLOD variables
	ID3D10EffectVariable*		m_morphLevels;
	ID3D10EffectVariable*		m_quadOffset;  
	ID3D10EffectVariable*		m_quadScale;   
	ID3D10EffectVariable*		m_quadWorldMax;

	// terrain variables
	ID3D10EffectVariable*		m_heightmapTextureInfo;	
	ID3D10EffectVariable*		m_terrainScale;				
	ID3D10EffectVariable*		m_terrainOffset;				
	ID3D10EffectVariable*		m_samplerWorldToTextureScale;						
	ID3D10EffectVariable*		m_gridDim;						
	ID3D10EffectVariable*		m_detailRatio;

	ID3D10EffectVariable*		m_cameraPos;			
	ID3D10EffectVariable*		m_renderwNormalMaps; 
	ID3D10EffectVariable*		m_viewDistance;

	static const unsigned ConstEffectVarCount = 7;
	std::array< Utils::IEffectVariable*, ConstEffectVarCount >	m_constEffectVars;
	static const unsigned PerFrameEffectVarCount = 8;
	std::array< Utils::IEffectVariable*, PerFrameEffectVarCount >	m_perFrameEffectVars;
};
