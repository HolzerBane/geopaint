#pragma once
#include <d3dx10.h>

////////////////////////////////////////////////////////////////////////////////
// Class name: ViewPointClass
////////////////////////////////////////////////////////////////////////////////
class ViewPoint 
{
public:
	ViewPoint();
	ViewPoint(const ViewPoint&);
	~ViewPoint();

	void SetPosition(float, float, float);
	void SetLookAt(float, float, float);
	void SetPosition(const D3DXVECTOR3& pos);
	void SetLookAt(const D3DXVECTOR3& look);

	void SetProjectionParameters(
		float fieldOfView, 
		float aspectRatio, 
		float nearPlane, 
		float farPlane);

	void GenerateViewMatrix();
	void GenerateProjectionMatrix();

	const D3DXMATRIX& GetViewMatrix() const;
	const D3DXMATRIX& GetProjectionMatrix() const;

private:
	D3DXVECTOR3 m_position, m_lookAt;
	D3DXMATRIX m_viewMatrix, m_projectionMatrix;
	float m_fieldOfView, m_aspectRatio, m_nearPlane, m_farPlane;
};