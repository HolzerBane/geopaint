#pragma once
#include <string>
#include "AttributeLayer.h"
#include "Utility\ImageConsts.h"

class ImageFile;


namespace AttributeTool
{

class AttributeFactory
{
public:

	static AttributeLayer<float> getFromImageChannel(const ImageFile& imagefile, const ImageConsts::Channel channel = ImageConsts::Channel::Red );
	static AttributeLayer<float> getFromImageChannel(const std::wstring& filePath, const ImageConsts::Channel channel = ImageConsts::Channel::Red );
};
}