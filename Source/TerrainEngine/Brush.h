#pragma once;

namespace TerrainTools
{

struct Brush
{
	unsigned	Size;
	float		Bias;
	float		Opacity;
	unsigned	Step;
};

}