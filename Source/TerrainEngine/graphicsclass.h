#pragma once

#include "d3dclass.h"
#include "cameraclass.h"
#include "Utility\FreeCamera.h"
#include "modelclass.h"
#include "NVUT\NVUTSkybox.h"
#include "ViewPoint.h"
#include "BitmapObject.h"

// GLOBALS //
const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = true;
const float SCREEN_DEPTH = 1000.0f;
const float SCREEN_NEAR = 0.1f;

// forward declarations
class TerrainShader;
class TextureShader;

////////////////////////////////////////////////////////////////////////////////
// Class name: GraphicsClass
////////////////////////////////////////////////////////////////////////////////
class GraphicsClass
{
public:
	GraphicsClass();
	GraphicsClass(const GraphicsClass&);
	~GraphicsClass();

	bool Initialize(int, int, HWND);
	void Shutdown();
	bool Frame( float dt );
	void onMouseMove( int mouseX, int mouseY );
	void onMouseClick( bool leftClick, bool rightClick);
	void onLeftMouseButtonUp();
	bool Render( float dt );
	bool testRenderHUD( float dt );
	bool isInitialized() const { return m_inited; }
	void toggleNormals() const { m_Model->toggleNormals(); }
	void toggleWireFrame() const { m_Model->toggleNormals(); }
	FreeCamera* getCamera() { return m_freeCamera;}
	void setFPS( const double fps );
	void drawHelpText() const;

private:
	bool			m_inited;
	bool			m_allowRender;

	D3DClass*		m_D3D;
	CameraClass*	m_Camera;
	FreeCamera*		m_freeCamera;
	ModelClass*		m_Model;
	TerrainShader*	m_AlphaMapShader;
	TextureShader*  m_textureShader;
	// input related
	BitmapObject	m_mousePointer;
	bool			m_beginCheck;

	// input parameters
	// mouse
	int				m_mouseX;
	int				m_mouseY;
	bool			m_mouseLeft;
	bool			m_mouseRight;
	bool			m_mouseLeftPrev;
	// switches
	class Switches
	{
	public:
		static bool	Wireframe;
		static bool	QuadDisplay;
		static bool HUDDisplay;
	};

	NVUTSkybox  m_skybox;

};
