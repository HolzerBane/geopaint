#include "DXUTHUDHelper.h"
#include "Utility\ReportError.h"
#include <D3D10.h>

DXUTHUDHelper::DXUTHUDHelper()
	: m_inited(false)
	, pTxtHelper(nullptr)
{ }


void DXUTHUDHelper::Init( ID3D10Device* device )
{
	HRESULT res = D3DX10CreateFont( device, 13, 0, FW_SEMIBOLD, 1, FALSE, DEFAULT_CHARSET,
      OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
      L"Verdana", &pFont );
	
	assert( res == S_OK );

	// huh?
	//res = D3DX10CreateSprite( device, 4096, &pTextSprite );
	//assert( res == S_OK );

	m_inited = true;
}

DXUTHUDHelper& DXUTHUDHelper::Instance()
{
	static DXUTHUDHelper instance;
	if ( instance.m_inited && instance.pTxtHelper == nullptr )
	{
		instance.pTxtHelper = new CDXUTTextHelper( instance.pFont, instance.pTextSprite, 15 );
		// well... clear the warning it generates
		ReportError::Instance().ClearMessages();
	}

	return instance;
}

DXUTHUDHelper::~DXUTHUDHelper()
{
	if ( Instance().pTxtHelper )
	{
		delete Instance().pTxtHelper;
		Instance().pTxtHelper = nullptr;
	}
}