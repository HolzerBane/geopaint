#pragma once

namespace TerrainTools
{

struct PaintBrush;

class PaintBrushHandler
{
public:
	PaintBrushHandler( );
	bool allowPaint( const TerrainTools::PaintBrush& brush, const unsigned x, const unsigned y);
	void resetPrevState();

private:
	unsigned	m_prevAllowedPaintX, m_prevAllowedPaintY;
	bool		m_allowPaint;

};

}