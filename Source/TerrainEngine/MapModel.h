#pragma once
#include "AttributeLayer.h" 
#include "AttributePainter.h" 
#include "AttributeTypes.h"
#include "AttributeMap.h" 
#include "DirectXTypes.h"
//#include <d3d10_1.h>
//#include <d3d10.h>
//#include <d3dx10.h>
#include "TextureObject.h"

namespace TerrainTools
{
	struct PaintBrush;
}

namespace TerrainModel
{

class TextureObject;

class MapModel
{
public:

	struct VertexType
	{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
	};

	MapModel( unsigned int x, unsigned int y );
	void setDevice( DX::DevicePtr d );

	/* TerrainGeneration */
	void generateRandom();
	void generatePerlinV2();
	void generateHeightMapFromLayers(  );

	// indexers
	void getListIndex( DX::IndexList& indices );

	int getIndexCount() const;
	int getVertexCount() const { return m_heightNormalLayer.count(); }
	const AttributeTool::AttributeLayer<D3DXVECTOR4>& getHeightNormalMap() const { return m_heightNormalLayer; }

	void getNBTOf( 
		const AttributeTool::Index x, const AttributeTool::Index y, 
		const float tex1u, const float tex1v, const float tex2u, const float tex2v,
		D3DXVECTOR3& n, D3DXVECTOR3& b, D3DXVECTOR3& t);

	D3DXVECTOR3 getNormalOf( const AttributeTool::Index x, const AttributeTool::Index y );

	D3DXVECTOR3 getVertexAt(const AttributeTool::Index x, const AttributeTool::Index y);

	void setTexturefactor( float tf ) { m_textureFactor = tf; }
	float getTexturefactor( ) const { return m_textureFactor; }

	unsigned addAttribute( AttributeTool::AttributeLayer< float >&&, const float heightContribution = 0.f );

	unsigned attributeCount() const { return m_attributeMap.size(); }
	unsigned attributeSizeX() const;
	unsigned attributeSizeY() const;

	void clearAttributes(); 

	bool mergeLayers( 	AttributeTool::AttributeLayer< D3DXVECTOR4 >& contribution, 
						AttributeTool::AttributeLayer< D3DXVECTOR4 >& indices );
	bool sendAttributesToDevice(  );

	ID3D10ShaderResourceView* getAttributeResourceView() const;
	ID3D10ShaderResourceView* getIndexResourceView() const;
	ID3D10ShaderResourceView* getHeightMapResourceView() const;
	ID3D10ShaderResourceView* getTangentMapResourceView() const;


	const D3DXVECTOR2& getSize() const { return m_mapSize; }
	void setSize( const D3DXVECTOR2& size ) { m_mapSize = size; }
	void setSize( const float x, const float y) { m_mapSize = D3DXVECTOR2(x, y); }

	void paintAttributeLayer( const TerrainTools::PaintBrush& paintBrush, const float x, const float y ); 
	void paintGeometryLayer( const TerrainTools::PaintBrush& paintBrush, const float x, const float y ); 
	
	//test function
	void	fuckUpTexture();

	// hide the copy constructor..

private:
	D3DXVECTOR2		m_mapSize;


	// stored for updating the texture on GPU
	AttributeTool::AttributePainter<D3DXVECTOR4>	m_brushPainter;
	AttributeTool::AttributePainter<D3DXVECTOR4>	m_brushGeometryPainter;
	AttributeTool::AttributeLayer<D3DXVECTOR4>		m_attributeLayer;

	AttributeTool::AttributeLayer<D3DXVECTOR4>	m_heightNormalLayer;
	AttributeTool::AttributeLayer<D3DXVECTOR3>	m_tangentLayer;
	AttributeTool::AttributeMap<float>			m_attributeMap;
	std::vector<float>							m_heightContribution;

	AttributeTool::AttributeLayer<float>		m_perlinLayer;

	TextureObject			m_attributeTexture;
	TextureObject			m_indexTexture;
	TextureObject			m_heightTexture;
	TextureObject			m_tangentTexture;
	float					m_textureFactor;

	DX::DevicePtr	m_device;
	DX::BufferPtr	m_vertexBuff;
	DX::BufferPtr	m_indexBuff;
};

}