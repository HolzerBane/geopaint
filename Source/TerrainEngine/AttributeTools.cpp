#include "AttributeTools.h"

using namespace AttributeTool;



AttributeTool::DirFunctionArray AttributeTool::ShiftFn::Shift =
{
	[]( const Coordinate2D& c) { return Coordinate2D( c.first,		c.second + 1	); },
	[]( const Coordinate2D& c) { return Coordinate2D( c.first + 1,	c.second + 1	); },
	[]( const Coordinate2D& c) { return Coordinate2D( c.first + 1,	c.second		); },
	[]( const Coordinate2D& c) { return Coordinate2D( c.first + 1,  c.second - 1	); },
	[]( const Coordinate2D& c) { return Coordinate2D( c.first,		c.second - 1	); },
	[]( const Coordinate2D& c) { return Coordinate2D( c.first - 1,	c.second - 1	); },
	[]( const Coordinate2D& c) { return Coordinate2D( c.first - 1,	c.second		); },
	[]( const Coordinate2D& c) { return Coordinate2D( c.first - 1,	c.second + 1	); }
};
