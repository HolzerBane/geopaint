#include "AttributeMap.h"
#include <assert.h>

using namespace AttributeTool;

//#################################################################################################
// param: layer count
template < class TValue >
AttributeMap< TValue >::AttributeMap( unsigned int s )
{
	m_layerVector.reserve( s );
}

//#################################################################################################
// param: layer count
template < class TValue >
AttributeMap< TValue >::~AttributeMap( )
{

}
//#################################################################################################

template < class TValue >
LayerIndex AttributeMap< TValue >::addLayer( AttributeLayer&& al )
{
	m_layerVector.push_back( al );
	return m_layerVector.size() - 1;
}
//#################################################################################################

template < class TValue >
AttributeLayer< TValue >& AttributeMap< TValue >::accessLayer( LayerIndex li )
{
	assert( li < m_layerVector.size() );
	return m_layerVector[li];
}

template < class TValue >
const AttributeLayer< TValue >& AttributeMap< TValue >::getLayer( LayerIndex li ) const
{
	assert( li < m_layerVector.size() );
	return m_layerVector[li];
}

//#################################################################################################

template < class TValue >
AttributeLayer< TValue >& AttributeMap< TValue >::operator[]( LayerIndex li )
{
	return accessLayer(li);
}

