#pragma once

#include "AttributeLayer.h"
#include "Utility\CDLOD\CDLODQuadTree.h"

class HeightMapAdapter : public IHeightmapSource
{
public:
	HeightMapAdapter( const AttributeTool::AttributeLayer<D3DXVECTOR4>& hm );

	int GetSizeX( );
	int GetSizeY( );

	// returned value is converted to height using following formula:
	// 'WorldHeight = WorldMinZ + GetHeightAt(,) * WorldSizeZ / 65535.0f'
	unsigned short  GetHeightAt( int x, int y );

	void GetAreaMinMaxZ( int x, int y, int sizeX, int sizeY, unsigned short & minZ, unsigned short & maxZ );

private:
	const AttributeTool::AttributeLayer<D3DXVECTOR4>&	m_heightMap;
};