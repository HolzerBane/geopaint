#pragma once
#include <d3d10_1.h>
#include <d3dx10.h>
#include <string>
#include "DirectXTypes.h"
#include "AttributeLayer.h"

namespace TerrainModel
{
	
class TextureObject
{
public:
	explicit TextureObject( unsigned arraySize = 1 );
	~TextureObject( );
	void setDevice( DX::DevicePtr d ) { m_device = d; }
	bool isInitialized( ) const { return m_initialized; }
	bool fillFromFile( const std::wstring& file, D3DX10_IMAGE_LOAD_INFO* loadInfo = nullptr, ID3DX10ThreadPump* threadPump = nullptr  );
	bool fillFromDescriptor( const D3D10_TEXTURE2D_DESC& textureInfo,  D3D10_SUBRESOURCE_DATA& resourceData );

	bool fillFromLayer( const AttributeTool::AttributeLayer<D3DXVECTOR4>& );
	bool fillFromLayer( const AttributeTool::AttributeLayer<D3DXVECTOR3>& );
	bool fillFromLayer( const AttributeTool::AttributeLayer<float>& );
	// for GPU -> CPU transfers
	bool fillStagingFromLayer(  const AttributeTool::AttributeLayer<D3DXVECTOR4>& );
	// updating
	bool update( const D3D10_BOX&& region, const AttributeTool::AttributeLayer<D3DXVECTOR4>& data );
	ID3D10ShaderResourceView* getResourceView() const { return m_textureResView; }
	ID3D10Texture2D* getResourceHandle() const { return m_textureHandle; }
	void reset();
	unsigned getArraySize( ) const { return m_arraySize; }

private:
	template< class TData >
	bool fill( const D3D10_TEXTURE2D_DESC& desc, const AttributeTool::AttributeLayer<TData>& layer);

	bool						m_initialized;
	ID3D10ShaderResourceView*	m_textureResView;
	ID3D10Texture2D*			m_textureHandle;

	unsigned					m_arraySize;
	ID3D10Device*				m_device;
};

// Definitions
template< class TData >
bool TextureObject::fill( const D3D10_TEXTURE2D_DESC& desc, const AttributeTool::AttributeLayer<TData>& layer)
{
	// should check if TData is compatible with desc.Format
	const size_t tSize = desc.Width * desc.Height;
	TData* tex = new TData[tSize];
	size_t ff= sizeof(TData);
		for( unsigned i = 0; i < tSize; ++i)
		{
			tex[i] =  layer.get( i );
		}
		
	D3D10_SUBRESOURCE_DATA resData;
	resData.pSysMem = (void*)tex;
	resData.SysMemPitch = desc.Width * sizeof(TData);
	resData.SysMemSlicePitch = 0;
	
	HRESULT res = m_device->CreateTexture2D( &desc, &resData, &m_textureHandle );
	unsigned subresource = 0;

	ReportError::Instance().CheckError();

	D3D10_SHADER_RESOURCE_VIEW_DESC resDesc;
	resDesc.Format = desc.Format;
	resDesc.ViewDimension = D3D_SRV_DIMENSION_TEXTURE2D;
	resDesc.Texture2D.MipLevels = desc.MipLevels;
	resDesc.Texture2D.MostDetailedMip = 0;

	res = m_device->CreateShaderResourceView( m_textureHandle, &resDesc, &m_textureResView );

	ReportError::Instance().CheckError();
	delete[] tex;

	m_initialized = res == S_OK;
	return m_initialized;
}

} // end of namespace