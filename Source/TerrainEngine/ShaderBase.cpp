////////////////////////////////////////////////////////////////////////////////
// Filename: ShaderBase.cpp
////////////////////////////////////////////////////////////////////////////////
#include "ShaderBase.h"
#include "Utility\File.h"

#include <fstream>
#include <algorithm>
#include <sstream>

using namespace Shader;

ShaderBase::ShaderBase()
{
	m_effect = 0;


	m_initializers[Text]		= [this](ID3D10Device* d, HWND h, const std::wstring& f) { return this->initializeTextShader(d, h, f); };
	m_initializers[Precompiled] = [this](ID3D10Device* d, HWND h, const std::wstring& f) { return this->initializePrecompiledShader(d, h, f); };
}


ShaderBase::ShaderBase(const ShaderBase& other)
{
}


ShaderBase::~ShaderBase()
{
}


bool ShaderBase::initialize(ID3D10Device* device, HWND hwnd, const std::wstring& file, const ShaderSource shaderSource )
{
	bool result;
	m_shaderFile = file;

	// Initialize the shader that will be used to draw the model.
	
	return m_initializers[shaderSource](device, hwnd, file);
}

void ShaderBase::render( ID3D10Device* device, const unsigned indexCount, const unsigned offset, const unsigned baseVertex)
{
	// Now render the prepared buffers with the shader.
	renderShader(device, indexCount, offset, baseVertex);

	return;
}


bool ShaderBase::initializeTextShader(
	ID3D10Device* device, 
	HWND hwnd, 
	const std::wstring& filename)
{
	HRESULT result;
	ID3D10Blob* errorMessage;


	// Initialize the error message.
	errorMessage = 0;

	// Load the shader in from the file.
	result = D3DX10CreateEffectFromFile(filename.c_str(), NULL, NULL, "fx_4_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, 
										device, NULL, NULL, &m_effect, &errorMessage, NULL);

	if(FAILED(result))
	{
		// If the shader failed to compile it should have writen something to the error message.
		if(errorMessage)
		{
			outputShaderErrorMessage(errorMessage, hwnd, filename);
		}
		// If there was  nothing in the error message then it simply could not find the shader file itself.
		else
		{
			MessageBox(hwnd, filename.c_str(), L"Missing Shader File", MB_OK);
		}

		return false;
	}

	return initializeTechniques(device);
}

bool ShaderBase::initializePrecompiledShader(
	ID3D10Device* device, 
	HWND hwnd, 
	const std::wstring& filename)
{
	HRESULT result;

	// Load the shader in from the file.
	ID3D10Blob* blob;
	bool success = File::readBLOBFromFile( filename, &blob );
	if ( !success )
	{
		MessageBox(hwnd, filename.c_str(), (L"Missing shader file: " + filename).c_str(), MB_OK);
		return false;
	}

	result = D3D10CreateEffectFromMemory( blob->GetBufferPointer(), blob->GetBufferSize(), 0, device, nullptr, &m_effect );
	if(FAILED(result))
	{
		MessageBox(hwnd, filename.c_str(), L"Unable to read shader from memory", MB_OK);
		return false;
	}

	return initializeTechniques(device);
}

bool ShaderBase::addTechnique( ID3D10Device* device,  const std::string& techName, const D3D10_INPUT_ELEMENT_DESC* polygonLayout, const unsigned numberOfElements )
{
	HRESULT result;
	// Get a pointer to the technique inside the shader.
	ID3D10EffectTechnique* technique = m_effect->GetTechniqueByName(techName.c_str());
	if(!technique)
	{
		return false;
	}

	// Get the description of the first pass described in the shader technique.
    D3D10_PASS_DESC passDesc;
    technique->GetPassByIndex(0)->GetDesc(&passDesc);

	// Create the input layout.
	ID3D10InputLayout* layout;
    result = device->CreateInputLayout(polygonLayout, numberOfElements, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, 
									   &layout);
	
	if(FAILED(result))
	{
		return false;
	}

	m_techniqueVector.push_back(technique);
	m_layoutVector.push_back(layout);

	return true;
}

void ShaderBase::shutdown()
{

	// Release the pointer to the shader layout.
	if(m_layoutVector.size() > 0)
	{
		std::for_each( m_layoutVector.begin(), m_layoutVector.end(), 
			[]( ID3D10InputLayout* layout ) { layout->Release(); }
		);
		m_layoutVector.clear();
	}

	// Release the pointer to the shader technique.
	if(m_techniqueVector.size() > 0)
	{
		std::for_each( m_techniqueVector.begin(), m_techniqueVector.end(), 
			[]( ID3D10EffectTechnique* tect ) { tect = 0; }
		);
		m_techniqueVector.clear();
	}

	// Release the pointer to the shader.
	if(m_effect)
	{
		m_effect->Release();
		m_effect = 0;
	}

	shutdownShader();

	return;
}

void ShaderBase::shutdownShader()
{

}


void ShaderBase::outputShaderErrorMessage(
	ID3D10Blob* errorMessage, 
	HWND hwnd, 
	const std::wstring& shaderFilename)
{
	char* compileErrors;
	unsigned long bufferSize, i;
	std::ofstream fout;


	// Get a pointer to the error message text buffer.
	compileErrors = (char*)(errorMessage->GetBufferPointer());

	// Get the length of the message.
	bufferSize = errorMessage->GetBufferSize();

	// Open a file to write the error message to.
	fout.open("shader-error.txt");

	// Write out the error message.
	for(i=0; i<bufferSize; i++)
	{
		fout << compileErrors[i];
	}

	std::string message( compileErrors, bufferSize );
	// Close the file.
	fout.close();

	// Release the error message.
	errorMessage->Release();
	errorMessage = 0;

	// Pop a message up on the screen to notify the user to check the text file for compile errors.
	std::wstringstream ws;
	ws << L"Error compiling shader.  Check shader-error.txt for message.\n";
	ws << L"Or read this:\n";
	ws << message.c_str();

	MessageBox(hwnd, ws.str().c_str(), L"Error Compiling Shader", MB_OK);

	return;
}

HRESULT ShaderBase::SetBool( ID3D10EffectVariable* variable, bool val )
{
	return variable->SetRawValue( &val, 0, sizeof(bool) );
}
//
HRESULT ShaderBase::SetFloat( ID3D10EffectVariable* variable, float val )
{
   return variable->SetRawValue( &val, 0, sizeof(float) );
}
//
HRESULT ShaderBase::SetFloatArray( ID3D10EffectVariable* variable, float val0, float val1 )
{
   float v[2] = { val0, val1 };
   return variable->SetRawValue( v, 0, sizeof(float) * 2 );
}
//
HRESULT ShaderBase::SetFloatArray( ID3D10EffectVariable* variable, float val0, float val1, float val2 )
{
   float v[3] = { val0, val1, val2 };
   return variable->SetRawValue( v, 0, sizeof(float) * 3 );
}
//
HRESULT ShaderBase::SetFloatArray( ID3D10EffectVariable* variable, float val0, float val1, float val2, float val3 )
{
   float v[4] = { val0, val1, val2, val3 };
   return variable->SetRawValue( v, 0, sizeof(float) * 4 );
}
//
HRESULT ShaderBase::SetFloatArray( ID3D10EffectVariable* variable, const float * valArray, int valArrayCount )
{
   return variable->SetRawValue( (void*)valArray, 0, sizeof(float) * valArrayCount );
}
//
HRESULT ShaderBase::SetVector( ID3D10EffectVariable* variable, const D3DXVECTOR4 & vec )
{
	return variable->SetRawValue( (void*)&vec, 0, sizeof(D3DXVECTOR4) );
}
//
HRESULT ShaderBase::SetMatrix( ID3D10EffectMatrixVariable* variable, const D3DXMATRIX & matrix)
{
   return variable->SetMatrix( (float*)&matrix ); 
}

void ShaderBase::renderShader(ID3D10Device* device, const unsigned  indexCount,  const unsigned offset, const unsigned baseVertex)
{
    D3D10_TECHNIQUE_DESC techniqueDesc;
	unsigned int i;
	

	// Set the input layout.
	device->IASetInputLayout(m_layoutVector[0]);

	// Get the description structure of the technique from inside the shader so it can be used for rendering.
    m_techniqueVector[0]->GetDesc(&techniqueDesc);

    // Go through each pass in the technique (should be just one currently) and render the triangles.
	for(i=0; i<techniqueDesc.Passes; ++i)
    {
        m_techniqueVector[0]->GetPassByIndex(i)->Apply(0);
        device->DrawIndexed(indexCount, offset, baseVertex);
    }

	return;
}