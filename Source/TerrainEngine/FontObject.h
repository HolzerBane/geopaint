#pragma once
//////////////
// INCLUDES //
//////////////
#include <fstream>
using namespace std;


class TextureClass;
////////////////////////////////////////////////////////////////////////////////
// Class name: FontObject
////////////////////////////////////////////////////////////////////////////////
class FontObject
{
private:
	struct FontType
	{
		float left, right;
		int size;
	};

	struct VertexType
	{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
	};

public:
	FontObject();
	FontObject(const FontObject&);
	~FontObject();

	bool Initialize(ID3D10Device*, char*, const std::wstring&);
	void Shutdown();

	ID3D10ShaderResourceView* GetTexture();

	void BuildVertexArray(void*, char*, float, float);

private:
	bool LoadFontData(char*);
	void ReleaseFontData();
	bool LoadTexture(ID3D10Device*, const std::wstring&);
	void ReleaseTexture();

private:
	FontType* m_Font;
	TextureClass* m_Texture;
};