#include "AttributeLayer.h"
#include <algorithm>
#include <assert.h>
#include <d3dx10math.h>
#include <limits>
#include <ppl.h>
#include <type_traits>

#include "Utility\Math.h"
#include "Utility\Converter.h"
//#include "Utility\ImageFile.h"

#undef min
#undef max

template<class TValue>
class Limits
{
public:
	static TValue getMin() { return -std::numeric_limits<TValue>::infinity(); }
	static TValue getMax() { return  std::numeric_limits<TValue>::infinity(); }
	static TValue nullVal() { return 0; }
};

template<>
class Limits<D3DXVECTOR4>
{
public:
	static D3DXVECTOR4 getMin() { 
							float m = -std::numeric_limits<float>::infinity();
							return D3DXVECTOR4( m, m, m, m );
						 }

	static D3DXVECTOR4 getMax() { 
							float m = std::numeric_limits<float>::infinity();
							return D3DXVECTOR4( m, m, m, m );
						 }

	static D3DXVECTOR4 nullVal() { return D3DXVECTOR4(); }

};

template<>
class Limits<D3DXVECTOR3>
{
public:
	static D3DXVECTOR3 getMin() { 
							float m = -std::numeric_limits<float>::infinity();
							return D3DXVECTOR3( m, m, m );
						 }

	static D3DXVECTOR3 getMax() { 
							float m = std::numeric_limits<float>::infinity();
							return D3DXVECTOR3( m, m, m );
						 }

	static D3DXVECTOR3 nullVal() { return D3DXVECTOR3(); }

};

using namespace AttributeTool;
//#################################################################################################

template <class TValue >
AttributeLayer< TValue >::AttributeLayer( )
{
	m_minLimit	= Limits<TValue>::getMin();
	m_maxLimit	= Limits<TValue>::getMax();
	m_minVal	= Limits<TValue>::getMax();
	m_maxVal	= Limits<TValue>::getMin();
}

template <class TValue >
AttributeLayer< TValue >::AttributeLayer( const AttributeLayer&& other )
{
	this->m_valueMatrix = std::move(other.m_valueMatrix );
	this->m_h			= std::move(other.m_h);
	this->m_maxLimit	= std::move(other.m_maxLimit);
	this->m_maxVal		= std::move(other.m_maxVal);
	this->m_minLimit	= std::move(other.m_minLimit);
	this->m_minVal		= std::move(other.m_minVal);
	this->m_w			= std::move(other.m_w);
}

//#################################################################################################
/*
template <class TValue >
AttributeLayer< TValue >::AttributeLayer( const FileSource& fs )
{
	m_minLimit	= Limits<TValue>::getMin();
	m_maxLimit	= Limits<TValue>::getMax();
	m_minVal =  Limits<TValue>::getMax();
	m_maxVal =  Limits<TValue>::getMin();

	ImageFile image( fs, m_w, m_h);
	m_valueMatrix.resize( m_w * m_h );
	// copy the image data to the layer
	Concurrency::parallel_for( 0u, m_w * m_h, [this, &image]( const unsigned i) 
		{
			set( i, image.red( i ) / 255.f);
		}
	);
}
*/
//#################################################################################################
template <class TValue >
AttributeLayer< TValue >::AttributeLayer( const Index w, const Index h )
	: m_w(w)
	, m_h(h)
{
	m_minLimit = Limits<TValue>::getMin();
	m_maxLimit = Limits<TValue>::getMax();
	m_minVal =  Limits<TValue>::getMax();
	m_maxVal =  Limits<TValue>::getMin();

	m_valueMatrix.resize( w * h );
}

//#################################################################################################
template <class TValue >
AttributeLayer< TValue >::~AttributeLayer()
{ 
	
}

//#################################################################################################
/*
template <class TValue >
void AttributeLayer< TValue >::saveTofile( const FileSource& fs ) const
{
	// assume D3DXVECTOR4 here
	static_assert(std::is_same<TValue, D3DXVECTOR4>::value, "use it only with D3DXVECTOR4 pls.." );
	unsigned char* image = new unsigned char[ m_w * m_h * 4];
	unsigned index = 0;
	//Concurrency::parallel_for_each( m_valueMatrix.begin(), m_valueMatrix.end(), 
	//	[this, &image, &index]( const TValue& value )
	for(auto it = m_valueMatrix.begin(); it != m_valueMatrix.end(); ++it )	
	{
		auto& value = *it;
		image[index++] = Converter::floatToByte(value[0]);
		image[index++] = Converter::floatToByte(value[1]);
		image[index++] = Converter::floatToByte(value[2]);
		image[index++] = Converter::floatToByte(value[3]);
	} 
	//); 

	ImageFile imageFile;
	imageFile.ReadBuffer( image, 4, m_w, m_h );
	imageFile.SaveBMP( fs );
}
*/
//#################################################################################################

template < class TValue >
void AttributeLayer< TValue >::setLimits( const TValue& minLimit, const TValue& maxLimit )
{
	m_minLimit = minLimit;
	m_maxLimit = maxLimit;
}

//#################################################################################################
template <class TValue >
void AttributeLayer< TValue >::clear()
{
	m_valueMatrix.clear();
}

//#################################################################################################
template <class TValue >
void AttributeLayer< TValue >::reallocate( const unsigned w, const unsigned h )
{ 
	m_w = w;
	m_h = h;
	m_valueMatrix.clear();
	m_valueMatrix.resize( w * h );
}

//#################################################################################################
template <class TValue >
bool AttributeLayer< TValue >::set( const Index x, const Index y, const TValue& v )
{
	if ( x < 0 || x >= m_w || y < 0 || y >= m_h ) return false;
	TValue limited = Math::clamp( m_minLimit, m_maxLimit, v );
	m_valueMatrix[y * m_w + x] = limited;
	setMin( limited ); 
	setMax( limited ); 
	return true;
}

//#################################################################################################
template <class TValue >
bool AttributeLayer< TValue >::set( const Index index, const TValue& v )
{
	if ( index < 0 || index >= m_w * m_h ) return false;
	TValue limited = Math::clamp( m_minLimit, m_maxLimit, v );
	m_valueMatrix[ index ] = limited;
	setMin( limited ); 
	setMax( limited ); 
	return true;
}

//#################################################################################################

template <class TValue >
bool AttributeLayer< TValue >::add( const Index x, const Index y, const TValue& v )
{
	if ( x < 0 || x >= m_w || y < 0 || y >= m_h ) return false;
	TValue newVal = m_valueMatrix[y * m_w + x] + v;
	newVal = Math::clamp( m_minLimit, m_maxLimit, newVal );
	m_valueMatrix[y * m_w + x] = newVal;
	setMin( newVal ); 
	setMax( newVal ); 
	return true;
}

//#################################################################################################

template < class TValue >
bool AttributeLayer< TValue >::isInbound( const Index x, const Index y, const unsigned padding = 0) const
{
	return ( 
		x >= padding		&& 
		x <	 m_w - padding	&& 
		y >= padding		&& 
		y <  m_h -padding
		);
}
//#################################################################################################

template < class TValue >
bool AttributeLayer< TValue >::isInboundIndex( const Index i, const unsigned padding = 0) const
{
	unsigned imod = i % m_w;
	return ( 
		i >= padding * m_w				&& 
		i <	 count() - padding * m_w	&& 
		imod >= padding					&& 
		imod <  m_w - padding
		);
}

//#################################################################################################
template <class TValue >	
const TValue& AttributeLayer< TValue >::get( const Index x, const Index y ) const
{
	assert(x < m_w && y < m_h);
	return m_valueMatrix[y * m_w + x];
}

//#################################################################################################
template <class TValue >	
const TValue& AttributeLayer< TValue >::get( const Index i ) const
{
	assert(i < m_valueMatrix.size());
	return m_valueMatrix[ i ];
}

//#################################################################################################
template <class TValue >	
TValue& AttributeLayer< TValue >::operator[]( const Index i )
{
	assert( i < m_valueMatrix.size() );
	return m_valueMatrix[i];
}

//#################################################################################################
template <class TValue >
void AttributeLayer< TValue >::mul( float val )
{
	for( unsigned i = 0; i < count(); ++i )
	{
		m_valueMatrix[i] *= val; 
	}
}

//#################################################################################################
template <class TValue >
void AttributeLayer< TValue >::mul( const MyType& other )
{
	op( other, []( const TValue& a, const TValue& b){ return a * b; } );
}

//#################################################################################################
template <class TValue >
void AttributeLayer< TValue >::add( const MyType& other)
{
	op( other, []( const TValue& a, const TValue& b){ return a + b; } );
}

//#################################################################################################

template <class TValue >
void AttributeLayer< TValue >::op( const MyType& other, std::function< TValue( const TValue&, const TValue&) > opFn )
{
	assert( m_w == other.m_w || m_h == other.m_h );

	for( unsigned i = 0; i < count(); ++i )
	{
		m_valueMatrix[i] = m_maxLimit, opFn( m_valueMatrix[i], other.m_valueMatrix[i] ); 
	}
}

//#################################################################################################
template <class TValue >
void AttributeLayer< TValue >::saturate( const float minimum, const float maximum )
{
	float satLen = fabs( maximum - minimum );
	float valLen = fabs( m_maxVal - m_minVal );
	float sOv	 = satLen / valLen;
	for( unsigned i = 0; i < count(); ++i )
	{
		m_valueMatrix[i] = minimum + ( m_valueMatrix[i] - m_minVal ) * sOv;
	}
}

//#################################################################################################
template <class TValue >
void AttributeLayer< TValue >::clamp( const float minimum, const float maximum )
{
	for( unsigned i = 0; i < count(); ++i )
	{
		if ( m_valueMatrix[i] > maximum )
		{
			m_valueMatrix[i] = maximum;
			continue;
		}

		if ( m_valueMatrix[i] < minimum)
		{
			m_valueMatrix[i] = minimum;
			continue;
		}
	}
}

//#################################################################################################
template <class TValue >
void AttributeLayer< TValue >::blur()
{
	// really slow weightened blur! 
	for ( int x = 2; x < m_w - 2; ++x )
	for ( int y = 2; y < m_h - 2; ++y )
	{
		// get is guaranteed to be in bounds... for layers that are large enough
		float neighbors = get(x-1, y) + get(x+1, y) +get(x, y-1) + get(x, y+1);
		float neighbors2 = get(x-1, y-1) + get(x+1, y+1) +get(x+1, y-1) + get(x-1, y+1) + get(x-2, y) + get(x+2, y) +get(x, y-2) + get(x, y+2);
		neighbors *= 0.25f;
		neighbors *= 2.f;
		neighbors *= 0.3f; 
		neighbors2 *= 0.125;
		neighbors2 *= 2.f;
		neighbors2 *= 0.4;

		set( x, y,  0.5f * (get( x, y ) * 2.f * 0.5f + neighbors + neighbors2 ));
	}
}


//#################################################################################################
template <class TValue >
void AttributeLayer< TValue >::setMin( const TValue& other )
{
	m_minVal = std::min( m_minVal, other );
}

//#################################################################################################
template <class TValue >
void AttributeLayer< TValue >::setMax( const TValue& other )
{
	m_maxVal = std::max( m_maxVal, other );	
}