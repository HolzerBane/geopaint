#include "PaintBrushHandler.h"
#include "PaintBrush.h"
#include <limits>
#include <iostream>

using namespace TerrainTools;

PaintBrushHandler::PaintBrushHandler()
	: m_allowPaint(true)
{};

bool PaintBrushHandler::allowPaint( const TerrainTools::PaintBrush& brush, const unsigned x, const unsigned y)
{
	// floated to avoid overflow
	unsigned stepSqr = std::powf( (float)m_prevAllowedPaintX - x, 2) + std::powf( (float)m_prevAllowedPaintY - y, 2);
	
	if ( m_allowPaint || stepSqr > brush.BrushType.Step * brush.BrushType.Step)
	{
		m_prevAllowedPaintX = x;
		m_prevAllowedPaintY = y;

		m_allowPaint = false; 

		return true;
	}

	return false;
}

void PaintBrushHandler::resetPrevState()
{
	m_allowPaint = true;
}
