#pragma once
#include "AttributeLayer.h" 
#include "d3dx9math.h"

namespace TerrainModel
{

struct TerrainMaterial
{
	AttributeTool::AttributeLayer<float>			Geometry;
	AttributeTool::AttributeLayer< D3DXVECTOR4 >	Diffuse;
};

}