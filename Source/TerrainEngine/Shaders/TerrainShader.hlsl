#define PROJECTION 1
#define LIGHTS 1

float4 morphLevels;

bool   renderWithNormalMaps = true;
float3 g_lightPos;
float4 g_fogColor = float4(0.4f, 0.4f, 0.45f, 1.0f);

// scale and offset variables for vertex shader
float4 heightmapTextureInfo;
float4 terrainScale;
float4 terrainOffset;
float2 samplerWorldToTextureScale;
float2 detailRatio;

float4 quadOffset;           // .z holds the z center of the AABB
float4 quadScale;            // .z holds the current LOD level
float2 quadWorldMax;         // .xy max used to clamp triangles outside of world range
float4 gridDim;

float3 cameraPos;
float  viewDistance;

// brush projection data
matrix brush_view;
matrix brush_projection;
Texture2D brush_projectionTexture;
float3 brush_position;

// Constant buffers
cbuffer Priorities
{
	float4 LP4 = float4( 0.6f, 0.2f, 0.15f, 0.05f ); 
	float3 LP3 = float3( 0.7f, 0.2f, 0.1f ); 
	float2 LP2 = float2( 0.8f, 0.2f );
	float  pi  = 3.14159265f;
}

cbuffer Lights
{
	// light amplitudes for linear and quadratic terms
    float  cb_lightA1 = 90.f;
    float  cb_lightA2 = 3.f;
}

cbuffer Material
{
	float Kd = 0.79;
    float Ks = 0.97;
    float Kb = 1.0;
    float SpecExpon = 24.0;
    float4 ambiColor = float4(0.15,0.15,0.15,0);
}

cbuffer TextureIndexBuffer
{       
    float2    mergeMap[7];
}

cbuffer AttributeInfoBuffer
{
	float2	attribSize;
}

matrix worldMatrix;
matrix viewMatrix;
matrix projectionMatrix;
Texture2D perlinTexture;

Texture2D attributeMap;
Texture2D indexMap;
// TextureArray indices:
// x - height info
// yzw - normal info
// ( bitangent info is calculated online )
Texture2D vertexHeightMap;  
Texture2D vertexTangentMap; 

Texture2DArray diffuseArray		: TextureArray;
Texture2DArray normalArray		: NormalArray;

TextureCube SkyBoxTexture		: Enviroment; 

///////////////////
// SAMPLE STATES //
///////////////////
SamplerState SampleLinear
{
    Filter = MIN_MAG_MIP_LINEAR;
    //Filter = ANISOTROPIC;
    AddressU = Wrap;
    AddressV = Wrap;
};

SamplerState SamplePoint
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};

SamplerState SampleAniso
{
    Filter = ANISOTROPIC;
    AddressU = Wrap;
    AddressV = Wrap;
};

SamplerState SampleSkyBox  
{ 
	Filter =  MIN_MAG_MIP_LINEAR;
	AddressU = Mirror; 
	AddressV = Mirror; 
};




//////////////
// TYPEDEFS //
//////////////
struct VertexInputType
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
};

struct MatVertexInputType
{
    float4 pos          : POSITION;
    float4 tex0         : TEXCOORD0;
    float4 tex1         : TEXCOORD1; 
};

struct MatPixelInputType
{
	// world view proj position
    float4 pos          : SV_Position;
	// two pairs of uv coordinates
    float4 tex0         : TEXCOORD0;
	// contribution factors
    float4 contrib      : TEXCOORD1; 
	// indices of texture layers to sample from
    float4 indices      : TEXCOORD2; 
	// geometry normal at pos
	float3 normal		: NORMAL;
	// uv increase dirs by tex0 xy
	float3 binormal		: BINORMAL;
	float3 tangent		: TANGENT;
	// world position 
	float3 worldPos     : TEXTURE0;
};

struct ProjPixelInputType
{
	// world view proj position
    float4 pos          : SV_Position;
	// two pairs of uv coordinates
    float4 tex0         : TEXCOORD0;
	// contribution factors
    float4 contrib      : TEXCOORD1; 
	// indices of texture layers to sample from
    float4 indices      : TEXCOORD2; 
	// geometry normal at pos
	/*
	float3 normal		: NORMAL;
	// uv increase dirs by tex0 xy
	float3 binormal		: BINORMAL;
	float3 tangent		: TANGENT;
	*/
	// world position 
	float3 worldPos     : TEXTURE0;
	// projection relative position
	float4 brush_position : TEXCOORD3;
	
	float  fogFactor	: TEXCOORD4;

};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
};

////////////////////////////////////////////////////////////////////////////////
// Storing rough alpha data
////////////////////////////////////////////////////////////////////////////////
tbuffer AlphaBuffer
{
	float4	alphaValues[16];
	float2	alphaSides;
}

float4 getNoisedLayerColor( float3 attIndex, const float3 params );// layer, attribute, noise
float4 getNoisedLayerNormal( float3 attIndex, const float3 params ); // layer, attribute, noise
float4 getProjectedBrushColor( const float4 brushPosition );
float  sampleHeightMap( float2 uv, float mipLevel );
float3 sampleNormalMap( float2 uv, float mipLevel );
float3 sampleTangentMap( float2 uv, float mipLevel );
float4 getFoggedColor( const float4 inputcolor, const float4 pixelPosition );
float  getFogFactor( const float4 position );

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////

/* CDLOD vertex shader*/
// returns baseVertexPos where: xy are true values, z is g_quadOffset.z which is z center of the AABB
float4 getBaseVertexPos( float4 inPos )
{
   float4 ret = inPos * quadScale + quadOffset;
   //ret.xz = min( ret.xz, 1200 );
   return ret;
}

// morphs vertex xy from high to low detailed mesh position
float2 morphVertex( float4 inPos, float2 vertex, float morphLerpK )
{
   // vertex is in the plane of the grid
   float2 fracPart = (frac( inPos.xz * float2(gridDim.y, gridDim.y) ) * float2(gridDim.z, gridDim.z) ) * quadScale.xz;
   return vertex.xy - fracPart * morphLerpK;
}

// calc texture tex coords for the whole world
float2 calcGlobalUV( float2 vertex )
{
   float2 globalUV = (vertex.xy - terrainOffset.xz) / terrainScale.xz;  // this can be combined into one inPos * a + b
   globalUV *= samplerWorldToTextureScale;
   globalUV += heightmapTextureInfo.zw * 0.5;
   return globalUV;
}


//float sampleHeightmap( float2 uv, float mipLevel, bool useFilter )
//{
//	return vertexHeightMap.SampleLevel( SampleLinear, uv.xy, mipLevel ).x;
//}

void ProcessCDLODVertex( 
	float4 inPos, out float4 outUnmorphedWorldPos, 
	out float4 outWorldPos, out float2 outGlobalUV, 
	out float2 outDetailUV, out float2 outMorphK, 
	out float outEyeDist/*, out float3 outNormal, out float3 outTangent */)
{
   float4 vertex		= getBaseVertexPos( inPos );
   const float LODLevel = quadScale.y;
   float mipLevel = 0;  

   float2 preGlobalUV = calcGlobalUV( vertex.xz );
   vertex.y = sampleHeightMap( preGlobalUV.xy, mipLevel ) * terrainScale.y + terrainOffset.y;

   outUnmorphedWorldPos = vertex;
   outUnmorphedWorldPos.w = 1;
   
   float eyeDist     = distance( vertex, float4(cameraPos, 1.0));
   float morphLerpK  = 1.0f - clamp( morphLevels.z - eyeDist * morphLevels.w, 0.0, 1.0 );   
   //float morphLerpK  = 1.0f - clamp( ml.z - eyeDist * ml.w, 0.0, 1.0 );   
   
   vertex.xz         = morphVertex( inPos, vertex.xz, morphLerpK );
 
   float2 globalUV   = calcGlobalUV( vertex.xz );

   float4 vertexSamplerUV = float4( globalUV.x, globalUV.y, 0, mipLevel );

   vertex.y = vertexHeightMap.SampleLevel( SampleLinear, vertexSamplerUV.xy, mipLevel ).x * terrainScale.y + terrainOffset.y;

   vertex.w = 1.0;   
   
   float detailMorphLerpK = 0.0;

   float2 detailUV   = float2( globalUV.x * detailRatio.x, globalUV.y * detailRatio.y );

   outWorldPos      = vertex;
   outGlobalUV      = globalUV;
   outDetailUV      = detailUV;
   outMorphK        = float2( morphLerpK, detailMorphLerpK );
   outEyeDist       = eyeDist;
   // uv might need to be non-morphed for the normal and tangent
   //outNormal		= sampleNormalMap( preGlobalUV, mipLevel );
   //outTangent		= sampleTangentMap( preGlobalUV, mipLevel );
}

float sampleLerp1D( Texture2D tex, float2 uv, float mipLevel )
{
	const float2 textureSize = heightmapTextureInfo.xy;
	const float2 texelSize   = heightmapTextureInfo.zw; 

   uv = uv.xy * textureSize - float2(0.5, 0.5);
   float2 uvf = floor( uv.xy );
   float2 f = uv - uvf;
   uv = (uvf + float2(0.5, 0.5)) * texelSize;

   float t00 = tex.SampleLevel( SampleLinear, uv.xy, mipLevel ).x;
   float t10 = tex.SampleLevel( SampleLinear, float2(uv.x + texelSize.x, uv.y), mipLevel ).x;

   float tA = lerp( t00, t10, f.x );

   float t01 = tex.SampleLevel( SampleLinear, float2( uv.x, uv.y + texelSize.y), mipLevel ).x;
   float t11 = tex.SampleLevel( SampleLinear, float2( uv.x + texelSize.x, uv.y + texelSize.y), mipLevel ).x;

   float tB = lerp( t01, t11, f.x );

   return lerp( tA, tB, f.y );
}

float3 sampleTangentMap( float2 uv, float mipLevel )
{
	const float2 textureSize = heightmapTextureInfo.xy;
	const float2 texelSize   = heightmapTextureInfo.zw; 

   uv = uv.xy * textureSize - float2(0.5, 0.5);
   float2 uvf = floor( uv.xy );
   float2 f = uv - uvf;
   uv = (uvf + float2(0.5, 0.5)) * texelSize;

   float3 t00 = vertexTangentMap.SampleLevel( SampleLinear, uv.xy, mipLevel ).xyz;
   float3 t10 = vertexTangentMap.SampleLevel( SampleLinear, float2(uv.x + texelSize.x, uv.y), mipLevel ).xyz;

   float3 tA = lerp( t00, t10, f.x );

   float3 t01 = vertexTangentMap.SampleLevel( SampleLinear, float2( uv.x, uv.y + texelSize.y), mipLevel ).xyz;
   float3 t11 = vertexTangentMap.SampleLevel( SampleLinear, float2( uv.x + texelSize.x, uv.y + texelSize.y), mipLevel ).xyz;

   float3 tB = lerp( t01, t11, f.x );

   return lerp( tA, tB, f.y );
}

float3 sampleNormalMap( float2 uv, float mipLevel )
{
	const float2 textureSize = heightmapTextureInfo.xy;
	const float2 texelSize   = heightmapTextureInfo.zw; 

   uv = uv.xy * textureSize - float2(0.5, 0.5);
   float2 uvf = floor( uv.xy );
   float2 f = uv - uvf;
   uv = (uvf + float2(0.5, 0.5)) * texelSize;

   float3 t00 = vertexHeightMap.SampleLevel( SampleLinear, uv.xy, mipLevel ).yzw;
   float3 t10 = vertexHeightMap.SampleLevel( SampleLinear, float2(uv.x + texelSize.x, uv.y), mipLevel ).yzw;

   float3 tA = lerp( t00, t10, f.x );

   float3 t01 = vertexHeightMap.SampleLevel( SampleLinear, float2( uv.x, uv.y + texelSize.y), mipLevel ).yzw;
   float3 t11 = vertexHeightMap.SampleLevel( SampleLinear, float2( uv.x + texelSize.x, uv.y + texelSize.y), mipLevel ).yzw;

   float3 tB = lerp( t01, t11, f.x );

   return lerp( tA, tB, f.y );
}

float sampleHeightMap( float2 uv, float mipLevel )
{
	//return vertexHeightMap.SampleLevel( SampleLinear, uv.xy, mipLevel ).x;

	const float2 textureSize = heightmapTextureInfo.xy;
	const float2 texelSize   = heightmapTextureInfo.zw; 

   uv = uv.xy * textureSize - float2(0.5, 0.5);
   float2 uvf = floor( uv.xy );
   float2 f = uv - uvf;
   uv = (uvf + float2(0.5, 0.5)) * texelSize;

   float t00 = vertexHeightMap.SampleLevel( SampleLinear, uv.xy, mipLevel ).x;
   float t10 = vertexHeightMap.SampleLevel( SampleLinear, float2(uv.x + texelSize.x, uv.y), mipLevel ).x;

   float tA = lerp( t00, t10, f.x );

   float t01 = vertexHeightMap.SampleLevel( SampleLinear, float2( uv.x, uv.y + texelSize.y), mipLevel ).x;
   float t11 = vertexHeightMap.SampleLevel( SampleLinear, float2( uv.x + texelSize.x, uv.y + texelSize.y), mipLevel ).x;

   float tB = lerp( t01, t11, f.x );

   return lerp( tA, tB, f.y );
   
}


ProjPixelInputType terrainCDLOD( 
		float4 pos          : POSITION,
		float4 tex0         : TEXCOORD
 )
{
   ProjPixelInputType output;
   
   float4 unmorphedWorldPos;
   float4 worldPos;
   float4 worldViewPos;
   float2 globalUV;
   float2 detailUV;
   float2 morphK;
   float eyeDist;

   /* get the vertex position of the patch vertex */
   ProcessCDLODVertex( 
		pos, unmorphedWorldPos, worldPos, 
		globalUV, detailUV, morphK, eyeDist
		/*, output.normal, output.tangent */);
   worldPos.w = 1.0f;

   pos.w = 1.0f;
   ////////////////////////////////////////////////////////////////////////////   
   float3 eyeDir     = normalize( (float3)cameraPos - (float3)worldPos );
   //
   output.pos = mul(worldPos, viewMatrix);
   output.pos = mul(output.pos, projectionMatrix);

   worldViewPos = mul(worldPos, viewMatrix);

	output.tex0 = float4( globalUV, detailUV );

	// get the attribute merge info - globalUV is 0 to 1 for the whole map
	output.contrib = attributeMap.SampleGrad(SampleLinear, globalUV, globalUV, globalUV );
	output.indices = indexMap.SampleGrad(SampleLinear, globalUV, globalUV, globalUV );

	// world position (no proj and view included) for lightning
	output.worldPos.xyz = mul(float4(pos.xyz,1), worldMatrix).xyz;

	// pass the brush location
	// projection matrices
    output.brush_position = worldPos;
    output.brush_position = mul(output.brush_position, brush_view);
    output.brush_position = mul(output.brush_position, brush_projection);

	output.fogFactor = getFogFactor( worldPos );
   
   return output;    
}
/*CDLOD vertex shader end*/

ProjPixelInputType AlphaMapVertexShader(
		float4 pos          : POSITION,
		float4 tex0         : TEXCOORD0,
		float3 normal       : NORMAL,
		float3 binormal     : BINORMAL,
		float3 tangent      : TANGENT
		)
{
    ProjPixelInputType output;
    
	// Change the position vector to be 4 units for proper matrix calculations.
    pos.w = 1.0f;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	float height = vertexHeightMap.SampleGrad(SampleLinear, tex0.zw, 0.1, 0.1 ).x;
	pos.y = height;

    output.pos = mul(pos, worldMatrix);
    output.pos = mul(output.pos, viewMatrix);
    output.pos = mul(output.pos, projectionMatrix);

	//output.normal.xyz = normalize( mul(normal, (float3x3)worldMatrix));
	//output.binormal.xyz = normalize( mul(binormal, (float3x3)worldMatrix));
	//output.tangent.xyz	= normalize( mul(tangent, (float3x3)worldMatrix));
    
	#if PROJECTION
	// projection matrices
    output.brush_position = mul(pos, worldMatrix);
    output.brush_position = mul(output.brush_position, brush_view);
    output.brush_position = mul(output.brush_position, brush_projection);
	#endif
	// world position for lighting
    output.worldPos.xyz = mul(float4(pos.xyz,1), worldMatrix).xyz;

	// Store the texture coordinates for the pixel shader.
    output.tex0 = tex0;
    
	// get the attribute merge info - tex0 last two floats are the uv-s for the attribute map
	output.contrib = attributeMap.SampleGrad(SampleLinear, tex0.zw, tex0.zw, tex0.zw );
	output.indices = indexMap.SampleGrad(SampleLinear, tex0.zw, tex0.zw, tex0.zw );
	output.fogFactor = 0.0f;

	return output;
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 AlphaMapPixelShader(ProjPixelInputType input) : SV_Target
{
	float4 color1;
    float4 color2;
    float4 alphaValue;
    float4 blendColor;
	float4 attributeValue;
	float4 perlinTex;
	float4 attr1;

	// is it really slower if it is put here?
	input.contrib = attributeMap.Sample(SampleLinear, input.tex0.xy);
	float3 tangent = vertexTangentMap.Sample( SampleLinear, input.tex0.xy ).x;
	float3 normal  =  vertexHeightMap.Sample( SampleLinear, input.tex0.xy ).yzw;
	float3 binormal	= cross(normal, tangent);
	binormal = normalize( binormal );
	// check if attributes are available and fill as needed

	/*********************************************************************************************/
	/* Diffuse Color */
	/*********************************************************************************************/
	float3 attIndex = float3( input.tex0.zw, 0);
	float4 diffuse = float4( 0.f, 0.f, 0.f, 0.f );
	float4 bump = float4( 0.f, 1.f, 0.f, 0.f );
	float  count = 0.0f;
	float  contribution = 1;

    perlinTex = perlinTexture.Sample(SampleLinear, input.tex0.xy * 4.0f);
	perlinTex = perlinTex / 1.5f;
	perlinTex += 0.33f;

	if ( input.contrib.x > 0.0f )
	{		
		diffuse += getNoisedLayerColor(  attIndex, float3(  input.indices.x, input.contrib.x, perlinTex.x) );
		bump  += getNoisedLayerNormal( attIndex, float3(  input.indices.x, input.contrib.x, perlinTex.x) );
		count   += 1.f;
		contribution -= input.contrib.x;
	}

	if ( input.contrib.y > 0.0f && contribution > 0.0f)
	{
		diffuse += getNoisedLayerColor( attIndex, float3(  input.indices.y, min( contribution, input.contrib.y), perlinTex.x) );
		bump  += getNoisedLayerNormal( attIndex, float3(  input.indices.y, min( contribution, input.contrib.y), perlinTex.x) );
		count   += 1.f;
		contribution -= input.contrib.y;
	}
	if ( input.contrib.z > 0.0f && contribution > 0.0f)
	{
		diffuse += getNoisedLayerColor( attIndex, float3(  input.indices.z, min( contribution, input.contrib.z), perlinTex.x) );
		bump  += getNoisedLayerNormal( attIndex, float3(  input.indices.z, min( contribution, input.contrib.z ), perlinTex.x) );
		count   += 1.f;
		contribution -= input.contrib.z;
	}

	if ( input.contrib.w > 0.0f && contribution > 0.0f)
	{
		diffuse += getNoisedLayerColor( attIndex, float3(  input.indices.w, min( contribution, input.contrib.w ), perlinTex.x) );
		bump  += getNoisedLayerNormal( attIndex, float3(  input.indices.w, min( contribution, input.contrib.w), perlinTex.x) );
		count   += 1.f;
		contribution -= input.contrib.w;
	}
	
	diffuse = saturate( diffuse );
	float4 result = diffuse;
	//diffuse = float4(input.tex1.w, input.tex1.w, input.tex1.w, 1.f);
	/*********************************************************************************************/
	/* Lighting */
	/*********************************************************************************************/

#if LIGHTS
    float3 Nn = normalize(normal);
    
	// Include binormal and tangent here
	float3 Tn = normalize(tangent);
    float3 Bn = normalize(binormal);

	bump *= 5.f;

    float3 Nb = Nn;
	if(renderWithNormalMaps) 
		Nb += (bump.x * Tn + bump.y * Bn);

    Nb = normalize(Nb);
    float3 toLight = g_lightPos - input.worldPos.xyz;
    float dist = length(toLight);
    float3 Ln = toLight / dist;
    float ldn = max(0.0, dot(Ln,Nb));
    float4 diffContrib = saturate(Kd*(ldn) + ambiColor);
    float scale = saturate( cb_lightA1 / dist + cb_lightA2 / (dist*dist));
	
    diffContrib *= scale;
    result = float4( diffuse.xyz * diffContrib.xyz, 1.f);    
	result += float4( 0.2f, 0.2f, 0.2f, 1.f );

	float sunStr = 1.f;
	if ( g_lightPos.y < 0.0f)
	{
		sunStr +=  g_lightPos.y / 20.f;
	}

	result *= sunStr ;
#endif

#if PROJECTION
	result += getProjectedBrushColor( input.brush_position );
#endif

	result = input.fogFactor * g_fogColor + ( 1.0f - input.fogFactor )* result;
	result.w = 1.0f;
	    
	return result * result ;
}


float4 TesterPixelShader(MatPixelInputType input) : SV_Target
{	
	//float4 diffuse = float4( 0.0f, 0.5f , 0.0f, 1.0f );
	float4 perlin = perlinTexture.Sample(SampleLinear, input.tex0.zw, 0 ) ;
	//float4 diffuse  = diffuseArray.Sample( SampleLinear, float3(input.tex0.xy, 2.f)) * perlin  ;
	float heightval = vertexHeightMap.Sample( SampleLinear, input.tex0.zw ).x;
	float4 diffuse = float4 ( heightval / 8.f, heightval / 2.f, heightval / 4.f, 1.f );
#if 0
	float3 Nn = normalize(input.normal);

    float3 toLight = g_lightPos - input.worldPos.xyz;
    float dist = length( toLight );
    float3 Ln = toLight / dist;
    float ldn = max(0.0, dot(Ln,Nn));
    float4 diffContrib = saturate(Kd*(ldn) + ambiColor);
    float scale = saturate( cb_lightA1 / dist + cb_lightA2 / (dist*dist));
    diffContrib *= scale;
    float4 result = float4( diffuse.xyz * diffContrib.xyz, 1.f); 
	float sunStr = 1.f;
	if ( g_lightPos.y < 0.0f)
	{
		sunStr +=  g_lightPos.y / cb_lightA1;
	}

	return result * sunStr ;
#endif

	return diffuse;
}

float4 TexTestPixelShader(MatPixelInputType input) : SV_Target
{
	float4 perlin = perlinTexture.Sample(SampleLinear, input.tex0.xy, 1 ) ;
	float4 color  = diffuseArray.Sample( SampleLinear, float3(input.tex0.zw, 2.f)) * perlin  ;
	return perlin;
}

float4 TestPixelShaderUV(ProjPixelInputType input) : SV_Target
{
	//float c = attributeMap.Sample( SampleLinear, float2(input.tex0.zw)).x;
	//float4 color = attributeMap.Sample( SampleLinear, float2(input.tex0.zw));
	//float4(0.0, input.contrib.z, 0.0f, 1.0f);
	//getNoisedLayerColor( float3(input.tex0.zw,0), 1);
	//float4(0.0, input.contrib.y, input.tex0.z, 1.0);
	//color = diffuseArray.Sample( SampleLinear, float3(input.tex0.zw, 2.f));//float4(0.0, input.tex0.w, input.tex0.z, 1.0);

	//float4 color = getProjectedBrushColor( input.brush_position );
	float4 color = float4( vertexTangentMap.Sample( SampleLinear, input.tex0.xy ).xyz , 1.0 );
	return color;
}

float4 WireFramePS(ProjPixelInputType input) : SV_Target
{
	//float height = sampleHeightMap( input.tex0.xy, 0 ).x;
	float height = input.tex0.z;
	return float4(frac(input.tex0.z), frac(input.tex0.w), 0.0f, 1.0f);
}

/*****************************************************************************************************/
// Ray triangle intersection
/*****************************************************************************************************/

/*****************************************************************************************************/
/* Helper functions*/
/*****************************************************************************************************/

float4 getNoisedLayerColor( float3 attIndex, const float3 params ) // layer, attribute, noise
{
		attIndex.z = params.x;
		float4 color = diffuseArray.Sample( SampleLinear, attIndex ) *params.y  ;

		float maskedPerlin = params.y * clamp(params.z, 0.2f, 1.f ); 
		float noisedMask =  (( 1.2f - params.y ) * maskedPerlin * 3 + pow( params.y, 3 ) );// * saturate( 1.f + perlinTex) ;
		color.w = saturate(noisedMask);
		return saturate( saturate(noisedMask)* color);
}

float4 getNoisedLayerNormal( float3 attIndex, const float3 params ) // layer, attribute, noise
{
		attIndex.z = params.x;
		float4 color = normalArray.Sample( SampleLinear, attIndex) * params.y  ;
		float maskedPerlin =  params.y * clamp(params.z, 0.f, 1.f );
		float noisedMask =  (( 1.f - params.y ) * maskedPerlin * 5 + pow( params.y, 5 ) );// * saturate( 1.f + perlinTex) ;
		// G is up
		float temp = color.y;	//G
		color.y = color.z;		// G = B 
		color.z = temp;			// B = G
		return saturate( saturate(noisedMask)* color);
}

float4 getFoggedColor( const float4 inputcolor, const float4 pixelPosition )
{
		float dist = distance( cameraPos, pixelPosition ); 
		float fogDensity = 0.02f;
		float4 fogColor = float4(0.8f, 0.8f, 0.0f, 0.f);
		fogColor.w =  1.0f - saturate(pow( 1.0f / 2.71828f,(dist * fogDensity)));

		float4 retColor = inputcolor * ( 1.0f - fogColor.w ) + fogColor.w * fogColor;
		retColor.w = inputcolor.w;

		return retColor;
}

float  getFogFactor( const float4 position )
{
		float dist = distance( cameraPos, position ); 
		float fogDensity = 0.00005f; // less means more fog
		float fogFact = 1.0f - saturate(pow( 0.4f,(dist * dist * fogDensity)));
		return  fogFact * fogFact;
}


#if 0
void generateNormalAndTangent(float3 v1, float3 v2, float2 st1, float2 st2)
	{
		float3 normal = v1.crossProduct(v2);
		
		float coef = 1/ (st1.u * st2.v - st2.u * st1.v);
		float3 tangent;

		tangent.x = coef * ((v1.x * st2.v)  + (v2.x * -st1.v));
		tangent.y = coef * ((v1.y * st2.v)  + (v2.y * -st1.v));
		tangent.z = coef * ((v1.z * st2.v)  + (v2.z * -st1.v));
		
		float3 binormal = normal.crossProduct(tangent);
	}
#endif

/**********************************************************************************************/
/* brush projection */
/***********************************************************************************************/
	float4 getProjectedBrushColor(const float4 brushPosition)
	{
		float2 brushProjectionCoord;
		float4 projectionColor = float4(0.0f, 0.0f, 0.0f, 1.0f);

		brushProjectionCoord.x =  brushPosition.x / brushPosition.w / 2.0f + 0.5f;
		brushProjectionCoord.y = -brushPosition.y / brushPosition.w / 2.0f + 0.5f;

		// detect if the projected image can be seen on this pixel
		if(	(saturate(brushProjectionCoord.x) == brushProjectionCoord.x) && 
			(saturate(brushProjectionCoord.y) == brushProjectionCoord.y))
		{
			// Sample the color value from the projection texture using the sampler at the projected texture coordinate location.
			projectionColor = brush_projectionTexture.Sample(SampleLinear, brushProjectionCoord);
			float treshold = 0.2f;
			projectionColor = clamp( -abs( treshold - projectionColor ) + treshold, 0.0f, 1.0f);

		}

		return projectionColor;
	}
/*****************************************************************************************************/
// Raster state
/*****************************************************************************************************/

RasterizerState SolidRS
{
    FillMode = Solid;
	MultisampleEnable = TRUE;
};

RasterizerState WireRS
{
    FillMode = Wireframe;
	MultisampleEnable = TRUE;
};

BlendState ZOnly
{
    RenderTargetWriteMask[0] = 0;
};

BlendState Normal
{
    RenderTargetWriteMask[0] = 0x0F;
};

DepthStencilState ZwriteNoZtest
{
    DepthEnable = false;
};

DepthStencilState NormalDS
{
    DepthEnable = true;
};

////////////////////////////////////////////////////////////////////////////////
// Technique
////////////////////////////////////////////////////////////////////////////////
technique10 AlphaMapTechnique
{
    pass pass0
    {
        //SetVertexShader(CompileShader(vs_4_0, AlphaMapVertexShader()));
        SetVertexShader(CompileShader(vs_4_0, terrainCDLOD()));
        SetPixelShader(CompileShader(ps_4_0, AlphaMapPixelShader()));
        //SetPixelShader(CompileShader(ps_4_0, TestPixelShaderUV()));
		//SetPixelShader(CompileShader(ps_4_0, WireFramePS()));
		//SetPixelShader(NULL);
		
		SetGeometryShader(NULL);
		SetBlendState( Normal, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
        SetDepthStencilState(NormalDS,0);
        SetRasterizerState(SolidRS);
    }
}