#pragma once
#include <vector>

struct ID3D10Buffer;
struct ID3D10Device;

namespace DX
{
	typedef ID3D10Buffer* BufferPtr;
	typedef ID3D10Device* DevicePtr;

	typedef std::vector< unsigned long > IndexList;
};