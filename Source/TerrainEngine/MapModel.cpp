#include "MapModel.h" 

#include <time.h>
#include <ppl.h>

#include "AttributeMap.hpp"
//#include "AttributeLayer.hpp"
#include "AttributePainter.hpp"
#include "PaintBrush.h"
#include "Utility\converter.h"
#include "Utility\Math.h"
#include "Utility\PerlinV2.h"

#undef min
#undef max

// disable unsigned to float cast warnings 
// occurs when initing dx vectors with unsigned ints
#pragma warning( disable : 4244 )

#define NORMAL_FAST		0
#define NORMAL_CROSS	1
#define NORMAL_AVG		0

using namespace TerrainModel;
using namespace AttributeTool;

//#################################################################################################
MapModel::MapModel(  unsigned int x, unsigned int y )
	: m_mapSize( x, y )
	, m_heightNormalLayer( x, y )
	, m_tangentLayer( x, y )
	, m_textureFactor( 5.f )
	, m_attributeMap(4)
	, m_attributeTexture( 1 )
	, m_indexTexture( 1 )
	, m_heightTexture( 1 )
	, m_brushPainter( m_attributeLayer )
	, m_brushGeometryPainter( m_heightNormalLayer )
{
	m_attributeLayer.setLimits( D3DXVECTOR4( 0.f, 0.f, 0.f, 0.f ), D3DXVECTOR4( 1.f, 1.f, 1.f, 1.f ) );
	m_heightNormalLayer.setLimits( D3DXVECTOR4( 0.f, 0.f, 0.f, 0.f ), D3DXVECTOR4( 1.f, 1.f, 1.f, 1.f ) );
}
//#################################################################################################

void MapModel::setDevice( DX::DevicePtr d )
{ 
	m_attributeTexture.setDevice( d );
	m_indexTexture.setDevice( d );
	m_heightTexture.setDevice( d );
	m_tangentTexture.setDevice( d );

	m_device = d; 
}
//#################################################################################################

unsigned MapModel::addAttribute( AttributeTool::AttributeLayer< float >&& al, const float heightContribution )
{
	m_heightContribution.push_back(heightContribution);
	return m_attributeMap.addLayer(std::move(al));
}

//#################################################################################################

unsigned MapModel::attributeSizeX() const { return m_attributeMap.getLayer(0).getW(); }
unsigned MapModel::attributeSizeY() const { return m_attributeMap.getLayer(0).getH(); }

void MapModel::clearAttributes()
{
	m_heightContribution.clear();
	m_attributeMap.clear();
	m_attributeLayer.clear();
	m_heightTexture.reset();
	m_indexTexture.reset();
	m_tangentTexture.reset();
	m_attributeTexture.reset();
}
//#################################################################################################

void MapModel::generateRandom()
{
	srand( time(NULL) );
	int ceil = 1000;

	for ( unsigned y = 0; y < m_heightNormalLayer.getH(); ++y)
	for ( unsigned x = 0; x < m_heightNormalLayer.getW(); ++x)
	{
		m_heightNormalLayer.set(x, y, D3DXVECTOR4( (rand() % ceil) / (float)ceil, 0.f, 0.f, 0.f ) );
	}
}

//#################################################################################################

void MapModel::generatePerlinV2()
{
	Algorithm::Noise::PerlinV2 p(12, 0.003f, 4.7f, 1);

	for ( unsigned y = 0; y < m_heightNormalLayer.getH(); ++y)
	for ( unsigned x = 0; x < m_heightNormalLayer.getW(); ++x)
	{
		m_heightNormalLayer.set( x, y, D3DXVECTOR4( p.Get(x,y), 0.f, 0.f, 0.f));
	}
}
//#################################################################################################
// return the Normal, Binormal and Tanget of vertex at x, y
void MapModel::getNBTOf( 
	const Index x, const Index y, 
	const float tex1u, const float tex1v, const float tex2u, const float tex2v,
	D3DXVECTOR3& n, D3DXVECTOR3& b, D3DXVECTOR3& t)
{
	D3DXVECTOR3 normal( 0.f, 1.f, 0.f);
	
	if (!m_heightNormalLayer.isInbound(x, y, 1))
	{
		n = normal;
		b = D3DXVECTOR3(1.f, 0.f, 0.f);
		t = D3DXVECTOR3(0.f, 0.f, 1.f);
		return;
	}

	float ratioW = m_mapSize.x / m_heightNormalLayer.getW(); 
	float ratioH = m_mapSize.y / m_heightNormalLayer.getH();

	auto VectorAt = [&ratioW, &ratioH, this]( Index x, Index y) 
	{ return D3DXVECTOR3( x * ratioW, m_heightNormalLayer.get( x, y ).x, y * ratioH);};


	// Normal
	n = getNormalOf( x, y );
	// Tangent 
	float coef = 1/ (tex1u * tex2v - tex2u * tex1v);

	D3DXVECTOR3 v1 = VectorAt(x+1, y) - VectorAt(x-1, y);
	D3DXVECTOR3 v2 = VectorAt(x, y+1) - VectorAt(x, y-1);

	t.x = coef * ((v1.x * tex2v)  + (v2.x * -tex1v));
	t.z = coef * ((v1.y * tex2v)  + (v2.y * -tex1v));
	t.y = coef * ((v1.z * tex2v)  + (v2.z * -tex1v));

	// Bitangent (Binormal) 
	t = Math::normalize(t);

	D3DXVec3Cross(&b, &n, &t);
	b.z = -b.z;
	b = Math::normalize(b);
}

//#################################################################################################

D3DXVECTOR3 MapModel::getNormalOf( const AttributeTool::Index x, const AttributeTool::Index y )
{
	if (!m_heightNormalLayer.isInbound(x, y, 1))
	{
		return D3DXVECTOR3(0.f, 1.f, 0.f);
	}

	D3DXVECTOR3 n;

#if NORMAL_ACCURATE
	//  normal calculation ( accurate )
	D3DXVECTOR3 center = VectorAt( x, y );

	Index smallestX = std::min( x-1, Index(0) );
	Index smallestY = std::min( y-1, Index(0) );
	Index largestX = std::min( x+1, m_heightNormalLayer.getW() - 1);
	Index largestY = std::min( y+1, m_heightNormalLayer.getH() - 1);

	// get all directions (edges may not be precise)
	for( Index nx = smallestX; nx <= largestX; nx++)
	for( Index ny = smallestY; ny <= largestY; ny++)
	{
		normal += Math::normalize( getVertexAt( nx, ny ) - center );
	}

	// with cross products:

	D3DXVECTOR3 va = Vector::normalize(vec3(size.xy,s21-s11));
    D3DXVECTOR3 vb = Vector::normalize(vec3(size.yx,s12-s10));
    D3DXVECTOR3 bump = vec4( cross(va,vb), s11 );
#endif

#if NORMAL_CROSS
//	X  - N1 - X
//	|    |    |
//	N2 - P  - N3
//	|    |    |
//	X  - N4 - X 
//	normal (P) = (N2 - N3) x (N1 - N4)
	D3DXVec3Cross( &n, &(getVertexAt(x-1, y) - getVertexAt(x+1, y)), &(getVertexAt(x, y+1) - getVertexAt(x, y-1)) ); 
	n.x *= 10.f;
	n.y /= 10.f;
	n.z *= 10.f;

#endif
#if NORMAL_FAST

	// fast normal calculation ( inaccurate, but ok )
	float sx =m_heightNormalLayer.get( x+1, y ).x - m_heightNormalLayer.get( x-1, y ).x;
	float sy =m_heightNormalLayer.get( x, y+1 ).x - m_heightNormalLayer.get( x, y-1 ).x;

	float xScale = 20.0f;
	float yScale = 20.0f;
    n = D3DXVECTOR3(-sx * xScale, 1, sy * yScale);
    
	n = Math::normalize(n);
#endif

	D3DXVec3Normalize( &n, &n );
	return n;
}

//#################################################################################################

D3DXVECTOR3 MapModel::getVertexAt(const AttributeTool::Index x, const AttributeTool::Index y)
{
	static const float ratioW = m_mapSize.x / m_heightNormalLayer.getW(); 
	static const float ratioH = m_mapSize.y / m_heightNormalLayer.getH();

	return D3DXVECTOR3( x * ratioW, m_heightNormalLayer.get( x, y ).x, y * ratioH);
}

//#################################################################################################

void MapModel::getListIndex( DX::IndexList& indices )
{
	int w = m_heightNormalLayer.getW();
	int h = m_heightNormalLayer.getH();

	indices.clear();
	indices.resize( getIndexCount() );

	for (int y = 0; y < h - 1; y++)
	{
		for (int x = 0; x < w - 1; x++)
		{
			int index = x + y * w ;
			int pos = (x + y * (w - 1)) * 6;

			indices[pos++]	=	( index );
			indices[pos++]	=	( index + w );
			indices[pos++]	=	( index + w + 1 );

			indices[pos++]	=	( index + w + 1 );
			indices[pos++]	=	( index + 1 );
			indices[pos++]	=	( index );
		}
	}	
}
//#################################################################################################

int MapModel::getIndexCount() const
{
	return (m_heightNormalLayer.getW()-1) * (m_heightNormalLayer.getH()-1) * 6;
}
//#################################################################################################

bool MapModel::mergeLayers( 
	AttributeTool::AttributeLayer< D3DXVECTOR4 >& contribution, 
	AttributeTool::AttributeLayer< D3DXVECTOR4 >& indices )
{
	// get first layer size
	const unsigned attrSize = m_attributeMap[0].count(); 

	contribution.reallocate( m_attributeMap[0].getW(), m_attributeMap[0].getH() );
	indices.reallocate( m_attributeMap[0].getW(), m_attributeMap[0].getH() );

	for ( unsigned i = 0; i < attrSize; ++i )
	{
		// layers: a, b, c, d
		// prioritize the first layer
		// for now, merge layers simply
		for ( unsigned dim = 0; dim < m_attributeMap.size(); ++dim)
		{
			contribution[i][dim] = m_attributeMap[dim][i];	
			indices[i][dim] = dim;	
		}
	}

	return true;
}
//#################################################################################################

bool MapModel::sendAttributesToDevice(  )
{
	AttributeTool::AttributeLayer< D3DXVECTOR4 > indices;
	mergeLayers(m_attributeLayer, indices);
	return	m_attributeTexture.fillFromLayer( m_attributeLayer ) && 
			m_indexTexture.fillFromLayer( indices )	&&	
			m_heightTexture.fillFromLayer( m_heightNormalLayer ) &&
			m_tangentTexture.fillFromLayer( m_tangentLayer );
}

//#################################################################################################

ID3D10ShaderResourceView* MapModel::getAttributeResourceView() const
{
	return m_attributeTexture.getResourceView();
}
//#################################################################################################

ID3D10ShaderResourceView* MapModel::getIndexResourceView() const
{
	return m_indexTexture.getResourceView();
}
//#################################################################################################

ID3D10ShaderResourceView* MapModel::getHeightMapResourceView() const
{
	return m_heightTexture.getResourceView();
}
//#################################################################################################

ID3D10ShaderResourceView* MapModel::getTangentMapResourceView() const
{
	return m_tangentTexture.getResourceView();
}

//#################################################################################################

void MapModel::generateHeightMapFromLayers(  )
{
	AttributeLayer<float> perlin( m_heightNormalLayer.getW(), m_heightNormalLayer.getH());
	
	AttributePainter<float> ap(perlin);
	ap.fillPerlin();
	perlin.saturate(0.f, 1.f);

	float rateX = m_attributeMap[0].getW() / (float) m_heightNormalLayer.getW() ;
	float rateY = m_attributeMap[0].getH() / (float) m_heightNormalLayer.getH() ;

	Concurrency::parallel_for( 0u, m_heightNormalLayer.getH() * m_heightNormalLayer.getW(), 
		[this, rateX, rateY, perlin]( const unsigned index)
		{
			const unsigned x = index % m_heightNormalLayer.getH();
			const unsigned y = index / m_heightNormalLayer.getW();

			float height = 0.0f;
			const Index attx = x * rateX;
			const Index atty = y * rateY;
		
			for( unsigned  heightIndex = 0; heightIndex < m_heightContribution.size(); ++heightIndex )
			{
				if ( Math::feq( m_heightContribution[heightIndex], 0.0f) )
				{
					continue;
				}

				const float attr = m_attributeMap[heightIndex].get( attx, atty );
				if ( attr > 0.f )
				{
					// land area
					//att3 = std::min(att3, 0.2f);
					height += attr * m_heightContribution[heightIndex] ;	
				}

				//assert(height > -0.0001f);
				//assert(height <  1.0001f);
			}

			const float perlinVal = perlin.get( x, y );
			height +=  0.2f * perlinVal;
			m_heightNormalLayer.set( x, y, D3DXVECTOR4(height, 0.f, 0.f, 0.f) );
		}
	);

	Concurrency::parallel_for( 0u, m_heightNormalLayer.getH() * m_heightNormalLayer.getW(), 
		[this, rateX, rateY, perlin]( const unsigned index)
	{
		const unsigned x = index % m_heightNormalLayer.getH();
		const unsigned y = index / m_heightNormalLayer.getW();

		D3DXVECTOR3 normal;
		D3DXVECTOR3 tangent;
		D3DXVECTOR3 binormal;

		getNBTOf( x, y, 
			1 / m_textureFactor, 0, 
			0, 1 / m_textureFactor,
			normal, binormal, tangent );

		D3DXVECTOR4 v = m_heightNormalLayer.get(x, y);
		v.y = normal.x;
		v.z = normal.y;
		v.w = normal.z;
		m_heightNormalLayer.set(x, y, v);
		m_tangentLayer.set( x, y, tangent);
	});
}

//#################################################################################################

void MapModel::paintAttributeLayer( const TerrainTools::PaintBrush& paintBrush, const float x, const float y )
{
	if ( paintBrush.Layer == 4) // yes, 4
	{
		paintGeometryLayer( paintBrush, x, y );
		return;
	}

	D3DXVECTOR4 brush( 0.f, 0.f, 0.f, 0.f );
	brush[paintBrush.Layer] = paintBrush.BrushType.Opacity;
	m_brushPainter.drawSpot( x, y, paintBrush.BrushType.Size, paintBrush.BrushType.Bias, brush, true );
 
	D3D10_BOX region;
	region.left =	std::max( 0.f, x - paintBrush.BrushType.Size );
	region.right =  std::min( x + paintBrush.BrushType.Size, (float)m_brushPainter.getLayer().getW() - 1 ) ;
	region.top =	std::max( 0.f, y - paintBrush.BrushType.Size );
	region.bottom = std::min( y + paintBrush.BrushType.Size, (float)m_brushPainter.getLayer().getH() - 1 ) ;
	region.back =	1;
	region.front =	0;

	m_attributeTexture.update( std::move(region), m_brushPainter.getLayer() );
}

//#################################################################################################

void MapModel::paintGeometryLayer( const TerrainTools::PaintBrush& paintBrush, const float x, const float y )
{
	// TODO : move the re-scaling layer-brush stuff to some data structure that does this automatically
	// needs some re-scaling...
	float scaledX = Math::rangedScale1D( m_attributeLayer.getW(), m_heightNormalLayer.getW(), x );
	float scaledY = Math::rangedScale1D( m_attributeLayer.getH(), m_heightNormalLayer.getH(), y );
	float scaledBrushSize = paintBrush.BrushType.Size * m_heightNormalLayer.getW() / (float)m_attributeLayer.getW();
	// TODO : copy heaven!
	D3DXVECTOR4 brush( 0.f, 0.f, 0.f, 0.f );
	// TODO : tweaked opacity value!
	brush[0] = paintBrush.BrushType.Opacity / 10.f;
	m_brushGeometryPainter.drawSpot( scaledX, scaledY, scaledBrushSize, paintBrush.BrushType.Bias, brush, true );

	D3D10_BOX region;
	region.left =	std::max( 0.f, scaledX - scaledBrushSize );
	region.right =  std::min( scaledX + scaledBrushSize, (float)m_brushGeometryPainter.getLayer().getW() - 1 ) ;
	region.top =	std::max( 0.f, scaledY - scaledBrushSize );
	region.bottom = std::min( scaledY + scaledBrushSize, (float)m_brushGeometryPainter.getLayer().getH() - 1 ) ;
	region.back =	1;
	region.front =	0;

	// calculate new normals
	const unsigned xSteps = region.right - region.left;
	const unsigned ySteps = region.bottom - region.top;
	Concurrency::parallel_for( 0u, xSteps * ySteps, 
		[this, &region, &xSteps](const unsigned i)
		{
			const unsigned x = region.left  + i % xSteps;
			const unsigned y = region.top	+ i / xSteps;

			D3DXVECTOR3 n  = getNormalOf(x, y);
			D3DXVECTOR4 hn =  m_heightNormalLayer.get(x,y);
			hn.y = n.x;
			hn.z = n.y;
			hn.w = n.z;
			m_heightNormalLayer.set(x, y, hn);
		}
	);

	m_heightTexture.update( std::move(region), m_heightNormalLayer);
}
 
//#################################################################################################

void	MapModel::fuckUpTexture()
{
	//unsigned char f = Converter::floatToByte(0.3f);
	//m_attributeLayer.saveTofile( "C:\\aa\\ide.bmp" ); 
}
