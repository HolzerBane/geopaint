#pragma once
#include "AttributeTypes.h"
#include "AttributeLayer.h"
#include <functional>
#include <array>
#include <vector>
#include "AttributeTools.h"

namespace AttributeTool
{


template < class TValue >
class AttributePainter
{
public:

	AttributePainter( AttributeLayer< TValue >& );

	void drawSpot( const Index x, const Index y, const unsigned radius, const float bias, const TValue& value, const bool additive = false );
	void fillRegion( const Index x1, const Index y1, const Index x2, const Index y2, const TValue& value );
	void fillPerlin( const Index x1, const Index y1, const Index x2, const Index y2 );
	void fillPerlin( );

	void makeRidges( const Coordinates& coords, const unsigned size );	// connects points and generates ridges
	void makePeaks( const Coordinates& coords, const unsigned size );	// start particle streams at points
	void erode();

	const AttributeLayer< TValue >& getLayer() const { return m_usedLayer; }

	// hide this cctor...
	AttributePainter( const AttributePainter< TValue>& );

private:

	struct GaussCache
	{
		unsigned radius;
		float bias;
		AttributeLayer<float> data;
	};

	GaussCache m_gaussCache;

	void generateGaussCache( const unsigned radius, const float bias );
	
	struct Node
	{
		Node( const Coordinate2D& p, const float& v, const unsigned l, unsigned bl, const Dir& d )
			: pos(p),value(v), life(l), branchesLeft(bl)
		{ 
			branches.push_back( d );
		}

		void addBranch( const Dir& d )
		{
			branches.push_back( d );
		}
		
		Coordinate2D		pos;
		float				value;
		unsigned			life;
		unsigned			branchesLeft;
		std::vector< Dir >	branches;
	};
	
	void spread( const Coordinate2D& coord, const unsigned size );
	void spread( Node& n, unsigned size );
	unsigned m_stateVar;
	Dir diverge( const Dir& input, unsigned d ) const { return ( AttributeTool::Dir)((input + ( rand() % d - d / 2 ) ) % AttributeTool::DirCount); }
	unsigned neighborsWithProp( const Coordinate2D,  std::function< bool(const TValue&) >, unsigned );


	AttributeLayer< TValue >& m_usedLayer;

};

}