////////////////////////////////////////////////////////////////////////////////
// Filename: TerrainShader.cpp
////////////////////////////////////////////////////////////////////////////////
#include "TerrainShader.h"
#include "MapModel.h"
#include "modelclass.h"
#include "textureClass.h"

#include "Utility\CDLOD\CDLODQuadTree.h"
#include "Utility\CDLOD\RegularGrid.h"
#include "Utility\FreeCamera.h"
#include "Utility\ReportError.h"

#include <assert.h>
#include <sstream>

TerrainShader::TerrainShader()
{
	m_worldMatrix		= nullptr;
	m_viewMatrix		= nullptr;
	m_projMatrix		= nullptr;

	/* CDLOD vars */
	m_morphLevels			= nullptr;
	m_quadOffset			= nullptr;
	m_quadScale				= nullptr;
	m_quadWorldMax			= nullptr;
}


TerrainShader::TerrainShader(const TerrainShader& other)
{
}

TerrainShader::~TerrainShader()
{
}

void TerrainShader::setModelParameters( TerrainModel::MapModel& model)
{
	
}

void TerrainShader::shutdownShader()
{
	// Release the pointers to the matrices inside the shader.
	m_worldMatrix = 0;
	m_viewMatrix = 0;
	m_projMatrix = 0;


	std::for_each( m_perFrameEffectVars.begin(), m_perFrameEffectVars.end(), 
		[]( Utils::IEffectVariable * var )
		{
			if ( var )
			{
				var->free();
				delete var;
				var = nullptr;
			}
		}
		);

	std::for_each( m_constEffectVars.begin(), m_constEffectVars.end(), 
		[]( Utils::IEffectVariable * var )
		{
			if ( var )
			{
				var->free();
				delete var;
				var = nullptr;
			}
		}
		);

	return;
}

void TerrainShader::setInputParams( 
		  const bool showNormals
		, const FreeCamera& camera 
		)
{
	const D3DXVECTOR3& cameraPos = camera.getEyePosition();
	SetFloatArray( m_cameraPos, cameraPos.x, cameraPos.y, cameraPos.z );
	SetFloat( m_viewDistance, camera.getViewDistance() );
	SetBool( m_renderwNormalMaps, showNormals );
}

void TerrainShader::SetPerFrameShaderParameters( )
{
	std::for_each( m_perFrameEffectVars.begin(), m_perFrameEffectVars.end(), 
		[]( Utils::IEffectVariable * var )
		{
			assert(var);
			var->set();
		}
		);

	return;
	
}

void TerrainShader::SetConstShaderParameters( )
{
	std::for_each( m_constEffectVars.begin(), m_constEffectVars.end(), 
		[]( Utils::IEffectVariable * var )
		{
			assert(var);
			var->set();
		}
		);

	return;
	
}

void TerrainShader::setCDLODParameters( const CDLODQuadTree & cdlodQuadTree, const RegularGrid<VertexType>& gridMesh )
{
	int textureWidth  = cdlodQuadTree.GetRasterSizeX();
	int textureHeight = cdlodQuadTree.GetRasterSizeY();

	const float maxTextureTile  = 16.f; 
	const MapDimensions & mapDims = cdlodQuadTree.GetWorldMapDims();
	const float detailRatio = std::max(mapDims.SizeX, mapDims.SizeY) / maxTextureTile;

	SetFloatArray( m_detailRatio, detailRatio, detailRatio );
	SetFloatArray( m_quadWorldMax, mapDims.MaxX(), mapDims.MaxY() );
	SetFloatArray( m_terrainScale, mapDims.SizeX, mapDims.SizeZ, mapDims.SizeY, 0.f );
	SetFloatArray( m_terrainOffset, mapDims.MinX, mapDims.MinZ, mapDims.MinY, 0.f );
	SetFloatArray( m_samplerWorldToTextureScale, (textureWidth-1.0f) / (float)textureWidth, (textureHeight-1.0f) / (float)textureHeight );
	SetFloatArray( m_heightmapTextureInfo,  (float)textureWidth, (float)textureHeight, 1.0f / (float)textureWidth, 1.0f / (float)textureHeight );
	SetFloatArray( m_gridDim, (float)gridMesh.GetDimensions(), gridMesh.GetDimensions() * 0.5f, 2.0f / gridMesh.GetDimensions(), 0.0f );		
}

void TerrainShader::setQuadRenderingParameters( const D3DXVECTOR4& offset,  const D3DXVECTOR4& scale )
{
	SetVector( m_quadOffset,offset );
	SetVector( m_quadScale,	scale );
}

bool TerrainShader::initializeTechniques(  ID3D10Device* device )
{
	const D3D10_INPUT_ELEMENT_DESC polygonLayout[] =
    {
		{	"POSITION",	0,	DXGI_FORMAT_R32G32B32A32_FLOAT,	0,	D3D10_APPEND_ALIGNED_ELEMENT,	D3D10_INPUT_PER_VERTEX_DATA,	0 },
		{	"TEXCOORD", 0,	DXGI_FORMAT_R32G32B32A32_FLOAT,	0,	D3D10_APPEND_ALIGNED_ELEMENT,	D3D10_INPUT_PER_VERTEX_DATA,	0 },
		{	"NORMAL",	0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	D3D10_APPEND_ALIGNED_ELEMENT,	D3D10_INPUT_PER_VERTEX_DATA,	0 },
		{	"BINORMAL",	0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	D3D10_APPEND_ALIGNED_ELEMENT,	D3D10_INPUT_PER_VERTEX_DATA,	0 },
		{	"TANGENT",	0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	D3D10_APPEND_ALIGNED_ELEMENT,	D3D10_INPUT_PER_VERTEX_DATA,	0 },
	};

	return addTechnique( device, "AlphaMapTechnique", polygonLayout, sizeof(polygonLayout) / sizeof(polygonLayout[0]) );
}

void TerrainShader::bindEffectVars( const ModelClass& model )
{

	m_perFrameEffectVars.fill( nullptr );
	m_constEffectVars.fill( nullptr );
	
	unsigned index = 0;
	
	// TODO : this may not be a good idea after all
	// TODO : the passed strings cause memory leaks (..?)
	m_perFrameEffectVars[index++] = new Utils::EffectVariable< ID3D10EffectMatrixVariable*, float* >( m_effect, "worldMatrix",		(float*)&model.getWorldMatrix() );
	m_perFrameEffectVars[index++] = new Utils::EffectVariable< ID3D10EffectMatrixVariable*, float* >( m_effect, "viewMatrix",		(float*)&model.getViewMatrix() );
	m_perFrameEffectVars[index++] = new Utils::EffectVariable< ID3D10EffectMatrixVariable*, float* >( m_effect, "projectionMatrix", (float*)&model.getProjMatrix() );
	// params are needed for now ;<
	m_perFrameEffectVars[index++] = new Utils::EffectVariable< ID3D10EffectVariable*, Utils::RawData >( m_effect, "g_lightPos", Utils::RawData( (void*)&model.getLightPos(), 0, sizeof(D3DXVECTOR3) ) );
	
	// brush projection
	m_perFrameEffectVars[index++] = new Utils::EffectVariable< ID3D10EffectMatrixVariable*, float* >( m_effect, "brush_projection",	(float*)&model.getBrushProjMatrix() );
	m_perFrameEffectVars[index++] = new Utils::EffectVariable< ID3D10EffectMatrixVariable*, float* >( m_effect, "brush_view",		(float*)&model.getBrushViewMatrix() );
	m_perFrameEffectVars[index++] = new Utils::EffectVariable<>( m_effect, "brush_projectionTexture",		model.GetBrush() );
	m_perFrameEffectVars[index++] = new Utils::EffectVariable< ID3D10EffectVariable*, Utils::RawData >( m_effect, "brush_position", Utils::RawData( (void*)&model.getCursorPosition(), 0, sizeof(D3DXVECTOR3) ) );

	assert(index == PerFrameEffectVarCount);
	index = 0;

	m_constEffectVars[index++] = new Utils::EffectVariable<>( m_effect, "perlinTexture", model.GetPerlin() );
	m_constEffectVars[index++] = new Utils::EffectVariable<>( m_effect, "diffuseArray",	model.GetTextureArray() );
	m_constEffectVars[index++] = new Utils::EffectVariable<>( m_effect, "normalArray",	model.getTextureClass()->GetNormalArray()  );
	
	m_constEffectVars[index++] = new Utils::EffectVariable<>( m_effect, "attributeMap",		model.getMapModel().getAttributeResourceView() );
	m_constEffectVars[index++] = new Utils::EffectVariable<>( m_effect, "indexMap",			model.getMapModel().getIndexResourceView() );
	m_constEffectVars[index++] = new Utils::EffectVariable<>( m_effect, "vertexHeightMap",	model.getMapModel().getHeightMapResourceView() );
	m_constEffectVars[index++] = new Utils::EffectVariable<>( m_effect, "vertexTangentMap",	model.getMapModel().getTangentMapResourceView() );
	
	assert(index == ConstEffectVarCount);
		

	m_cameraPos			= m_effect->GetVariableByName("cameraPos");
	m_renderwNormalMaps = m_effect->GetVariableByName("g_RenderWithNormalMaps");

	m_morphLevels	= m_effect->GetVariableByName("morphLevels");
	m_quadOffset	= m_effect->GetVariableByName("quadOffset");
	m_quadScale		= m_effect->GetVariableByName("quadScale");
	m_quadWorldMax	= m_effect->GetVariableByName("quadWorldMax");
	m_gridDim		= m_effect->GetVariableByName("gridDim");
	m_detailRatio	= m_effect->GetVariableByName("detailRatio");
	m_viewDistance	= m_effect->GetVariableByName("viewDistance");

	m_heightmapTextureInfo			= m_effect->GetVariableByName("heightmapTextureInfo");
	m_terrainScale					= m_effect->GetVariableByName("terrainScale"); 
	m_terrainOffset					= m_effect->GetVariableByName("terrainOffset"); 
	m_samplerWorldToTextureScale	= m_effect->GetVariableByName("samplerWorldToTextureScale"); 

	m_worldMatrix	= m_effect->GetVariableByName( "worldMatrix" )->AsMatrix();		
	m_viewMatrix	= m_effect->GetVariableByName(  "viewMatrix" )->AsMatrix();		
	m_projMatrix	= m_effect->GetVariableByName(  "projectionMatrix" )->AsMatrix();

}
