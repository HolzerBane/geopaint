#pragma once
#include <d3dx10math.h>

struct VertexType
{
	D3DXVECTOR4 pos;
	D3DXVECTOR4 tex;
};