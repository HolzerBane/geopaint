////////////////////////////////////////////////////////////////////////////////
// Filename: TextureClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "TextureClass.h"
//#include "AttributeLayer.hpp"
#include "AttributePainter.hpp"
#include "Paths.h"

#define CONSOLE_ON
#include "Utility\DebugConsole.h"
#include "Utility\ReportError.h"
#include "Utility\File.h"
#include "Utility\Memory.h"

//#include <d3dx10.h>
// only this is needed:
#define D3DX_FROM_FILE          ((UINT) -3)

#include <assert.h>
#include <ppl.h>

TextureClass::TextureClass()
{

	m_texturePtr	= nullptr;
	m_textureArray	= nullptr;

	m_normalPtr		= nullptr;
	m_normalArray	= nullptr;

	m_enviroPtr		= nullptr;
	m_enviroment	= nullptr;
}

//#################################################################################################

TextureClass::TextureClass(const TextureClass& other)
{
}

//#################################################################################################

TextureClass::~TextureClass()
{
}

//#################################################################################################

bool TextureClass::Initialize(ID3D10Device* device)
{
	m_device = device;

	DebugConsole::scoped_trace st( L"TextureClass" );
	 
	bool ok = loadPerlin( 1024 );
	ok = ok && loadDefaultTextures();

	return ok;
}
//#################################################################################################

bool TextureClass::loadTextureArray( const std::vector< std::wstring >& textureNames, ID3D10Texture2D** ppTex2D, ID3D10ShaderResourceView** ppSRV )
{
    HRESULT hr = S_OK;
    D3D10_TEXTURE2D_DESC desc;
    ZeroMemory( &desc, sizeof(D3D10_TEXTURE2D_DESC) );
    
	for(unsigned i = 0; i < textureNames.size(); i++)
    {
		const std::wstring& str = textureNames[i];

        ID3D10Resource *pRes = NULL;
        D3DX10_IMAGE_LOAD_INFO loadInfo;
        ZeroMemory( &loadInfo, sizeof( D3DX10_IMAGE_LOAD_INFO ) );
        loadInfo.Width = D3DX_FROM_FILE;
        loadInfo.Height  = D3DX_FROM_FILE;
        loadInfo.Depth  = D3DX_FROM_FILE;
        loadInfo.FirstMipLevel = 0;
        loadInfo.MipLevels = D3DX_FROM_FILE;
        loadInfo.Usage = D3D10_USAGE_STAGING;
        loadInfo.BindFlags = 0;
        loadInfo.CpuAccessFlags = D3D10_CPU_ACCESS_WRITE | D3D10_CPU_ACCESS_READ;
        loadInfo.MiscFlags = 0;
        loadInfo.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        loadInfo.Filter = D3DX10_FILTER_NONE;
        loadInfo.MipFilter = D3DX10_FILTER_NONE;

		HRESULT hr = D3DX10CreateTextureFromFile( m_device, str.c_str(), &loadInfo, NULL, &pRes, &hr );
		assert( hr == S_OK );
        if( pRes )
        {
            ID3D10Texture2D* pTemp;
            pRes->QueryInterface( __uuidof( ID3D10Texture2D ), (LPVOID*)&pTemp );
            pTemp->GetDesc( &desc );

            D3D10_MAPPED_TEXTURE2D mappedTex2D;
            if(DXGI_FORMAT_R8G8B8A8_UNORM != desc.Format)   //make sure we're R8G8B8A8
                return false;

            if(!(*ppTex2D))
            {
                desc.Usage = D3D10_USAGE_DEFAULT;
                desc.BindFlags = D3D10_BIND_SHADER_RESOURCE;
                desc.CPUAccessFlags = 0;
				desc.ArraySize = textureNames.size();
                hr = (m_device->CreateTexture2D( &desc, NULL, ppTex2D));
				assert( hr == S_OK );
            }

            for(UINT iMip=0; iMip < desc.MipLevels; iMip++)
            {
                pTemp->Map( iMip, D3D10_MAP_READ, 0, &mappedTex2D );

                m_device->UpdateSubresource( (*ppTex2D), 
                    D3D10CalcSubresource( iMip, i, desc.MipLevels ),
                    NULL,
                    mappedTex2D.pData,
                    mappedTex2D.RowPitch,
                    0 );
				ReportError::Instance().CheckError();
                pTemp->Unmap( iMip );
            }

			TerrainModel::Memory::Release( pRes );
            TerrainModel::Memory::Release( pTemp );
        }
        else
        {
            return false;
        }
    }

    D3D10_SHADER_RESOURCE_VIEW_DESC SRVDesc;
    ZeroMemory( &SRVDesc, sizeof(SRVDesc) );
    SRVDesc.Format = desc.Format;
    SRVDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2DARRAY;
    SRVDesc.Texture2DArray.MipLevels = 6;//desc.MipLevels;
	SRVDesc.Texture2DArray.ArraySize = textureNames.size();
    hr = m_device->CreateShaderResourceView( *ppTex2D, &SRVDesc, ppSRV );
	assert( hr == S_OK );

	ReportError::Instance().CheckError();

    return hr == S_OK;
}

//#################################################################################################

bool TextureClass::loadCubeTex( const std::wstring& szTextureName, ID3D10Texture2D **ppTexture10, ID3D10ShaderResourceView **ppSRV10)
{
    HRESULT hr;

	ID3D10Resource *pRes = NULL;
	hr = D3DX10CreateTextureFromFile( m_device, szTextureName.c_str(), NULL, NULL, &pRes, &hr );
	ReportError::Instance().CheckError();
	if( SUCCEEDED(hr) && pRes )
	{
		pRes->QueryInterface( __uuidof( ID3D10Texture2D ), (LPVOID*)ppTexture10 );
		D3D10_TEXTURE2D_DESC desc;
		(*ppTexture10)->GetDesc( &desc );
		D3D10_SHADER_RESOURCE_VIEW_DESC SRVDesc;
		ZeroMemory( &SRVDesc, sizeof(SRVDesc) );
		SRVDesc.Format = desc.Format;
		if(desc.ArraySize == 6)
		{
			SRVDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURECUBE;
			SRVDesc.TextureCube.MipLevels = desc.MipLevels;
		}
		else
		{
			SRVDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2D;
			SRVDesc.Texture2D.MipLevels = desc.MipLevels;
		}
		m_device->CreateShaderResourceView( *ppTexture10, &SRVDesc, ppSRV10 );
		ReportError::Instance().CheckError();
		TerrainModel::Memory::Release( pRes );
	}

	else
	{
		return false;
	}
	return true;
}

//#################################################################################################

bool TextureClass::loadCubeMapFromDDS( const std::wstring& name, ID3D10Texture2D **ppTexture10, ID3D10ShaderResourceView **ppSRV10 )
{
	HRESULT hr;


	ID3D10Resource *pRes = NULL;
	hr = D3DX10CreateTextureFromFile( m_device, name.c_str(), NULL, NULL, &pRes,&hr );
	ReportError::Instance().CheckError();
	if( SUCCEEDED(hr) && pRes )
	{
		pRes->QueryInterface( __uuidof( ID3D10Texture2D ), (LPVOID*)ppTexture10 );
		D3D10_TEXTURE2D_DESC desc;
		(*ppTexture10)->GetDesc( &desc );
		D3D10_SHADER_RESOURCE_VIEW_DESC SRVDesc;
		ZeroMemory( &SRVDesc, sizeof(SRVDesc) );
		SRVDesc.Format = desc.Format;
		SRVDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURECUBE;
		SRVDesc.TextureCube.MipLevels = desc.MipLevels;
		m_device->CreateShaderResourceView( *ppTexture10, &SRVDesc, ppSRV10 );
		ReportError::Instance().CheckError();
		TerrainModel::Memory::Release( pRes );

		//m_pEnvMapTexVar->SetResource(pSRV10);
		//SAFE_RELEASE(pTexture10);
		//SAFE_RELEASE(pSRV10);
	}

	return true;
}

//#################################################################################################

bool TextureClass::loadPerlin( const unsigned perlinSize )
{
	// create perlin texture manually
	D3D10_TEXTURE2D_DESC desc;

	const unsigned texSize = perlinSize;
	desc.BindFlags = D3D10_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	desc.Usage = D3D10_USAGE_DYNAMIC;
	desc.MiscFlags = 0;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Width = texSize;
	desc.Height = texSize;
	desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;

	AttributeTool::AttributeLayer<float> perlinesdi(perlinSize, perlinSize);
	AttributeTool::AttributePainter<float> ap(perlinesdi);
	ap.fillPerlin();

	D3DXVECTOR4* tex = new D3DXVECTOR4[texSize*texSize];
	size_t ff= sizeof(D3DXVECTOR4);
	unsigned texIndex = 0;
	for( unsigned i = 0; i < (perlinSize)*(perlinSize); ++i)
	{
		//if ( perlinesdi.isInboundIndex(i, 1) )
		//{
			assert( texIndex < texSize*texSize );
			float val = perlinesdi.get( i );
			tex[texIndex].x = val;
			tex[texIndex].y = val;
			tex[texIndex].z = val;
			tex[texIndex].w = 1.0f;
			texIndex++;
		//}
	}

	D3D10_SUBRESOURCE_DATA resData;
	resData.pSysMem = (void*)tex;
	resData.SysMemPitch = texSize * sizeof(D3DXVECTOR4);
	resData.SysMemSlicePitch = 0;

	ID3D10Texture2D *pTexture = NULL;
	HRESULT res = m_device->CreateTexture2D( &desc, &resData, &pTexture );
	unsigned subresource = 0;
	
	D3D10_SHADER_RESOURCE_VIEW_DESC resDesc;
	resDesc.Format = desc.Format;
	resDesc.ViewDimension = D3D_SRV_DIMENSION_TEXTURE2D;
	resDesc.Texture2D.MipLevels = desc.MipLevels;
	resDesc.Texture2D.MostDetailedMip = 0;

	HRESULT result = m_device->CreateShaderResourceView( pTexture, &resDesc, &m_perlin);

	delete[] tex;

	return result == S_OK;
}

//#################################################################################################

bool TextureClass::loadDefaultTextures()
{
	bool ok = true;

	std::vector< std::wstring > textureFiles;
	/*pTextureFiles.push_back( L"..\\Engine\\Data\\rock10c.dds"	);
	textureFiles.push_back( L"..\\Engine\\Data\\grass10c.dds"	);
	textureFiles.push_back( L"..\\Engine\\Data\\dirtb10c.dds"	);
	textureFiles.push_back( L"..\\Engine\\Data\\sand10c.dds"	);*/
	textureFiles.push_back( Paths::HiResTextures + L"rock1024.dds"	);
	textureFiles.push_back( Paths::HiResTextures + L"grass1024.dds"	);
	textureFiles.push_back( Paths::HiResTextures + L"dirt1024.dds"	);
	textureFiles.push_back( Paths::HiResTextures + L"sand1024.dds"	);
	ok = ok && loadTextureArray( textureFiles, &m_texturePtr, &m_textureArray );

	textureFiles.clear();
	/*textureFiles.push_back( L"..\\Engine\\Data\\rock10c_n.dds"	);
	textureFiles.push_back( L"..\\Engine\\Data\\grass10c_n.dds");
	textureFiles.push_back( L"..\\Engine\\Data\\dirtb10c_n.dds"	);
	textureFiles.push_back( L"..\\Engine\\Data\\sand10c_n.dds"	);*/
	textureFiles.push_back( Paths::HiResTextures + L"rock1024n.dds"	);
	textureFiles.push_back( Paths::HiResTextures + L"grass1024n.dds" );
	textureFiles.push_back( Paths::HiResTextures + L"dirt1024n.dds"	);
	textureFiles.push_back( Paths::HiResTextures + L"sand1024n.dds"	);
	ok = ok && loadTextureArray( textureFiles, &m_normalPtr, &m_normalArray );
	ok = ok && loadCubeMapFromDDS(  Paths::Textures + L"SkyCubemap.dds", &m_enviroPtr, &m_enviroment );

	return ok;
}
//#################################################################################################

bool TextureClass::loadTextures( std::vector< std::wstring >&& textureFiles )
{
	bool ok = loadTextureArray( textureFiles, &m_texturePtr, &m_textureArray );

	for( unsigned index = 0; index < textureFiles.size(); ++index )
	{
		textureFiles[index] = File::addPrefix( textureFiles[index], L"n" );
	}

	ok = ok && loadTextureArray( textureFiles, &m_normalPtr, &m_normalArray );

	return ok;
}
//#################################################################################################

bool TextureClass::createBrush( const unsigned size)
{
	D3D10_TEXTURE2D_DESC desc;

	desc.BindFlags = D3D10_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	desc.Usage = D3D10_USAGE_DYNAMIC;
	desc.MiscFlags = 0;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Width = size;
	desc.Height = size;
	desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;

	D3D10_SHADER_RESOURCE_VIEW_DESC resDesc;
	resDesc.Format = desc.Format;
	resDesc.ViewDimension = D3D_SRV_DIMENSION_TEXTURE2D;
	resDesc.Texture2D.MipLevels = desc.MipLevels;
	resDesc.Texture2D.MostDetailedMip = 0;

	D3DXVECTOR4* brush = new D3DXVECTOR4[size*size];
	ID3D10Texture2D *pTexture = NULL;
	D3D10_SUBRESOURCE_DATA resData;
	resData.pSysMem = (void*)brush;
	resData.SysMemPitch = size * sizeof(D3DXVECTOR4);
	resData.SysMemSlicePitch = 0;
	
	AttributeTool::AttributeLayer<float> roundBrush(size, size);
	AttributeTool::AttributePainter<float> ap(roundBrush);
	ap.drawSpot( size / 2, size / 2, size / 2 , 3.f, 1.f);

	Concurrency::parallel_for( 0u, size*size, [&roundBrush, &brush](const unsigned i)
	{
		float val = roundBrush.get( i );
		brush[i].x = val;
		brush[i].y = val;
		brush[i].z = val;
		brush[i].w = 1.0f;
	}
	);

	HRESULT result	= m_device->CreateTexture2D( &desc, &resData, &pTexture );
	result			= m_device->CreateShaderResourceView( pTexture, &resDesc, &m_brush);
	
	delete[] brush;

	ReportError::Instance().CheckError();
	return result == S_OK;
}
//#################################################################################################

void TextureClass::Shutdown()
{
	// Release the texture resources.
	/* EffectVariable will take care of this */
	

	return;
}
//#################################################################################################

void TextureClass::ReleaseResource( ID3D10ShaderResourceView* rw)
{
	if ( rw )
	{
		rw->Release();
		rw = nullptr;
	}
}
//#################################################################################################

ID3D10ShaderResourceView*  TextureClass::GetPerlin() 
{
	return m_perlin;
}
//#################################################################################################

ID3D10ShaderResourceView*  TextureClass::GetBrush() 
{
	return m_brush;
}
