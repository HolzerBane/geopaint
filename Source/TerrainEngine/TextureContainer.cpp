#include "TextureContainer.h"
#include "TextureObject.h"
#include <assert.h>

using namespace TerrainModel;


TextureContainer::TextureContainer()  { }
TextureContainer::~TextureContainer() { }

TextureContainer::TextureId TextureContainer::addTexture( TextureObjectPtr tex )
{
	m_textures.push_back( tex );
	return m_textures.size() - 1;
}

TextureContainer::TextureObjectPtr TextureContainer::getTexture( TextureId tId )
{
	assert( tId < m_textures.size() );
	return m_textures[ tId ];
}
