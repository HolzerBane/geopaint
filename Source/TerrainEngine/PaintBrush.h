#pragma once;
#include "Brush.h"

namespace TerrainTools
{

struct PaintBrush
{
	Brush		BrushType;
	unsigned	Layer;
};

}