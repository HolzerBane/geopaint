////////////////////////////////////////////////////////////////////////////////
// Filename: TextureClass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _TextureClass_H_
#define _TextureClass_H_


//////////////
// INCLUDES //
//////////////
#include <d3d10.h>
#include <d3dx10.h>
#include <vector>
#include <string>

////////////////////////////////////////////////////////////////////////////////
// Class name: TextureClass
////////////////////////////////////////////////////////////////////////////////
class TextureClass
{
public:
	TextureClass();
	TextureClass(const TextureClass&);
	~TextureClass();

	bool Initialize(ID3D10Device*, const std::wstring&, const std::wstring&, const std::wstring&);
	void Shutdown();

	bool loadTextureArray( 
		ID3D10Device* pd3dDevice, 
		const std::vector< std::wstring >& szTextureNames, 
		int iNumTextures,
		ID3D10Texture2D** ppTex2D, 
		ID3D10ShaderResourceView** ppSRV);

	bool loadCubeTex(
		ID3D10Device *pDev10, 
		const std::wstring& szTextureNames, 
		ID3D10Texture2D **ppTexture10, 
		ID3D10ShaderResourceView **ppSRV10);

	bool loadCubeMapFromDDS( 
		ID3D10Device* pd3dDevice, 
		const std::wstring& name, 
		ID3D10Texture2D **ppTexture10, 
		ID3D10ShaderResourceView **ppSRV10 );
	
	ID3D10ShaderResourceView* GetTextureArray() const { return m_textureArray; }
	ID3D10ShaderResourceView* GetNormalArray() const { return m_normalArray; }
	ID3D10ShaderResourceView* GetPerlin();

//private:
	void ReleaseResource( ID3D10ShaderResourceView* ); 

	ID3D10ShaderResourceView* m_textures[3];
	ID3D10ShaderResourceView* m_attrMap[3];
	ID3D10ShaderResourceView* m_perlin;

	ID3D10Texture2D*			m_texturePtr;
	ID3D10Texture2D*			m_normalPtr;
	ID3D10Texture2D*			m_enviroPtr;
	ID3D10ShaderResourceView* 	m_textureArray;
	ID3D10ShaderResourceView* 	m_normalArray;
	ID3D10ShaderResourceView* 	m_enviroment;
	//D3D10_MAPPED_TEXTURE2D		mappedTex;

};

#endif