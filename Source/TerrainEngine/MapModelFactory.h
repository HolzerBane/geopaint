#pragma once

#include "MapModel.h"
#include <string>

namespace TerrainModel
{

class MapModelFactoryException : std::exception
{
public:
	MapModelFactoryException( const std::string& message )
		: m_message( message )
	{ }

	const std::string& getMessage() const { return m_message; }

private:
	std::string m_message;
};

class MapModelFactory
{
public:

	bool createFromFile( const std::wstring& xmlConfigFile, MapModel& model );
private:
};

}