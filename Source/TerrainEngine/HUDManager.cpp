#include "HUDManager.h"
#include <iostream>

using namespace HUD;

HUDManager& HUDManager::Instance()
{
	static HUDManager hudManager;

	return hudManager;
}

CDXUTDialogResourceManager& HUDManager::getResourceManager()
{
	return m_resourceManager;
}

bool HUDManager::processMessage( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	return Instance().m_resourceManager.MsgProc( hWnd, msg, wParam, lParam );
}

HRESULT HUDManager::setDevice( ID3D10Device* device )
{
	return Instance().getResourceManager().OnD3D10CreateDevice(device);
}

HRESULT HUDManager::setSwapChain(  ID3D10Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc  )
{
	return Instance().getResourceManager().OnD3D10ResizedSwapChain( pd3dDevice, pBackBufferSurfaceDesc );
}
