////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "TextureObject.h"


////////////////////////////////////////////////////////////////////////////////
// Class name: BitmapObject
////////////////////////////////////////////////////////////////////////////////
class BitmapObject
{
private:
	struct VertexType
	{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
	};

public:
	BitmapObject();
	BitmapObject(const BitmapObject&);
	~BitmapObject();

	bool Initialize(ID3D10Device*, int, int, const std::wstring&, int, int);
	void Shutdown();
	bool Render(ID3D10Device*, int, int);

	int GetIndexCount();
	ID3D10ShaderResourceView* GetTexture();

private:
	bool InitializeBuffers(ID3D10Device*);
	void ShutdownBuffers();
	bool UpdateBuffers(int, int);
	void RenderBuffers(ID3D10Device*);

	bool LoadTexture(ID3D10Device*, const std::wstring&);
	void ReleaseTexture();

private:
	ID3D10Buffer	*m_vertexBuffer,*m_indexBuffer;
	int				m_vertexCount,	m_indexCount;
	TerrainModel::TextureObject		m_Texture;
	int				m_screenWidth,	m_screenHeight;
	int				m_bitmapWidth,	m_bitmapHeight;
	int				m_previousPosX, m_previousPosY;
};