#include "TextureObject.h"
//#include "AttributeLayer.hpp"
#include "Utility\ReportError.h"
#include "Utility\Memory.h"
#include "Utility\ReportError.h"

#include <assert.h>
#include <ppl.h>

using namespace TerrainModel;

TextureObject::TextureObject( unsigned arraySize )
	: m_arraySize(arraySize)
	, m_initialized(false)
	, m_textureResView(nullptr)
	, m_textureHandle(nullptr)
{

}

TextureObject::~TextureObject( )
{
	reset();
}

bool TextureObject::fillFromFile( const std::wstring& file, D3DX10_IMAGE_LOAD_INFO* loadInfo, ID3DX10ThreadPump* threadPump )
{
	// auto descriptor usage - may need more attention
	HRESULT result	= D3DX10CreateTextureFromFile( m_device, file.c_str(), loadInfo, threadPump, (ID3D10Resource**)&m_textureHandle, NULL );
	result			= m_device->CreateShaderResourceView( m_textureHandle, nullptr, &m_textureResView );
	//result = D3DX10CreateShaderResourceViewFromFile(m_device, file.c_str(), NULL, NULL, &m_textureResView, NULL);
	ReportError::Instance().CheckError();

	return result == S_OK;
}

bool TextureObject::fillFromDescriptor(
	const D3D10_TEXTURE2D_DESC& textureInfo,  
	D3D10_SUBRESOURCE_DATA& resourceData )
{
	HRESULT result = m_device->CreateTexture2D( &textureInfo, &resourceData, &m_textureHandle );
	ReportError::Instance().CheckError();

	D3D10_SHADER_RESOURCE_VIEW_DESC resDesc;
	resDesc.Format = textureInfo.Format;
	resDesc.ViewDimension = D3D_SRV_DIMENSION_TEXTURE2D;
	resDesc.Texture2D.MipLevels = textureInfo.MipLevels;
	resDesc.Texture2D.MostDetailedMip = 0;

	result = m_device->CreateShaderResourceView( m_textureHandle, &resDesc, &m_textureResView);
	ReportError::Instance().CheckError();

	m_initialized = result == S_OK;
	return m_initialized;
}

// parameterize this
bool TextureObject::fillFromLayer( const AttributeTool::AttributeLayer<D3DXVECTOR4>& layer)
{
// create texture manually
	D3D10_TEXTURE2D_DESC desc;
	
	desc.BindFlags = D3D10_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	desc.Usage = D3D10_USAGE_DYNAMIC;
	desc.MiscFlags = 0;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Width = layer.getW();
	desc.Height = layer.getH();
	desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	
	return fill( desc, layer);
	
}

// parameterize this
bool TextureObject::fillFromLayer( const AttributeTool::AttributeLayer<D3DXVECTOR3>& layer)
{
	// create texture manually
	D3D10_TEXTURE2D_DESC desc;
	
	desc.BindFlags = D3D10_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	desc.Usage = D3D10_USAGE_DYNAMIC;
	desc.MiscFlags = 0;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Width = layer.getW();
	desc.Height = layer.getH();
	desc.Format = DXGI_FORMAT_R32G32B32_FLOAT;
	
	return fill( desc, layer);

}



// parameterize this
bool TextureObject::fillFromLayer( const AttributeTool::AttributeLayer<float>& layer)
{
	// create texture manually
	D3D10_TEXTURE2D_DESC desc;
	
	desc.BindFlags = D3D10_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	desc.Usage = D3D10_USAGE_DYNAMIC;
	desc.MiscFlags = 0;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Width = layer.getW();
	desc.Height = layer.getH();
	desc.Format = DXGI_FORMAT_R32_FLOAT;
	
	return fill( desc, layer);

}

bool TextureObject::fillStagingFromLayer(  const AttributeTool::AttributeLayer<D3DXVECTOR4>& layer )
{
	D3D10_TEXTURE2D_DESC desc;
	
	desc.BindFlags = 0;
	desc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	desc.Usage = D3D10_USAGE_STAGING;
	desc.MiscFlags = 0;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Width = layer.getW();
	desc.Height = layer.getH();
	desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	
	return fill( desc, layer);
}

bool TextureObject::update( const D3D10_BOX&& region, const AttributeTool::AttributeLayer<D3DXVECTOR4>& data )
{
	// create some texture 
	D3D10_TEXTURE2D_DESC desc;
	
	desc.BindFlags = D3D10_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	desc.Usage = D3D10_USAGE_DYNAMIC;
	desc.MiscFlags = 0;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Width = region.right - region.left;
	desc.Height = region.bottom - region.top;
	desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;

	if ( desc.Width <= 0 || desc.Height <= 0)
	{
		return false;
	}

	const size_t tSize = desc.Width * desc.Height;
	D3DXVECTOR4* tex = new D3DXVECTOR4[tSize];

	const unsigned xSteps = desc.Width;
	const unsigned ySteps = desc.Height;

	Concurrency::parallel_for( 0u, tSize, [tex, &data, region, xSteps, ySteps]( const unsigned index)
	{
		const unsigned x = region.left  + index % xSteps;
		const unsigned y = region.top	+ index / xSteps;
		const D3DXVECTOR4& v =  data.get(x, y);
		tex[index] = v;
	}
	);

	D3D10_SUBRESOURCE_DATA resData;
	resData.pSysMem = (void*)tex;
	resData.SysMemPitch = desc.Width * sizeof(D3DXVECTOR4);
	resData.SysMemSlicePitch = 0;
	
	ID3D10Texture2D* srcResource;
	HRESULT res = m_device->CreateTexture2D( &desc, &resData, &srcResource );
	ReportError::Instance().CheckError();

	unsigned subresource = 0;

	m_device->CopySubresourceRegion( m_textureHandle, 0, region.left, region.top, 0, srcResource, 0, NULL );
	
	delete[] tex;
	srcResource->Release();
	srcResource = nullptr;

	ReportError::Instance().CheckError();

	return true;
}

void TextureObject::reset()
{
	// EffectVariable frees these
	//Memory::Release(m_textureResView);
	//Memory::Release(m_textureHandle);

	m_arraySize = 0; 
	m_initialized = false;
}