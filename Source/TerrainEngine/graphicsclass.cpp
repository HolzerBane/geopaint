#include "graphicsclass.h"
#include "DXUT/Core/DXUT.h"
#include "DXUT/optional/DXUTcamera.h"
#include <DXUTgui.h>

#include "TerrainShader.h"
#include "ShaderBase.h"
#include "TextureClass.h"
#include "InputClass.h"
#include "TextureShader.h"
#include "HUDControls.h"
#include "HUDManager.h"
#include "DXUTHUDHelper.h"
#include "Paths.h"

#define CONSOLE_ON
#include "Utility\DebugConsole.h"
#include "Utility\DxCanvas\DxCanvas.h"
#include "Utility\ReportError.h"

#include <d3d9types.h>

#include <iostream>
#include <sstream>

bool GraphicsClass::Switches::QuadDisplay	= false;
bool GraphicsClass::Switches::Wireframe		= false;
bool GraphicsClass::Switches::HUDDisplay	= true;

GraphicsClass::GraphicsClass()
{
	m_D3D = 0;
	m_Camera = 0;
	m_Model = 0;
	m_AlphaMapShader = 0;
	m_freeCamera = nullptr;
	m_inited = false;

	m_mouseLeft		= false;
	m_mouseRight	= false;
}


GraphicsClass::GraphicsClass(const GraphicsClass& other)
{
}


GraphicsClass::~GraphicsClass()
{
}

void CALLBACK OnGUIEvent( UINT eventId, int controlId, CDXUTControl* pControl, void* pUserContext )
{
	HUD::HUDControls::Instance().processGUIEvent( controlId, eventId );
}


bool GraphicsClass::Initialize(int screenWidth, int screenHeight, HWND hwnd)
{
	auto st = DebugConsole::scoped_trace( L"Graphics" );

	bool result;
	D3DXMATRIX baseViewMatrix;


	// Create the Direct3D object.
	m_D3D = new D3DClass;
	if(!m_D3D)
	{
		return false;
	}

	// Initialize the Direct3D object.
	result = m_D3D->Initialize(screenWidth, screenHeight, VSYNC_ENABLED, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize Direct3D.", L"Error", MB_OK);
		return false;
	}
	DebugConsole::trace(L"D3D Device initialized");
	
	// create skybox object
	m_skybox.OnCreateDevice(m_D3D->GetDevice()); 

	DebugConsole::trace(L"Skybox initialized");

	// Create the camera object.
	m_freeCamera = new FreeCamera(D3DXVECTOR3( 40.162f, 40.497f, 0.f )  , D3DXVECTOR3( -40.f, 35.f, 100.f ) );
	m_Camera = new CameraClass();
	if(!m_Camera)
	{
		return false;
	}

	DebugConsole::trace(L"Camera initialized");

	// Create the alpha map shader object.
	m_AlphaMapShader = new TerrainShader();
	if(!m_AlphaMapShader)
	{
		return false;
	}

	// Initialize the alpha map shader object.
	result = m_AlphaMapShader->initialize(m_D3D->GetDevice(), hwnd, Paths::Shaders + L"TerrainShader.hlsl_o",Shader::ShaderBase::Text );
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the alpha map shader object.", L"Error", MB_OK);
		return false;
	}
	DebugConsole::trace(L"Shader initialized");

	// Initialize a base view matrix with the camera for 2D user interface rendering.
	m_Camera->SetPosition(0.0f, 0.0f, 1.0f);
	m_Camera->Render( CameraClass::USE_ROTATE );
	m_Camera->GetViewMatrix(baseViewMatrix);

	// Create the model object.
	m_Model = new ModelClass;
	if(!m_Model)
	{
		return false;
	}

	// Initialize the model object.
	result = m_Model->Initialize(m_D3D->GetDevice());
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the model object.", L"Error", MB_OK);
		return false;
	}

	// get the skybox texture from the texture object
	m_skybox.SetTexture( m_Model->getTextureClass()->GetEnviroment());

	m_Model->m_shader = m_AlphaMapShader;
	m_AlphaMapShader->setModelParameters( m_Model->getMapModel() );
	m_AlphaMapShader->bindEffectVars( *m_Model );

	// Create the texture shader object.
	m_textureShader = new TextureShader;
	if(!m_textureShader)
	{
		return false;
	}

	// Initialize the texture shader object.
	result = m_textureShader->Initialize(m_D3D->GetDevice(), hwnd);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the texture shader object.", L"Error", MB_OK);
		return false;
	}
	DebugConsole::trace(L"Bitmap Shader initialized");

	// mouse input / cursor setup
	// Create the bitmap object.
	// Initialize the bitmap object.
	result = m_mousePointer.Initialize(m_D3D->GetDevice(), screenWidth, screenHeight, Paths::Textures+ L"mouse.dds", 32, 32);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the bitmap object.", L"Error", MB_OK);
		return false;
	}
	m_beginCheck = false;
	DebugConsole::trace(L"Mouse initialized");


	// set const shader params;
	m_AlphaMapShader->setCDLODParameters( m_Model->getQuadTree(), m_Model->getRegularGrid());
	m_AlphaMapShader->SetConstShaderParameters();

	DebugConsole::trace(L"Shader bound to model");

	// initialize HUD related stuff
	DxCanvas::GetCanvas2D()->Initialize( m_D3D->GetDevice() );
	DxCanvas::GetCanvas3D()->Initialize( m_D3D->GetDevice() );

	HUD::HUDControls::Instance().initializeControls( m_Model->getPaintBrush() );

	// set GUI callback
	HUD::HUDControls::Instance().setCallback( OnGUIEvent );
	D3DCOLOR color = 0x88888888;
	HUD::HUDControls::Instance().setBackGround( color );

	// set HUD events
	HUD::HUDControls& controls = HUD::HUDControls::Instance();

	controls.BrushBiasChanged		+= [this]( const unsigned param ) { m_Model->setBrushBias(param);	};
	controls.BrushOpacityChanged	+= [this]( const unsigned param ) { m_Model->setBrushOpacity(param);};
	controls.BrushSizeChanged		+= [this]( const unsigned param ) { m_Model->setBrushSize(param);	};
	controls.BrushLayerChanged		+= [this]( const unsigned param ) { m_Model->setBrushLayer(param);	};
	controls.BrushEraserChanged		+= [this]( const bool param		) { m_Model->setBrushEraser(param);	};
	controls.NewTerrainRequested	+= [this]( const TerrainGeneratorStruct::Data& param ) 
												{
													m_allowRender = false;
													m_Model->newTerrain(param);
													m_allowRender = true;
												};

	controls.addLayer( 0, L"Mountain" );
	controls.addLayer( 1, L"Grass" );
	controls.addLayer( 2, L"Dirt" );
	controls.addLayer( 3, L"Desert" );
	controls.addLayer( 4, L"Terrain" );

	DebugConsole::trace(L"HUD initialized");

	ReportError::Instance().CheckError();
	m_inited = true;
	m_allowRender = true;
	return true;
}

void GraphicsClass::Shutdown()
{
	// Release the alpha map shader object.
	if(m_AlphaMapShader)
	{
		m_AlphaMapShader->shutdown();
		delete m_AlphaMapShader;
		m_AlphaMapShader = 0;
	}

	// Release the model object.
	if(m_Model)
	{
		m_Model->Shutdown();
		delete m_Model;
		m_Model = 0;
	}

	// Release the camera object.
	if(m_Camera)
	{
		delete m_Camera;
		delete m_freeCamera;
		m_freeCamera = 0;
		m_Camera = 0;
	}

	// Release the Direct3D object.
	if(m_D3D)
	{
		m_D3D->Shutdown();
		delete m_D3D;
		m_D3D = 0;
	}

	// Release the texture shader object.
	if(m_textureShader)
	{
		m_textureShader->Shutdown();
		delete m_textureShader;
		m_textureShader = 0;
	}

	return;
}

bool GraphicsClass::Frame( float dt)
{
	if ( m_allowRender )
	{
		// Set the position of the camera.
		m_freeCamera->animate(dt);
		m_Model->Frame(dt);
		ReportError::Instance().CheckError();

		if ( m_mouseLeft )
		{
			//std::cout << "handled brush! " << std::endl;
			m_Model->handleBrushRequest( );
		}
		ReportError::Instance().CheckError();
	}
	return true;
}
void GraphicsClass::onMouseClick( bool leftClick, bool rightClick)
{
	m_mouseLeft  = leftClick;
	m_mouseRight = rightClick;
}

void GraphicsClass::onMouseMove( int mouseX, int mouseY )
{
	m_mouseX = mouseX;
	m_mouseY = mouseY;
	//std::cout << m_mouseX << "\t" << m_mouseY << std::endl;

	if ( m_Model )
	{
		m_Model->calculateBrushPosition( *m_freeCamera, m_mouseX, m_mouseY );
	}
}

void GraphicsClass::onLeftMouseButtonUp()
{
	m_Model->onBrushRequestEnd();
}

void GraphicsClass::setFPS( const double fps )
{
	std::stringstream ssFps;
	ssFps << "FPS: " << fps;
	DxCanvas::GetCanvas2D()->DrawString( ssFps.str().c_str() );
}

bool GraphicsClass::testRenderHUD( float dt )
{
	m_D3D->BeginScene(0.0f, 0.0f, 0.0f, 1.0f);
	HUD::HUDControls::Instance().render( dt );
	m_D3D->EndScene();

	return true;
}

void GraphicsClass::drawHelpText() const
{
	DxCanvas::GetCanvas2D()->DrawString( "------------ Help ------------");
		// dump them leaks
#if defined(DEBUG) || defined(_DEBUG)
//	_CrtDumpMemoryLeaks();
#endif
	DxCanvas::GetCanvas2D()->DrawString( "WASD / arrows  - Move camera");
	DxCanvas::GetCanvas2D()->DrawString( "Q,E / PgUp,PgDn  - Change elevation");
	DxCanvas::GetCanvas2D()->DrawString( "Left  Click  - Paint");
	DxCanvas::GetCanvas2D()->DrawString( "Right Click-Drag  - Rotate camera");
	DxCanvas::GetCanvas2D()->DrawString( "Alt + F4  - Quit");
}

bool GraphicsClass::Render( float dt )
{
	if (!m_allowRender)
	{
		return true;
	}

	HRESULT result = S_OK;

	D3DXMATRIX orthoMatrix;

	// Clear the buffers to begin the scene.
	m_D3D->BeginScene(0.0f, 0.0f, 0.0f, 1.0f);

#if 1
	std::stringstream ssCam;
	ssCam << "Camera Position:  X: " << m_freeCamera->getEyePosition().x << "\t Y: " << m_freeCamera->getEyePosition().y << "\t Z: " << m_freeCamera->getEyePosition().z;
	DxCanvas::GetCanvas2D()->DrawString( ssCam.str().c_str());

	// Get the world, view, projection, and ortho matrices from the camera and d3d objects.
	m_D3D->GetWorldMatrix( m_Model->accessWorldMatrix() );
	m_Model->setViewMatrix( m_freeCamera->getViewMatrix() );
	m_Model->setProjMatrix( m_freeCamera->getProjMatrix() );
	//m_D3D->GetProjectionMatrix( m_Model->accessProjMatrix() );
	m_D3D->GetOrthoMatrix(orthoMatrix);

	// move skybox
	m_skybox.OnFrameRender(m_Model->getWorldMatrix() * m_Model->getViewMatrix() * m_Model->getProjMatrix(), 0.7f ,D3DXVECTOR4(0.5f,0.5f,0.8f,0), m_Model->getLightPos() );
	// Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing.
	
	//m_Model->Render(m_D3D->GetDevice());
	m_AlphaMapShader->setInputParams( 
		m_Model->showNormals(), 
		*m_freeCamera
		);


	m_AlphaMapShader->SetPerFrameShaderParameters();

	m_Model->RenderCDLOD( m_D3D->GetDevice(), *m_freeCamera );

	// draw help
	drawHelpText();

#endif 
	// draw debug stuff
	if (Switches::QuadDisplay)
	{
		DxCanvas::vaDrawCanvas3D( *m_D3D, m_freeCamera->getViewMatrix(), m_freeCamera->getProjMatrix() );
	}
	if ( Switches::HUDDisplay )
	{
		DxCanvas::vaDrawCanvas2D( *m_D3D, DXUTHUDHelper::Instance().getTextHelper() );	
	}

	HUD::HUDControls::Instance().render( dt );

	// clear errors here... DXUT generates one warning with dx10 fonts the first time it draws them..
	ReportError::Instance().ClearMessages();
	// Render the model using the alpha map shader.

	// handle 2D renders
	// Turn off the Z buffer to begin all 2D rendering.
	m_D3D->TurnZBufferOff();

	// Turn on alpha blending.
	m_D3D->EnableAlphaBlending();

	// Render the mouse cursor with the texture shader.
	
	m_mousePointer.Render(m_D3D->GetDevice(), m_mouseX, m_mouseY);  
	
	D3DXMATRIX baseViewMatrix;

	// mouse camera
	m_Camera->SetPosition(0, 0, -10.f);
	m_Camera->SetLookAt(0, 0, 0.f);
	m_Camera->Render( CameraClass::USE_LOOKAT );
	m_Camera->GetViewMatrix(baseViewMatrix);

	m_textureShader->Render(
		m_D3D->GetDevice(), 
		m_mousePointer.GetIndexCount(), 
		m_Model->getWorldMatrix(), 
		baseViewMatrix,
		orthoMatrix, 
		m_mousePointer.GetTexture());
	
	// Turn of alpha blending.
	m_D3D->DisableAlphaBlending();

	// Turn the Z buffer back on now that all 2D rendering has completed.
	m_D3D->TurnZBufferOn();

	// Present the rendered scene to the screen.
	m_D3D->EndScene();

	return true;
}