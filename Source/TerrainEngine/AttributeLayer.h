#pragma once

#include <vector>
#include "AttributeTypes.h"
#include <array>
#include <functional>
// AttributeLayer
//
// Represents a layer of values as a float matrix
struct D3DXVECTOR4;

namespace AttributeTool
{

// TODO : rethink this template
//		  separate template dependant data and operations (save to / load from file, needs factory f.e. )	 
template< class TValue >
class AttributeLayer
{
public:
	typedef AttributeLayer< TValue> MyType;
	typedef std::vector< TValue > ValueVector;
	typedef std::vector< ValueVector > ValueMatrix;
	typedef std::wstring FileSource;

	AttributeLayer();
	AttributeLayer( const AttributeLayer&& other );
	//AttributeLayer( const FileSource& );
	AttributeLayer( const unsigned w, const unsigned h );
	~AttributeLayer();


	//void saveTofile( const FileSource& fs ) const;
	void clear();
	void reallocate( const unsigned w, const unsigned h );
	bool set( const Index x, const Index y, const TValue& v );
	bool set( const Index index, const TValue& v );
	bool add( const Index x, const Index y, const TValue& v );

	bool isInbound( const Index x, const Index y, const unsigned padding = 0 ) const;
	bool isInboundIndex( const Index i, const unsigned padding = 0) const;

	const TValue& get( const Index x, const Index y ) const;
	
	const TValue& get( const Index i ) const;
	TValue& operator[]( const Index i );

	void mul( float );
	void mul( const MyType& );
	void add( const MyType& );
	void op( const MyType&, std::function< TValue( const TValue&, const TValue&) > );
	void saturate( const float minimum, const float maximum );
	void clamp( const float minimum, const float maximum );
	void blur();
	unsigned getW() const { return m_w; }
	unsigned getH() const { return m_h; }

	unsigned count() const { return m_w * m_h; }

	TValue* getPtr() { return &m_valueMatrix[0]; }
	void setLimits( const TValue& minLimit , const TValue& maxLimit );
	const TValue& getMinValue() const { return m_minVal; }
	const TValue& getMaxValue() const { return m_maxVal; }

private:

	ValueVector m_valueMatrix;
	unsigned m_w;
	unsigned m_h;
	TValue	 m_minVal;
	TValue	 m_maxVal;

	TValue	 m_minLimit;
	TValue	 m_maxLimit;

	inline void setMin( const TValue& other );
	inline void setMax( const TValue& other );

};

}

#include "AttributeLayer.hpp"

