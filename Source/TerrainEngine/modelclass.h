#pragma once

#include <fstream>
#include "mapModel.h"

#include "Utility\CDLOD\CDLODQuadTree.h"
#include "Utility\CDLOD\RegularGrid.h"
#include "HeightMapAdapter.h"
#include "ViewPoint.h"
#include "PaintBrushHandler.h"
#include "PaintBrush.h"
#include "VertexType.h"

/* fwd decs*/
class FreeCamera;
class TerrainShader;
class TextureClass;

namespace TerrainTools
{
	class PaintBrushHandler;
}

namespace TerrainGeneratorStruct
{
	struct Data;
}
/* fwd decs end*/

////////////////////////////////////////////////////////////////////////////////
// Class name: ModelClass
////////////////////////////////////////////////////////////////////////////////
class ModelClass
{
	

public:
	ModelClass();
	ModelClass(const ModelClass&);
	~ModelClass();

	bool Initialize(ID3D10Device*);
	void Shutdown();
	void Render(ID3D10Device*);
	bool Frame( float dt );
	int GetIndexCount() const;

	ID3D10ShaderResourceView* GetTextureArray() const;
	ID3D10ShaderResourceView* GetNormalArray() const;
	ID3D10ShaderResourceView* GetPerlin() const;
	ID3D10ShaderResourceView* GetBrush() const;
	ID3D10ShaderResourceView* GetVertexHeightMap() const;

	TextureClass* getTextureClass() const { return m_TextureArray; } 
	const D3DXVECTOR3& getLightPos() const { return m_lightPos; }

	TerrainModel::MapModel& getMapModel( )				{ return m_mapModel; }
	const TerrainModel::MapModel& getMapModel( ) const	{ return m_mapModel; }

	void toggleNormals() { m_showNormals = !m_showNormals; }
	bool showNormals() const { return m_showNormals; }

	const D3DXMATRIX& getWorldMatrix() const { return m_worldMatrix; }
	const D3DXMATRIX& getViewMatrix() const { return m_viewMatrix; }
	const D3DXMATRIX& getProjMatrix() const { return m_projMatrix; }

	void setWorldMatrix( const D3DXMATRIX& wm)  { m_worldMatrix = wm; }
	void setViewMatrix( const D3DXMATRIX& vm)  { m_viewMatrix = vm; }
	void setProjMatrix( const D3DXMATRIX& pm)  { m_projMatrix = pm; }

	const D3DXMATRIX& getBrushViewMatrix() const { return m_brushViewPoint.GetViewMatrix(); }
	const D3DXMATRIX& getBrushProjMatrix() const { return m_brushViewPoint.GetProjectionMatrix(); }

	D3DXMATRIX& accessWorldMatrix() { return m_worldMatrix; }
	D3DXMATRIX& accessViewMatrix() { return m_viewMatrix; }
	D3DXMATRIX& accessProjMatrix() { return m_projMatrix; }

	const CDLODQuadTree& getQuadTree() const { return m_quadTree; }
	const RegularGrid<VertexType>&   getRegularGrid() const { return m_gridPatch; }

	void calculateBrushPosition( const FreeCamera& camera, const int mouseX, const int mouseY );
	const ViewPoint&  getBrushViewPoint() const { return m_brushViewPoint; }
	const TerrainTools::PaintBrush& getPaintBrush() const { return m_paintBrush; }

	const D3DXVECTOR3& getCursorPosition() const { return m_cursorPos; } 
	void setBrushSize( const unsigned size );
	void setBrushBias( const unsigned bias );
	void setBrushOpacity( const unsigned opacity );
	void setBrushLayer( const unsigned layerId );
	void setBrushEraser( const bool isErasing );
	bool newTerrain( const TerrainGeneratorStruct::Data& data );
	
	bool handleBrushRequest( );
	void onBrushRequestEnd( );

	bool InitializeBuffers(ID3D10Device*);

	bool initGridPatchBuffer( const unsigned int dimension, ID3D10Device* device);
	bool initTestMap();
	bool initEmptyBuffer( const unsigned dimX, const unsigned dimY );

	void ShutdownBuffers();
	void RenderBuffers(ID3D10Device*);
	void RenderCDLOD( ID3D10Device* device, const FreeCamera& camera );
	HRESULT RenderQuadTree(	ID3D10Device* device, 
							const CDLODQuadTree::LODSelectionOnStack<4096>& cdlodSelection, 
							int filterLodLevel, unsigned& renderedTriangles ) const;

	void updateCursorViewport();

	bool LoadTextures(ID3D10Device*);
	void ReleaseTextures();

	ID3D10Device*		m_device;

	TerrainShader*		m_shader;
private:
	void				createQuadTree();

	RegularGrid<VertexType>	m_gridPatch;

	D3DXVECTOR3			m_lightPos;
	ID3D10Buffer*		m_vertexBuffer, *m_indexBuffer;
	int					m_vertexCount,	m_indexCount;
	TextureClass*		m_TextureArray;
	TerrainModel::MapModel m_mapModel;
	bool				m_showNormals;
	bool				m_wireframe;
	D3DXMATRIX			m_worldMatrix;
	D3DXMATRIX			m_viewMatrix;
	D3DXMATRIX			m_projMatrix;


	CDLODQuadTree		m_quadTree;
	HeightMapAdapter	m_ha;
	float				m_mapSizeFactor;
	MapDimensions		m_mapDiemnsions;

	ViewPoint			m_brushViewPoint;	
	D3DXVECTOR3			m_cursorPos;
	D3DXVECTOR3			m_cursorSource;

	TerrainTools::PaintBrush		m_paintBrush;
	TerrainTools::PaintBrushHandler m_paintBrushHandler;

};
