#pragma once

#include <d3d10_1.h>
#include <d3dx10.h>
#include <vector>
#include <array>
#include <functional>
#include <string>

namespace Shader
{

////////////////////////////////////////////////////////////////////////////////
// Class name: ShaderBase
////////////////////////////////////////////////////////////////////////////////
class ShaderBase
{
public:

	enum ShaderSource
	{
		Text,
		Precompiled
	};

	ShaderBase();
	virtual ~ShaderBase();

	bool initialize( ID3D10Device* device, HWND targetWindow, const std::wstring& fileName, const ShaderSource shaderSource );

	void shutdown();
	void render( ID3D10Device* device, const unsigned indexCount, const unsigned offset = 0, const unsigned baseVertex = 0);

protected:
	bool virtual initializeTechniques( ID3D10Device* device ) = 0;
	void virtual shutdownShader();
	void virtual renderShader( ID3D10Device* device, const unsigned indexCount, const unsigned offset = 0, const unsigned baseVertex = 0);

	bool addTechnique( ID3D10Device* device, const std::string& techName, const D3D10_INPUT_ELEMENT_DESC* polygonLayout, const unsigned numberOfElements );

	HRESULT SetBool( ID3D10EffectVariable*, bool val );
	
	HRESULT SetFloat( ID3D10EffectVariable* variable, float val );
	HRESULT SetFloatArray( ID3D10EffectVariable* variable, float val0, float val1 );
	HRESULT SetFloatArray( ID3D10EffectVariable* variable, float val0, float val1, float val2 );
	HRESULT SetFloatArray( ID3D10EffectVariable* variable, float val0, float val1, float val2, float val3 );
	HRESULT SetFloatArray( ID3D10EffectVariable* variable, const float * valArray, int valArrayCount );
	
	HRESULT SetVector( ID3D10EffectVariable* variable, const D3DXVECTOR4 & vec );
	HRESULT SetMatrix( ID3D10EffectMatrixVariable* variable, const D3DXMATRIX & matrix);

private:
	bool initializeTextShader(ID3D10Device*, HWND, const std::wstring&);
	bool initializePrecompiledShader(ID3D10Device*, HWND, const std::wstring&);
	void outputShaderErrorMessage(ID3D10Blob*, HWND, const std::wstring&);

protected:
	ID3D10Effect*						m_effect;

private:
	ShaderBase(const ShaderBase&);

	std::array< std::function< bool(ID3D10Device*, HWND, const std::wstring&)>, 2 > m_initializers;
	std::vector<ID3D10EffectTechnique*>	m_techniqueVector;
	std::vector<ID3D10InputLayout*>		m_layoutVector;

	std::wstring		m_shaderFile;
};

}