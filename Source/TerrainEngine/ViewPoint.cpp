#include "ViewPoint.h"


ViewPoint::ViewPoint()
{
}


ViewPoint::ViewPoint(const ViewPoint& other)
{
}


ViewPoint::~ViewPoint()
{
}


void ViewPoint::SetPosition(float x, float y, float z)
{
	m_position = D3DXVECTOR3(x, y, z);
	return;
}


void ViewPoint::SetLookAt(float x, float y, float z)
{
	m_lookAt = D3DXVECTOR3(x, y, z);
}

void ViewPoint::SetPosition(const D3DXVECTOR3& pos)
{
	m_position = pos;
}


void ViewPoint::SetLookAt(const D3DXVECTOR3& look)
{
	m_lookAt = look;
}

void ViewPoint::SetProjectionParameters(float fieldOfView, float aspectRatio, float nearPlane, float farPlane)
{
	m_fieldOfView = fieldOfView;
	m_aspectRatio = aspectRatio;
	m_nearPlane = nearPlane;
	m_farPlane = farPlane;
}


void ViewPoint::GenerateViewMatrix()
{
	D3DXVECTOR3 up;


	// Setup the vector that points upwards.
	up.x = 0.0f;
	up.y = 0.0f;
	up.z = 1.0f;

	// Create the view matrix from the three vectors.
	D3DXMatrixLookAtLH(&m_viewMatrix, &m_position, &m_lookAt, &up);
}


void ViewPoint::GenerateProjectionMatrix()
{
	// Create the projection matrix for the view point.
	D3DXMatrixPerspectiveFovLH(&m_projectionMatrix, m_fieldOfView, m_aspectRatio, m_nearPlane, m_farPlane);
}


const D3DXMATRIX&  ViewPoint::GetViewMatrix() const
{
	return m_viewMatrix;
}


const D3DXMATRIX&  ViewPoint::GetProjectionMatrix() const
{
	return m_projectionMatrix;
}
