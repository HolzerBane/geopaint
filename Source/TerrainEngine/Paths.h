#pragma once
#include <string>

namespace Paths
{
#ifdef _DEBUG
	static const std::wstring Resource		= L"../Resource/";
	static const std::wstring Textures		= L"../Resource/Textures/";
	static const std::wstring HiResTextures	= L"../Resource/Textures/HiRes/";
	static const std::wstring Shaders		= L"../Resource/Shaders/";
#else	    
	static const std::wstring Resource		= L"../Resource/";
	static const std::wstring Textures		= L"../Resource/Textures/";
	static const std::wstring HiResTextures	= L"../Resource/Textures/HiRes/";
	static const std::wstring Shaders		= L"../Resource/Shaders/";
#endif

}