#pragma once

#define DIRECTINPUT_VERSION 0x0800

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

#include <dinput.h>
#include <algorithm>
#include <functional>
#include <vector>

#include "Utility\Event.h"

class InputClass
{
public:

	Event<> LeftMouseButtonUp;

	InputClass();
	InputClass(const InputClass&);
	~InputClass();

	bool Initialize(HINSTANCE, HWND, int, int);

	void Shutdown();
	bool Frame();

	void MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );

	void GetMouseLocation(int&, int&);
	bool IsKeyPressed( const byte );
	bool IsLeftMouseButtonDown();
	bool IsRightMouseButtonDown();
	void releaseMouseClicks();

private:
	bool ReadKeyboard();
	bool ReadMouse();
	void ProcessInput();

private:
	IDirectInput8* m_directInput;
	IDirectInputDevice8* m_keyboard;
	IDirectInputDevice8* m_mouse;

	unsigned char m_keyboardState[256];
	DIMOUSESTATE m_mouseState;

	int m_screenWidth, m_screenHeight;
	int m_mouseX, m_mouseY;
	bool m_mouseLeftButtonDown, m_mouseRightButtonDown;
};

