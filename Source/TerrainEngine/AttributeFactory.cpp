#include "AttributeFactory.h"
#include "Utility\ImageFile.h"
#include <string>

using namespace AttributeTool;

AttributeLayer<float> AttributeFactory::getFromImageChannel(const ImageFile& imageFile, const ImageConsts::Channel channel ) 
{
	AttributeLayer<float> result( imageFile.getWidth(), imageFile.getHeight() );

	for ( unsigned index = 0; index < result.count(); ++index )
	{
		result.set( index, imageFile.getChannel( channel, index ) / 255.f );
	}

	return result;
}

AttributeLayer<float> AttributeFactory::getFromImageChannel(const std::wstring& filePath, const ImageConsts::Channel channel ) 	
{
	unsigned w,h;
	ImageFile imageFile( filePath, w, h );

	return getFromImageChannel(imageFile, channel );
}
