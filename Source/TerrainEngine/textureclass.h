#pragma once

////////////////////////////////////////////////////////////////////////////////
// Filename: TextureClass.h
////////////////////////////////////////////////////////////////////////////////

//////////////
// INCLUDES //
//////////////
#include <d3d10_1.h>
#include <d3dx10.h>
#include <vector>
#include <string>

////////////////////////////////////////////////////////////////////////////////
// Class name: TextureClass
////////////////////////////////////////////////////////////////////////////////
class TextureClass
{
public:
	TextureClass();
	~TextureClass();

	bool Initialize( ID3D10Device* device );
	void Shutdown();

	bool loadPerlin( const unsigned perlinSize );
	bool loadDefaultTextures();

	bool loadTextureArray( const std::vector< std::wstring >& textureNames, ID3D10Texture2D** ppTex2D, ID3D10ShaderResourceView** ppSRV );
	bool loadTextures( std::vector< std::wstring >&& textureFiles );

	bool createBrush( const unsigned size);
	
	ID3D10ShaderResourceView* GetTextureArray() const { return m_textureArray; }
	ID3D10ShaderResourceView* GetNormalArray() const { return m_normalArray; }
	ID3D10ShaderResourceView* GetEnviroment() const { return m_enviroment; }
	ID3D10ShaderResourceView* GetPerlin();
	ID3D10ShaderResourceView* GetBrush(); 

private:
	TextureClass( const TextureClass& );

	void ReleaseResource( ID3D10ShaderResourceView* ); 

	bool loadCubeTex(
		const std::wstring& textureNames, 
		ID3D10Texture2D **ppTexture10, 
		ID3D10ShaderResourceView **ppSRV10);

	bool loadCubeMapFromDDS( 
		const std::wstring& name, 
		ID3D10Texture2D **ppTexture10, 
		ID3D10ShaderResourceView **ppSRV10 );

	ID3D10Device*				m_device;
	ID3D10ShaderResourceView*	m_perlin;

	ID3D10Texture2D*			m_texturePtr;
	ID3D10Texture2D*			m_normalPtr;
	ID3D10Texture2D*			m_enviroPtr;

	ID3D10ShaderResourceView* 	m_textureArray;
	ID3D10ShaderResourceView* 	m_normalArray;
	ID3D10ShaderResourceView* 	m_enviroment;

	// brush projection
	ID3D10ShaderResourceView*	m_brush;

	//D3D10_MAPPED_TEXTURE2D		mappedTex;

};
