#include "MapModelFactory.h"
#include "..\Utility\File.h"
#include "..\..\external\RapidXML\rapidxml.hpp"
#include "..\..\external\RapidXML\rapidxml_utils.hpp"

#define CONSOLE_ON
#include "..\Utility\DebugConsole.h"

using namespace TerrainModel;

const wchar_t* XmlRootTag					= L"MapModel";
									  
const wchar_t* XmlTexturesTag			= L"Textures";
const wchar_t* XmlTextureTag			= L"Texture";
const wchar_t* XmlTextureDiffusePathTag	= L"DiffusePath";
const wchar_t* XmlTextureNormalPathTag	= L"NormalPath";
const wchar_t* XmlTextureIdTag			= L"TextureId";
				  
const wchar_t* XmlLayersTag				= L"Layers";
const wchar_t* XmlLayerTag				= L"Layer";
const wchar_t* XmlLayerTextureIdTag		= L"TextureId";
const wchar_t* XmlLayerChannelTag		= L"Channel";
					  
const wchar_t* XmlChannelRedValue		= L"Red";
const wchar_t* XmlChannelGreenValue		= L"Green";
const wchar_t* XmlChannelBlueValue		= L"Blue";
const wchar_t* XmlChannelalphaValue		= L"Alpha";

using namespace TerrainModel;

// TODO : unused, unfinished
bool MapModelFactory::createFromFile( const std::wstring& xmlConfigfile, MapModel& model )
{
	std::wstring strFile;
	File::readFileToString( xmlConfigfile, strFile );

	rapidxml::xml_document<wchar_t> doc;

	wchar_t* fileStart = &strFile[0];

	try
	{
	doc.parse<0>( fileStart );

	// get the root tag
	auto rootNode = doc.first_node( XmlRootTag );

	// parse textures
	auto texturesNode = rootNode->first_node( XmlTexturesTag );

	std::vector<std::wstring> diffuseTextures;
	std::vector<std::wstring> normalTextures;
	std::vector<std::wstring> textureIds;

	unsigned textureIndex = 0;
	for ( auto textureNode = texturesNode->first_node( XmlTextureTag ); textureNode != nullptr; textureNode = texturesNode->next_sibling( XmlTextureTag ) )
	{
		diffuseTextures.push_back( rapidxml::safeGetChildValue<wchar_t>( textureNode, XmlTextureDiffusePathTag ));
		normalTextures.push_back( rapidxml::safeGetChildValue<wchar_t>( textureNode, XmlTextureNormalPathTag ));
		textureIds.push_back( rapidxml::safeGetChildValue<wchar_t>( textureNode, XmlTextureIdTag ));
		
		textureIndex++;
	}

	// create textures
	for ( unsigned i = 0; i < textureIndex; ++i )
	{

	}

	} catch ( rapidxml::parse_error& error)
	{
		DebugConsole::trace( error.what() );
		return false;
	}

	return true;
}

