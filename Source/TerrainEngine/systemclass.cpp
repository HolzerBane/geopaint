////////////////////////////////////////////////////////////////////////////////
// Filename: systemclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "DXUT.h"
#include "systemclass.h"
#include "Utility\FreeCamera.h"
#include <time.h>
#include "Utility\DeltaTime.h"
#include "Utility\StopWatch.h"
#include "HUDControls.h"
#include "HUDManager.h"

#define CONSOLE_ON
#include "Utility\DebugConsole.h"

#include <iostream>

SystemClass::SystemClass()
{
	m_Input = 0;
	m_Graphics = 0;
}


SystemClass::SystemClass(const SystemClass& other)
{
}


SystemClass::~SystemClass()
{
}


bool SystemClass::Initialize()
{
	auto st = DebugConsole::scoped_trace( L"SystemClass" );

	int screenWidth, screenHeight;
	bool result;

	FPSTimer::Initialize();
	// Initialize the width and height of the screen to zero before sending the variables into the function.
	screenWidth = 0;
	screenHeight = 0;

	// Initialize the windows api.
	InitializeWindows(screenWidth, screenHeight);

	// Create the input object.  This object will be used to handle reading the keyboard input from the user.
	m_Input = new InputClass;
	if(!m_Input)
	{
		return false;
	}

	// Initialize the input object.
	result = m_Input->Initialize(m_hinstance, m_hwnd, screenWidth, screenHeight);
	if(!result)
	{
		MessageBox(m_hwnd, L"Could not initialize the input object.", L"Error", MB_OK);
		return false;
	}

	// Create the graphics object.  This object will handle rendering all the graphics for this application.
	m_Graphics = new GraphicsClass;
	if(!m_Graphics)
	{
		return false;
	}

	// Initialize the graphics object.
	result = m_Graphics->Initialize(screenWidth, screenHeight, m_hwnd);
	if(!result)
	{
		return false;
	}
	
	// set events
	m_Input->LeftMouseButtonUp += [this](Empty){ m_Graphics->onLeftMouseButtonUp(); };

	return true;
}


void SystemClass::Shutdown()
{
	// Release the graphics object.
	if(m_Graphics)
	{
		m_Graphics->Shutdown();
		delete m_Graphics;
		m_Graphics = 0;
	}

	// Release the input object.
	if(m_Input)
	{
		m_Input->Shutdown();
		delete m_Input;
		m_Input = 0;
	}

	// Shutdown the window.
	ShutdownWindows();
	
	return;
}


void SystemClass::Run()
{
	MSG msg;
	bool done, result;
	time_t time_start, time_end = 0;
	

	// Initialize the message structure.
	ZeroMemory(&msg, sizeof(MSG));
	
	// FPS watch and its stuff
	StopWatch stopWatch;
	const unsigned maxRounds = 10;
	unsigned rounds = 0;
	double lapse = 0.0f;
	double fps = 0.0;

	done = false;
	// Loop until there is a quit message from the window or the user.
	while(!done)
	{
		stopWatch.start();

		time (&time_start);
		// Handle the windows messages.
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}


		// If windows signals to end the application then exit out.
		if(msg.message == WM_QUIT)
		{
			done = true;
		}
		else
		{
			// Otherwise do the frame processing.
			double dt = FPSTimer::DeltaTime(); //difftime(time_start, time_end);			

			result = Frame( (float)dt );
			if(!result)
			{
				done = true;
			}

			time (&time_end);
			lapse += stopWatch.stop();
			m_Graphics->setFPS( fps );

			if ( rounds >= maxRounds )
			{
				fps = maxRounds / lapse;
				lapse = 0.0;
				rounds = 0;
			}
			rounds++;
			// for one frame leak detection
			// done = true;
		}

		// Check if the user pressed escape and wants to quit.
		if(m_Input->IsKeyPressed(DIK_ESCAPE) == true)
		{
			done = true;
		}

		if ( m_Input->IsKeyPressed( DIK_N ))
		{
			m_Graphics->toggleNormals();
		}

		
	}

	return;
}


bool SystemClass::Frame( float dt )
{
	bool result = true;


	// Do the input frame processing.
	//result = m_Input->Frame();
	if(!result)
	{
		return false;
	}

	// Do the frame processing for the graphics object.
	result = m_Graphics->Frame( dt );

	if(!result)
	{
		return false;
	}

	// Finally render the graphics to the screen.
	result = m_Graphics->Render( dt );

	//result = m_Graphics->testRenderHUD( dt );
	if(!result)
	{
		return false;
	}


	return true;
}


LRESULT CALLBACK SystemClass::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	if (  m_Input && m_Graphics )
	{
		int mouseX, mouseY;
		m_Input->MsgProc(hwnd, umsg, wparam, lparam);
		m_Input->GetMouseLocation(mouseX, mouseY);
		m_Graphics->onMouseMove( mouseX, mouseY );
	}

	if ( HUD::HUDManager::Instance().processMessage( hwnd, umsg, wparam, lparam ) )
	{
		m_Input->releaseMouseClicks();
		return 0;
	}

	if ( HUD::HUDControls::Instance().processMessage( hwnd, umsg, wparam, lparam ) )
	{
		m_Input->releaseMouseClicks();
		return 0;
	}

	if ( m_Graphics && m_Input ) 
	{
		FreeCamera* fc = m_Graphics->getCamera(); 
		if ( fc )
		{
			fc->handleInput( hwnd, umsg, wparam, lparam);
		}

		if ( m_Graphics->isInitialized() )
		{
			if(umsg == WM_KEYDOWN)
			{
				if((char)wparam == VK_F1)
				{
					m_Graphics->toggleNormals();
				}
				return 1;
			}

			m_Graphics->onMouseClick( m_Input->IsLeftMouseButtonDown(), m_Input->IsRightMouseButtonDown() );
		}

		
	}

	return DefWindowProc(hwnd, umsg, wparam, lparam);
}


void SystemClass::InitializeWindows(int& screenWidth, int& screenHeight)
{
	WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int posX, posY;


	// Get an external pointer to this object.	
	ApplicationHandle = this;

	// Get the instance of this application.
	m_hinstance = GetModuleHandle(NULL);

	// Give the application a name.
	m_applicationName = L"Engine";

	// Setup the windows class with default settings.
	wc.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc   = WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = m_hinstance;
	wc.hIcon		 = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm       = wc.hIcon;
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = m_applicationName;
	wc.cbSize        = sizeof(WNDCLASSEX);
	
	// Register the window class.
	RegisterClassEx(&wc);

	// Determine the resolution of the clients desktop screen.
	screenWidth  = GetSystemMetrics(SM_CXSCREEN);
	screenHeight = GetSystemMetrics(SM_CYSCREEN);

	// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
	if(FULL_SCREEN)
	{
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize       = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth  = (unsigned long)screenWidth;
		dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
		dmScreenSettings.dmBitsPerPel = 32;			
		dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		posX = posY = 0;
	}
	else
	{
		// If windowed then set it to 800x600 resolution.
		screenWidth  = 800;
		screenHeight = 600;

		// Place the window in the middle of the screen.
		posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth)  / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
	}

	// Create the window with the screen settings and get the handle to it.
	m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_applicationName, m_applicationName, 
						    WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
						    posX, posY, screenWidth, screenHeight, NULL, NULL, m_hinstance, NULL);

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(m_hwnd, SW_SHOW);
	SetForegroundWindow(m_hwnd);
	SetFocus(m_hwnd);
	
	// show mouse cursor.
	ShowCursor(false);

	return;
}


void SystemClass::ShutdownWindows()
{
	// Show the mouse cursor.
	ShowCursor(true);

	// Fix the display settings if leaving full screen mode.
	if(FULL_SCREEN)
	{
		ChangeDisplaySettings(NULL, 0);
	}

	// Remove the window.
	DestroyWindow(m_hwnd);
	m_hwnd = NULL;

	// Remove the application instance.
	UnregisterClass(m_applicationName, m_hinstance);
	m_hinstance = NULL;

	// Release the pointer to this class.
	ApplicationHandle = NULL;

	return;
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
	switch(umessage)
	{
		// Check if the window is being destroyed.
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}

		// Check if the window is being closed.
		case WM_CLOSE:
		{
			PostQuitMessage(0);		
			return 0;
		}

		// All other messages pass to the message handler in the system class.
		default:
		{
			return ApplicationHandle->MessageHandler(hwnd, umessage, wparam, lparam);
		}
	}
}