#include "HeightMapAdapter.h"	


HeightMapAdapter::HeightMapAdapter( const AttributeTool::AttributeLayer<D3DXVECTOR4>& hm )
	: m_heightMap( hm )
{

}

int HeightMapAdapter::GetSizeX( )
{
	return m_heightMap.getW();
}

int HeightMapAdapter::GetSizeY( )
{
	return m_heightMap.getH();
}

// returned value is converted to height using following formula:
// 'WorldHeight = WorldMinZ + GetHeightAt(,) * WorldSizeZ / 65535.0f'
unsigned short  HeightMapAdapter::GetHeightAt( int x, int y )
{
	return (short)(m_heightMap.get( x, y ).x * 65535);
}

void HeightMapAdapter::GetAreaMinMaxZ( int x, int y, int sizeX, int sizeY, unsigned short & minZ, unsigned short & maxZ )
{
	assert( x >= 0 && y >= 0 && (x+sizeX) <= (int)m_heightMap.getW() && (y+sizeY) <= (int)m_heightMap.getH()  );
	minZ = 65535;
	maxZ = 0;

	for( int ry = y; ry < y + sizeY; ry++ )
		for( int rx = x; rx < x + sizeX; rx++ )
		{
			float hm = m_heightMap.get( rx, ry ).x;
			unsigned short h = (unsigned short)( hm* 65535);
			minZ = std::min( minZ, h );
			maxZ = std::max( maxZ, h );
		}
}
