////////////////////////////////////////////////////////////////////////////////
// Filename: cameraclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "cameraclass.h"


CameraClass::CameraClass()
{
	m_positionX = 0.0f;
	m_positionY = 0.0f;
	m_positionZ = 0.0f;

	m_rotationX = 0.0f;
	m_rotationY = 0.0f;
	m_rotationZ = 0.0f;
}


CameraClass::CameraClass(const CameraClass& other)
{
}


CameraClass::~CameraClass()
{
}


void CameraClass::SetPosition(float x, float y, float z)
{
	m_positionX = x;
	m_positionY = y;
	m_positionZ = z;
	return;
}


void CameraClass::SetRotation(float x, float y, float z)
{
	m_rotationX = x;
	m_rotationY = y;
	m_rotationZ = z;
	return;
}


D3DXVECTOR3 CameraClass::GetPosition()
{
	return D3DXVECTOR3(m_positionX, m_positionY, m_positionZ);
}


void CameraClass::Render( RenderMode rendermode )
{
	D3DXVECTOR3 up;
	D3DXVECTOR3 position;
	
	D3DXVECTOR3 lookAt;
	float radians;


	// Setup the vector that points upwards.
	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;

	// Setup the position of the camera in the world.
	position.x = m_positionX;
	position.y = m_positionY;
	position.z = m_positionZ;

	switch(rendermode)
	{
		case USE_LOOKAT:
			lookAt.x = m_lookX;
			lookAt.y = m_lookY;
			lookAt.z = m_lookZ;
			break;

		case USE_ROTATE:
			// Calculate the rotation in radians.
			radians = m_rotationY * 0.0174532925f;

			// Setup where the camera is looking.
			lookAt.x = sinf(radians) + m_positionX;
			lookAt.y = m_positionY;
			lookAt.z = cosf(radians) + m_positionZ;
			break;
	}
	// Create the view matrix from the three vectors.
	D3DXMatrixLookAtLH(&m_viewMatrix, &position, &lookAt, &up);

	return;
}

void CameraClass::SetLookAt(float x, float y, float z)
{
	m_lookX = x;
	m_lookY = y;
	m_lookZ = z;
}



void CameraClass::GetViewMatrix(D3DXMATRIX& viewMatrix)
{
	viewMatrix = m_viewMatrix;
	return;
}