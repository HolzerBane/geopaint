#include "HUDControls.h"
#include "..\WinGUI\IWinGUI.h"
#include "..\WinGUI\WinGuiFactory.h"
#include "PaintBrush.h"

using namespace HUD;


HUDControls& HUDControls::Instance()
{
	static HUDControls hudControls;

	return hudControls;
}


void HUDControls::initialize( 
			CDXUTDialogResourceManager& manager, 
			const unsigned width, const unsigned height,  
			const unsigned posX, const unsigned posY
	)
{
	CDXUTDialog& dialog = Instance().m_dialog;

	dialog.SetLocation( posX, posY );
	dialog.SetSize( width, height );
	dialog.Init( &manager );
}

void HUDControls::initializeControls( const TerrainTools::PaintBrush& brush )
{
	CDXUTDialog& dialog = Instance().m_dialog;

	unsigned row				= 10;
	const unsigned rowIncrement	= 25;
	const unsigned rightMargin	= 10;
	const unsigned defaultWidth	= 100;

	dialog.AddButton( B_NewTerrain, L"New Terrain", rightMargin, row, 100, 20, 0u, false, &m_newTerrain ); row += rowIncrement;
	dialog.AddButton( B_SaveTerrain, L"Save Terrain", rightMargin, row, 100, 20, 0u, false, &m_saveTerrain ); row += rowIncrement * 2;
	dialog.AddStatic( -1, L"Brush Settings:", rightMargin, row + rowIncrement, defaultWidth, 22 ); row += rowIncrement;
	dialog.AddStatic( -1, L"Selected Layer:", rightMargin, row + rowIncrement, defaultWidth, 22 );row += rowIncrement;
	dialog.AddComboBox( CB_BrushLayer, rightMargin, row + rowIncrement, defaultWidth, rowIncrement - 5, 0u, false, &m_brushLayer );  row += rowIncrement;
	dialog.AddCheckBox( CB_BrushEraser, L"Erase Mode:", rightMargin, row + rowIncrement, defaultWidth, 22, false, 0, false, &m_brushEraser ); row += rowIncrement;
	dialog.AddStatic( -1, L"Brush bias:", rightMargin, row + rowIncrement, defaultWidth, 22 );row += + rowIncrement;
	dialog.AddSlider( S_BrushBias,		rightMargin, row + rowIncrement, defaultWidth, rowIncrement - 5, 0, 20, brush.BrushType.Bias,	false, &m_brushBias );  row += rowIncrement;
	dialog.AddStatic( -1, L"Brush Opacity:", rightMargin, row + rowIncrement, defaultWidth, 22 );row += rowIncrement;
	dialog.AddSlider( s_BrushOpacity,	rightMargin, row + rowIncrement, defaultWidth, rowIncrement - 5, 0, 100, brush.BrushType.Opacity * 100, false, &m_brushOpacity );  row += rowIncrement;
	dialog.AddStatic( -1, L"Brush Size:", rightMargin, row + rowIncrement, defaultWidth, 22 );row += + rowIncrement;
	dialog.AddSlider( S_BrushSize,		rightMargin, row + rowIncrement, defaultWidth, rowIncrement - 5, 1, 100,  brush.BrushType.Size, false, &m_brushSize );  row += rowIncrement;
}

bool HUDControls::processMessage( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	return Instance().m_dialog.MsgProc( hWnd, msg, wParam, lParam );
}

void HUDControls::render( float dt )
{
	Instance().m_dialog.OnRender(dt);
}

void HUDControls::setCallback( PCALLBACKDXUTGUIEVENT callback )
{
	Instance().m_dialog.SetCallback( callback );	
}

void HUDControls::setBackGround( const D3DCOLOR& color )
{
	Instance().m_dialog.SetBackgroundColors( color );
}

void HUDControls::addLayer( const unsigned id, const std::wstring& name )
{
	if ( m_brushLayer )
	{
		m_brushLayer->AddItem( name.c_str(), NULL );
	}
}

void HUDControls::clearLayers()
{
	if ( m_brushLayer )
	{
		m_brushLayer->RemoveAllItems();
	}
}

void HUDControls::processGUIEvent( const int controlId, const int eventId )
{
	switch( controlId )
	{
	case HUDControls::CB_BrushLayer :
		{
			// dereferencing unsigned pointer!
			const unsigned layerId = m_brushLayer->GetSelectedIndex();
			Instance().BrushLayerChanged( layerId );
			break;
		}
	case HUDControls::S_BrushBias :
		Instance().BrushBiasChanged( m_brushBias->GetValue() );
		break;
	case HUDControls::s_BrushOpacity :
		Instance().BrushOpacityChanged( m_brushOpacity->GetValue() );
		break;
	case HUDControls::S_BrushSize :
		Instance().BrushSizeChanged( m_brushSize->GetValue() );
		break;
	case HUDControls::CB_BrushEraser :
		Instance().BrushEraserChanged( m_brushEraser->GetChecked() );
		break;

	case HUDControls::B_NewTerrain :
		TerrainGeneratorStruct::Data returnData;
		ShowCursor(true);
		if ( WinGuiFactory::CreateWinGUI()->NewTerrainDialog( GetModuleHandle(nullptr), 1, returnData) )
		{
			Instance().NewTerrainRequested( returnData );
		}
		ShowCursor(false);
		break;
	}
}


HUDControls::HUDControls()
	: m_brushLayer(nullptr)
	, m_brushBias(nullptr)
	, m_brushOpacity(nullptr)
	, m_brushSize(nullptr)
{
}