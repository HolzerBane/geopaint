#pragma once
#include <memory>
#include <vector>
#include "TextureObject.h"

namespace TerrainModel
{

class TextureContainer
{
public:
	typedef unsigned TextureId;
	typedef std::shared_ptr< TextureObject > TextureObjectPtr;
	typedef std::vector< TextureObjectPtr > TextureVector;

	TextureContainer();
	~TextureContainer();

	TextureId addTexture( TextureObjectPtr tex );
	TextureObjectPtr getTexture( TextureId ); 
	unsigned getContainerSize() const { return m_textures.size(); }

private:
	TextureVector m_textures;


};

}