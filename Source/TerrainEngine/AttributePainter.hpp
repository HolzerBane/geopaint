#include "AttributePainter.h"

//#include "AttributeLayer.h"
#include "Utility\perlinV2.h"
#include "AttributeTools.h"
#include <ppl.h>

using namespace AttributeTool;
#undef min
#undef max
//#################################################################################################

template< class TValue >
AttributePainter< TValue >::AttributePainter( AttributeLayer< TValue >& atl )
	:	m_usedLayer( atl )
{
	m_gaussCache.radius = 0;
	m_gaussCache.bias	= 0.f;
}
//#################################################################################################

template< class TValue >
AttributePainter< TValue >::AttributePainter( const AttributePainter& other)
	: m_usedLayer( other.m_usedLayer)
{

}

//#################################################################################################

template< class TValue >
void AttributePainter< TValue >::drawSpot( 
	const Index cx, const Index cy, const unsigned radius, float bias, const TValue& value, const bool additive )
{
	if ( m_gaussCache.radius != radius || m_gaussCache.bias != bias )
	{
		generateGaussCache( radius, bias );
	}

	// gaussian surface
	// 1.3*e^(-((t-3)/1.9)^2) * 1.3*e^(-((s-3)/1.9)^2)
	// e^(-(t*2)^2) *e^(-(s*2)^2)
	// start - top left, end - bottom right 
	Coordinate2D start( std::max( 0, (int)cx - (int)radius) , std::max( 0, (int)cy - (int)radius) );
	Coordinate2D end(	std::min( m_usedLayer.getW()-1, cx + radius), std::min( m_usedLayer.getH()-1, cy + radius) );
	const unsigned diameter = radius * 2;
	//for( int x = start.first; x != end.first; ++x )
	//for( int y = start.second; y != end.second; ++y )
	const unsigned xSteps = end.first	- start.first;
	const unsigned ySteps = end.second	- start.second;
	
	unsigned xOffset = 0u;
	unsigned yOffset = 0u;
	if ( start.first == 0 )
	{
		xOffset = diameter - xSteps;
	}
	if ( start.second == 0 )
	{
		yOffset = diameter - ySteps;
	}

	Concurrency::parallel_for( 0u, xSteps * ySteps, [additive, &value, &start, &end, xSteps, ySteps, xOffset, yOffset, this]( const unsigned i )
	{
		const unsigned x = start.first  + i % xSteps;
		const unsigned y = start.second + i / xSteps;

		float gauss = m_gaussCache.data.get(x - start.first + xOffset, y - start.second + yOffset);

		TValue finalVal = value * gauss;
		if (additive)
		{
			m_usedLayer.add( x, y, finalVal );
		}
		else
		{
			m_usedLayer.set( x, y, finalVal);
		}
	}
	);
}

template< class TValue >
void AttributePainter< TValue >::generateGaussCache( const unsigned radius, const float bias )
{
	const unsigned diameter = radius * 2;

	m_gaussCache.radius	= radius;
	m_gaussCache.bias	= bias;
	m_gaussCache.data.reallocate( diameter, diameter );
	// gaussian surface
	// 1.3*e^(-((t-3)/1.9)^2) * 1.3*e^(-((s-3)/1.9)^2)
	// e^(-(t*2)^2) *e^(-(s*2)^2)
	float rf = (float)radius;
	Concurrency::parallel_for( 0u, diameter * diameter, [this, diameter, rf, bias, radius]( const unsigned i )
	{
		const int x = i % diameter;
		const int y = i / diameter;
		float s = ( x - (int)radius ) / rf; 
		float t = ( y - (int)radius ) / rf; 
		float gauss = std::expf( -std::powf( s * bias, 2 ) ) * std::expf( -std::powf( t * bias, 2 ) );

		m_gaussCache.data.set( x, y, gauss);
	}
	);
}

//#################################################################################################

template< class TValue >
void AttributePainter< TValue >::fillRegion( 
	const Index x1, const Index y1, const Index x2, const Index y2, const TValue& value )
{
	// bottom left: x1, y1
	// top right  : x2, y2
	for ( Index x = x1; x1 < x2; ++x)
	for ( Index y = y1; y1 < y2; ++y)
	{
		m_usedLayer.set( x, y, value);
	}
}
//#################################################################################################
template< class TValue >
void AttributePainter< TValue >::fillPerlin( const Index x1, const Index y1, const Index x2, const Index y2 )
{
	Algorithm::Noise::PerlinV2 p( 12 , 0.04f, 1, 1 );

	for ( unsigned y = y1; y < y2; ++y)
	for ( unsigned x = x1; x < x2; ++x)
	{
		m_usedLayer.set( x, y, (float)( p.Get((float)x, (float)y) + 1 )  / 2.f );
	}
}
//#################################################################################################

template< class TValue >
void AttributePainter< TValue >::fillPerlin( )
{
	fillPerlin( 0, 0, m_usedLayer.getW(), m_usedLayer.getH());
}
//#################################################################################################

template< class TValue >
void AttributePainter< TValue >::makeRidges( const Coordinates& coords, const unsigned size )
{

}
//#################################################################################################

static int nodeCount = 0;

template< class TValue >
void AttributePainter< TValue >::makePeaks( const Coordinates& coords, const unsigned size )
{
	// fixed seed
	srand(103);
	m_stateVar = size / 15;
	for( auto peakIt = coords.begin(); peakIt != coords.end(); ++peakIt)
	{
		spread( *peakIt, size );
	}
#if 0 	
	// set corners..
	m_usedLayer.set(1, 1, 1.f);
	m_usedLayer.set(1, m_usedLayer.getH() - 2, 1.f);
	m_usedLayer.set(m_usedLayer.getW() - 2, 0,  1.f);
	m_usedLayer.set(m_usedLayer.getW() - 2, m_usedLayer.getH() - 2,  1.f);

	std::vector< Index > priNodes;
	float falloff = 0.001f;
	// approximate size
	// initialize primary nodes
	Index index = 0;
	
	for( auto peakIt = coords.begin(); peakIt != coords.end(); ++peakIt)
	{
		// elevate peak
		float height = 1.0f - (rand() % 6) * 0.05f;
		m_usedLayer.set( peakIt->first, peakIt->second, height);
		//		   position, branch count, level, dir
		Node node( *peakIt, 1.0f, 10, 3, (Dir)(rand() % 8) ); 
		// +3 dirs
		for ( unsigned i = 0; i < rand() % 3 + 3; ++i )
		{
			node.addBranch( (Dir)(rand() % 8) );
		}
		
		nodeCount = coords.size();
		spread(node, size);
	}
#endif
}
//#################################################################################################

template< class TValue >
void AttributePainter< TValue >::erode()
{
	// later!
}
//#################################################################################################

template< class TValue >
void AttributePainter< TValue >::spread( const Coordinate2D& coord, const unsigned size )
{
	// test
	const Coordinate2D center( 256, 256 );

	static const auto hasValFn = []( const TValue& t ){ return t > 0.0f; };
	const unsigned neighCheck = 6;
	const unsigned n = neighborsWithProp( coord, hasValFn, neighCheck );
	const unsigned thresh = size / m_stateVar; 
	if ( n > thresh ) return;
	n = neighborsWithProp( coord, hasValFn, 3 );
	if ( n > 4) return;
	
	m_usedLayer.set( coord.first, coord.second, 1.f );
	if ( size - 1 == 0 ) return;

	unsigned tries = 0;

	for (; tries < 8; ++tries )
	{
		const int cx = abs( coord.first - center.first );
		const int cy = abs( coord.second - center.second );
		const unsigned xySqrt = cx*cx + cy*cy;
		const Dir dir = (Dir)(rand() % DirCount);
		const Coordinate2D next = ShiftFn::Shift[dir](coord);
		// skip if this move would be towards the center
		const int nx = abs ( next.first - center.first );
		const int ny = abs ( next.second- center.second);
		const unsigned nSqrt = nx*nx + ny * ny;

		if ( nSqrt + 16 <= xySqrt )
			continue;
		
		if ( m_usedLayer.isInbound( next.first, next.second ) == false ) return;

		spread( next, size - 1 );
	}
}

//#################################################################################################

template< class TValue >
void AttributePainter< TValue >::spread( Node& n, unsigned size )
{	
	if ( n.life == 0 || nodeCount > 100 )  
	{
		nodeCount--;
		return;
	}

	static const auto hasValFn = []( const TValue& t ){ return t > 0.0f; };
	unsigned neigh = neighborsWithProp( n.pos, hasValFn );
	if ( neigh > 1) return;

	Coordinate2D lastCoord = n.pos;
	unsigned branchFactor = size / n.branches.size();
	unsigned origBranches = n.branchesLeft;
	for ( auto dirIt = n.branches.begin(); dirIt != n.branches.end(); ++dirIt )
	{
		Dir lastDir = *dirIt;
		n.branchesLeft = origBranches;
		for ( unsigned sIt = 0; sIt < size; ++sIt )
		{
			lastCoord = ShiftFn::Shift[ lastDir ]( lastCoord );
			// set attribute
			if (!m_usedLayer.set( lastCoord.first, lastCoord.second, n.value ))
			{
				// we are out of bounds
				break;
			}
			// randomly change direction
			if ( (rand() % 3 ) == 0)
			{
				lastDir = diverge( lastDir, 2);
			}

			// randomly branch
			if ( (rand() % branchFactor ) == 0 && n.branchesLeft > 0)
			{
				Node newNode( lastCoord, n.value, n.life - 1, n.branchesLeft, lastDir );
				nodeCount++;
				newNode.addBranch( diverge( lastDir, 2) );
				newNode.addBranch( diverge( lastDir, 2) );
				spread( newNode, size - 1);
				n.branchesLeft--;

			}
		}
	}
}
//#################################################################################################

template< class TValue >
unsigned AttributePainter< TValue >::neighborsWithProp( 
	const Coordinate2D coord,  
	std::function< bool(const TValue&) > propFn, unsigned range )
{
	unsigned found = 0;
	unsigned ro2 = range / 2;
	for( Index x = coord.first - ro2; x <= coord.first + ro2; ++x )
		for( Index y = coord.second - ro2; y <= coord.second + ro2; ++y )
		{
			if ( m_usedLayer.isInbound(x, y))
			{
				float val =  m_usedLayer.get( x, y );
				if (  propFn(val) )
				{
					found++;
				}
			}
		}

	return found;
}
