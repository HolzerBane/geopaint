#pragma once
#include <array>
#include <functional>
#include "AttributeTypes.h"

namespace AttributeTool
{
	enum Dir
	{
		N,
		NE,
		E,
		SE,
		S,
		SW,
		W,
		NW,
	};

	static const unsigned DirCount = 8;

	typedef std::function< Coordinate2D(const Coordinate2D& ) > DirFunction;
	typedef std::array< DirFunction, DirCount > DirFunctionArray;
	struct ShiftFn
	{
		static DirFunctionArray Shift;
	};
}