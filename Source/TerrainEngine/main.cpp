////////////////////////////////////////////////////////////////////////////////
// Filename: main.cpp
////////////////////////////////////////////////////////////////////////////////
#include "DXUT.h"
#include "DXUTmisc.h"
#include "DXUTcamera.h"
#define CONSOLE_ON
#include "Utility\DebugConsole.h"

#include "systemclass.h"
#include "..\WinGUI\WinGuiFactory.h"
#include "..\WinGUI\IWinGUI.h"
#include "..\WinGUI\resource.h"
#include "..\winGUI\TerrainGeneratorStruct.h"

#if defined(DEBUG) || defined(_DEBUG)
#include <vld.h>
#endif

#pragma comment(linker, \
  "\"/manifestdependency:type='Win32' "\
  "name='Microsoft.Windows.Common-Controls' "\
  "version='6.0.0.0' "\
  "processorArchitecture='*' "\
  "publicKeyToken='6595b64144ccf1df' "\
  "language='*'\"")

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
	// check ShaderCompiler error output
	#ifdef _DEBUG
	// check what the shader compiler did
	std::wifstream ifs("log.txt");
	if ( ifs.is_open())
	{
		std::wstringstream ss;
		ss << ifs.rdbuf();
		std::wstring logFileData = ss.str();

		if ( logFileData.length() > 0 )
		{
			std::wstring errorMsg = L"There were errors compiling the shaders.\nRead log.txt and weep:\n\n";
			errorMsg += logFileData;
			MessageBox( 0, errorMsg.c_str(), L"Shader Compile Error", MB_OK | MB_ICONERROR );  

			return -1;
		}
	}
	#endif

	DebugConsole::RedirectIOToConsole( false, true );

	// get resources from dll... not if it's working or anything
	HRSRC hSrc = NULL;
	HMODULE lib =::LoadLibraryW(L"WinGUI.dll");
	HMODULE hMod = GetModuleHandle(L"WinGUI.dll");
	hSrc = ::FindResourceW(hMod, MAKEINTRESOURCE(IDD_NEW_TERRAIN), MAKEINTRESOURCE(RT_DIALOG));
	::FindResourceW(nullptr, MAKEINTRESOURCE(101), MAKEINTRESOURCE(RT_DIALOG) );
	
	// initialize DXUT framework
	DXUTInit( true, true ); // Parse the command line and show msgboxes

	// Create the system object.
	SystemClass* System;
	bool result;

	System = new SystemClass;
	if(!System)
	{
		return -1;
	}

	// Initialize and run the system object.
	result = System->Initialize();
	if(result)
	{
		System->Run();
	}

	// Shutdown and release the system object.
	System->Shutdown();
	delete System;
	System = nullptr;

	return 0;
}