#pragma once

// these two in this order are necessary
#include <DXUT.h>
#include <DXUTgui.h>

struct ID3D10Device;

namespace HUD
{

class HUDManager
{
public:
	static HUDManager& Instance();
	void registerDialog( CDXUTDialog& dialog );
	CDXUTDialogResourceManager& getResourceManager();
	bool processMessage( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
	HRESULT setDevice( ID3D10Device* device );
	HRESULT setSwapChain(  ID3D10Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc );

private:
	HUDManager(){}
	HUDManager( const HUDManager& hm ){}

	CDXUTDialogResourceManager	m_resourceManager;
};

}