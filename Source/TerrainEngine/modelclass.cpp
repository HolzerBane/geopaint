
#include <d3d10_1.h>
#include <assert.h>
#include <sstream>

#include "modelclass.h"
//#include "AttributeLayer.hpp"
#include "AttributeMap.hpp"
#include "AttributePainter.hpp"
#include "AttributeFactory.h"
#include "HeightMapAdapter.h"
#include "TerrainShader.h"
#include "TextureClass.h"
#include "TextureShader.h"
#include "PaintBrushHandler.h"
#include "..\WinGUI\TerrainGeneratorStruct.h"
#include "Paths.h"

#include "Math.h"

#define CONSOLE_ON
#include "Utility\DebugConsole.h"
#include "Utility\File.h"
#include "Utility\FreeCamera.h"
#include "Utility\dxCanvas\DxCanvas.h"

#include <D3dx10math.h>

#undef min
#undef max

using namespace TerrainModel;
using namespace std;

#define LIGHT	0
//#################################################################################################

ModelClass::ModelClass()
	: m_mapModel(256, 256)
	, m_lightPos(50.f, 20.f, 0.f)
	, m_wireframe(false)
	, m_ha( m_mapModel.getHeightNormalMap() )
	, m_mapSizeFactor(2.0f)
{
	m_vertexBuffer = 0;
	m_indexBuffer = 0;
	m_TextureArray = 0;
	m_showNormals = true;

	m_paintBrush.Layer = 1;
	m_paintBrush.BrushType.Bias = 8.f;
	m_paintBrush.BrushType.Opacity = 0.8f;
	m_paintBrush.BrushType.Size = 50;
	m_paintBrush.BrushType.Step = 1;

	m_brushViewPoint.SetProjectionParameters((float)(D3DX_PI / 2.0f), 1.0f, 0.01f, 100.0f);
}

//#################################################################################################

ModelClass::ModelClass(const ModelClass& other)
	: m_mapModel(128, 128)
	, m_ha( m_mapModel.getHeightNormalMap() )

{

}

//#################################################################################################

ModelClass::~ModelClass()
{
}

//#################################################################################################

bool ModelClass::Initialize(ID3D10Device* device)
{
	auto st =  DebugConsole::scoped_trace( L"ModelClass" );

	m_device = device;
	bool result = true;
	m_mapModel.setDevice( device );

	// Load in the model data.

#if 0

	result = LoadModel(modelFilename);
	if(!result)
	{
		return false;
	}

	// Initialize the vertex and index buffer that hold the geometry for the triangle.

#endif
	result = InitializeBuffers(device);
#if 0
	m_mapModel.generateRandom();
	m_mapModel.fillBuffersWithLayer( device );
	m_indexCount = m_mapModel.getIndexCount();
#endif
	if(!result)
	{
		return false;
	}

	// Load the textures for this model.
	result = LoadTextures(device);
	if(!result)
	{
		return false;
	}

	return true;
}

//#################################################################################################

void ModelClass::Shutdown()
{
	// Release the model textures.
	ReleaseTextures();

	// Release the vertex and index buffers.
	ShutdownBuffers();

	return;
}

//#################################################################################################

bool ModelClass::Frame( float dt )
{
#if LIGHT
	float angle = dt / 60.f; 
	D3DXMATRIX zrot, trans;
	D3DXMatrixRotationZ( &zrot, angle);
	D3DXMatrixTranspose( &trans, &zrot );
	m_lightPos = Math::mulMatVec( trans, m_lightPos );
#endif

	return true;
}
//#################################################################################################

void ModelClass::Render(ID3D10Device* device)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(device);

	return;
}

//#################################################################################################

int ModelClass::GetIndexCount() const
{
	return m_indexCount;
}

//#################################################################################################

ID3D10ShaderResourceView* ModelClass::GetTextureArray() const
{
	return m_TextureArray->GetTextureArray();
}
//#################################################################################################

ID3D10ShaderResourceView* ModelClass::GetNormalArray() const
{
	return m_TextureArray->GetNormalArray();
}
//#################################################################################################

ID3D10ShaderResourceView* ModelClass::GetPerlin() const
{
	return m_TextureArray->GetPerlin();
}
//#################################################################################################

ID3D10ShaderResourceView* ModelClass::GetBrush() const
{
	return m_TextureArray->GetBrush();
}
//#################################################################################################
bool ModelClass::InitializeBuffers(ID3D10Device* device)
{
	auto st = DebugConsole::scoped_trace(L"Initializing buffers");

	//initTestBuffer(device);
	//initFlatBuffer(device);

	initGridPatchBuffer( 32, device);

	// prepare CDLOD quad tree
	initTestMap();
	createQuadTree();
	// finished QuadTree

	return true;
}
//#################################################################################################

bool ModelClass::initGridPatchBuffer( const unsigned int dimension, ID3D10Device* device)
{
	m_gridPatch.setupGrid( dimension, device );

	const unsigned stride = sizeof(VertexType); 
	const unsigned offset = 0;

	m_indexCount = m_gridPatch.GetIndexCount();
	m_vertexCount = m_gridPatch.GetVertexCount();
	m_vertexBuffer = m_gridPatch.GetVertexBuffer();
	m_indexBuffer =  m_gridPatch.GetIndexBuffer();

	return true;
}

//#################################################################################################

bool ModelClass::initTestMap()
{
	//add attribute layers

	m_mapModel.clearAttributes();
	
	AttributeFactory attributeFactory;
	m_mapModel.addAttribute( attributeFactory.getFromImageChannel( Paths::Textures + L"Attr1.bmp", ImageConsts::Red ), 0.4f);
	m_mapModel.addAttribute( attributeFactory.getFromImageChannel( Paths::Textures + L"Attr2.bmp", ImageConsts::Red ));
	m_mapModel.addAttribute( attributeFactory.getFromImageChannel( Paths::Textures + L"Attr3.bmp", ImageConsts::Red ));
	m_mapModel.addAttribute( attributeFactory.getFromImageChannel( Paths::Textures + L"Attr4.bmp", ImageConsts::Red ), 0.1f);

	m_mapModel.generateHeightMapFromLayers();
	bool ok = m_mapModel.sendAttributesToDevice();	

	//m_mapModel.fuckUpTexture();

	return ok;
}

//#################################################################################################

bool ModelClass::initEmptyBuffer( const unsigned dimX, const unsigned dimY )
{
	//add attribute layers
	m_mapModel.addAttribute( AttributeTool::AttributeLayer<float>( dimX, dimY ));
	m_mapModel.addAttribute( AttributeTool::AttributeLayer<float>( dimX, dimY ));
	m_mapModel.addAttribute( AttributeTool::AttributeLayer<float>( dimX, dimY ));
	m_mapModel.addAttribute( AttributeTool::AttributeLayer<float>( dimX, dimY ));

	m_mapModel.generateHeightMapFromLayers();
	bool ok = m_mapModel.sendAttributesToDevice();	

	return ok;	
}

//#################################################################################################

void ModelClass::ShutdownBuffers()
{
	// Release the index buffer.
	if(m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}

//#################################################################################################

void ModelClass::RenderBuffers(ID3D10Device* device )
{
	// Set vertex buffer stride and offset.
	const unsigned stride = sizeof(VertexType); 
	const unsigned offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	device->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R16_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}
//#################################################################################################

void ModelClass::RenderCDLOD( ID3D10Device* device, const FreeCamera& camera )
{
	/* select the CDLOD nodes from the quad tree based on the camera */
	D3DXPLANE planes[6];
	camera.GetFrustumPlanes( planes );

	// Do the terrain LOD selection based on our camera
	// this will store the selection of terrain quads that we want to render 
	CDLODQuadTree::LODSelectionOnStack<4096>     cdlodSelection( camera.getEyePosition(), 1000.f, planes, 2 );
	
	// do the main selection process...
	m_quadTree.LODSelect( &cdlodSelection );
	/* start rendering the LOD patches based on the selection */

	// Debug view
	//m_quadTree.DebugDrawAllNodes();
	if( true )
	{
		static float dbgCl = 0.2f;
		static float dbgLODLevelColors[4][4] = { {1.0f, 1.0f, 1.0f, 1.0f}, {1.0f, dbgCl, dbgCl, 1.0f}, {dbgCl, 1.0f, dbgCl, 1.0f}, {dbgCl, dbgCl, 1.0f, 1.0f} };

		for( int i = 0; i < cdlodSelection.GetSelectionCount(); i++ )
		{
			const CDLODQuadTree::SelectedNode & nodeSel = cdlodSelection.GetSelection()[i];
			int lodLevel = nodeSel.LODLevel;

			bool drawFull = nodeSel.TL && nodeSel.TR && nodeSel.BL && nodeSel.BR;

			unsigned int penColor = (((int)(dbgLODLevelColors[lodLevel % 4][0] * 255.0f) & 0xFF) << 16)
				| (((int)(dbgLODLevelColors[lodLevel % 4][1] * 255.0f) & 0xFF) << 8)
				| (((int)(dbgLODLevelColors[lodLevel % 4][2] * 255.0f) & 0xFF) << 0)
				| (((int)(dbgLODLevelColors[lodLevel % 4][3] * 255.0f) & 0xFF) << 24);

			AABB boundingBox;
			nodeSel.GetAABB( boundingBox, m_quadTree.GetRasterSizeX(), m_quadTree.GetRasterSizeY(), m_quadTree.GetWorldMapDims() );
			boundingBox.Expand( -0.003f );
			if( drawFull )
			{
				DxCanvas::GetCanvas3D()->DrawBox( boundingBox.Min, boundingBox.Max, penColor );
			}
			else
			{
				float midX = boundingBox.Center().x;
				float midY = boundingBox.Center().y;

				if( nodeSel.TL )
				{
					AABB bbSub = boundingBox; bbSub.Max.x = midX; bbSub.Max.y = midY;
					bbSub.Expand( -0.002f );
					DxCanvas::GetCanvas3D()->DrawBox( bbSub.Min, bbSub.Max, penColor );
				}
				if( nodeSel.TR ) 
				{
					AABB bbSub = boundingBox; bbSub.Min.x = midX; bbSub.Max.y = midY;
					bbSub.Expand( -0.002f );
					DxCanvas::GetCanvas3D()->DrawBox( bbSub.Min, bbSub.Max, penColor );
				}
				if( nodeSel.BL ) 
				{
					AABB bbSub = boundingBox; bbSub.Max.x = midX; bbSub.Min.y = midY;
					bbSub.Expand( -0.002f );
					DxCanvas::GetCanvas3D()->DrawBox( bbSub.Min, bbSub.Max, penColor );
				}
				if( nodeSel.BR ) 
				{
					AABB bbSub = boundingBox; bbSub.Min.x = midX; bbSub.Min.y = midY;
					bbSub.Expand( -0.002f );
					DxCanvas::GetCanvas3D()->DrawBox( bbSub.Min, bbSub.Max, penColor );
				}
			}
		}
	}
	// Render
	unsigned renderedTriangles = 0;
	for( int i = cdlodSelection.GetMinSelectedLevel(); i <= cdlodSelection.GetMaxSelectedLevel(); i++ )
	{
		RenderQuadTree( device, cdlodSelection, i, renderedTriangles );
	}

	std::stringstream triStream;
	triStream << "Rendered Triangles: " << renderedTriangles;
	DxCanvas::GetCanvas2D()->DrawString( triStream.str().c_str() );
}

//#################################################################################################

HRESULT ModelClass::RenderQuadTree(  
	ID3D10Device* device, 
	const CDLODQuadTree::LODSelectionOnStack<4096>& cdlodSelection,
	int filterLodLevel, unsigned& renderedTriangles ) const
{
	/* set the buffers for rendering */
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	const unsigned stride = sizeof(VertexType); 
	const unsigned offset = 0;

	ID3D10Buffer* vertices = m_gridPatch.GetVertexBuffer();
	ID3D10Buffer* indices =  m_gridPatch.GetIndexBuffer();

	device->IASetVertexBuffers(0, 1, &vertices, &stride, &offset);
	// Set the index buffer to active in the input assembler so it can be rendered.
	device->IASetIndexBuffer( indices, DXGI_FORMAT_R16_UINT, 0);
	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	const CDLODQuadTree::SelectedNode * selectionArray = cdlodSelection.GetSelection();
	const int selectionCount = cdlodSelection.GetSelectionCount();

	int qtRasterX = cdlodSelection.GetQuadTree()->GetRasterSizeX();
	int qtRasterY = cdlodSelection.GetQuadTree()->GetRasterSizeY();
	MapDimensions mapDims = cdlodSelection.GetQuadTree()->GetWorldMapDims();

	int prevMorphConstLevelSet = -1;
	for( int i = 0; i < selectionCount; i++ )
	{
		const CDLODQuadTree::SelectedNode & nodeSel = selectionArray[i];
		
		// Filter out the node if required
		if( filterLodLevel != -1 && filterLodLevel != nodeSel.LODLevel )
			continue;

		if( prevMorphConstLevelSet != nodeSel.LODLevel )
		{
			prevMorphConstLevelSet = nodeSel.LODLevel;
			float v[4];// = {1.0f, 1.0f, 1.0f, 1.0f};
			cdlodSelection.GetMorphConsts( prevMorphConstLevelSet, v );

			m_shader->m_morphLevels->SetRawValue( v, 0, sizeof(float) * 4 );
		}

		// debug render - should account for unselected parts below!
		bool drawFull = nodeSel.TL && nodeSel.TR && nodeSel.BL && nodeSel.BR;

		AABB boundingBox;
		nodeSel.GetAABB( boundingBox, qtRasterX, qtRasterY, mapDims );

		float offset[4] = { boundingBox.Min.x, (boundingBox.Min.y + boundingBox.Max.y) * 0.5f, boundingBox.Min.z, 0.0f };
		
		m_shader->setQuadRenderingParameters(
			D3DXVECTOR4(boundingBox.Min.x, (boundingBox.Min.y + boundingBox.Max.y) * 0.5f, boundingBox.Min.z, 0.0f),
			D3DXVECTOR4(boundingBox.Max.x-boundingBox.Min.x, (float)nodeSel.LODLevel, boundingBox.Max.z-boundingBox.Min.z, 0.0f )
			);

		unsigned gridDim = m_gridPatch.GetDimensions();

		int totalVertices = (gridDim+1) * (gridDim+1);
		int totalIndices = gridDim * gridDim * 2 * 3;

		//std::cout << nodeSel.BL << " " << nodeSel.BR << " " << nodeSel.TL << " " << nodeSel.TR << std::endl;

		if( drawFull )
		{
			int halfd = ((gridDim+1)/2) * ((gridDim+1)/2) * 2;
			int drawIndices = halfd * 3;

			m_shader->render( device, totalIndices, 0 );
			renderedTriangles += totalIndices/3;
		}
		else 
		{
			int halfd = ((gridDim+1)/2) * ((gridDim+1)/2) * 2;
			int drawIndices = halfd * 3;

			// TODO : can be optimized by combining calls
			if( nodeSel.TL )
			{
				m_shader->render( device, drawIndices, 0);
				renderedTriangles +=  halfd;
			}
			if( nodeSel.TR ) 
			{
				m_shader->render( device, drawIndices, m_gridPatch.GetIndexEndTL());
				renderedTriangles +=  halfd;
			}
			if( nodeSel.BL ) 
			{
				m_shader->render( device, drawIndices, m_gridPatch.GetIndexEndTR());
				renderedTriangles +=  halfd;
			}
			if( nodeSel.BR ) 
			{
				m_shader->render( device, drawIndices, m_gridPatch.GetIndexEndBL() );
				renderedTriangles +=  halfd;
			}
		}

	}

	return S_OK;
}

void ModelClass::calculateBrushPosition( 
	const FreeCamera& camera, const int mouseX, const int mouseY )
{

	D3DXMATRIX world; 
	D3DXMatrixIdentity( &world );
	const D3DXMATRIX& view   = camera.getViewMatrix();
	const D3DXMATRIX& proj   = camera.getProjMatrix();
	
	D3DXVECTOR3 camPos, camDir;

	UINT nViewports = 1;
    D3D10_VIEWPORT viewports;
	m_device->RSGetViewports( &nViewports, &viewports );

	const D3DXVECTOR3 mousePosN( (float)mouseX, (float)mouseY, 0 );
    const D3DXVECTOR3 mousePosF( (float)mouseX, (float)mouseY, 1 );

	D3DXVec3Unproject( &camPos, &mousePosN, &viewports, &proj, &view, &world );
	D3DXVec3Unproject( &camDir, &mousePosF, &viewports, &proj, &view, &world );
	camDir -= camPos;
	D3DXVec3Normalize( &camDir, &camDir );

	float maxDist = camera.getViewDistance();
	if ( m_quadTree.IntersectRay( camPos, camDir, maxDist, m_cursorPos ) )
	{
		// create the brush viewpoint 
		const auto& mapDims = m_quadTree.GetWorldMapDims();
		unsigned nIndexX = std::min( m_quadTree.GetRasterSizeX() - 1u, unsigned(( m_cursorPos.x + mapDims.SizeX / 2.f ) * m_quadTree.GetRasterSizeX() /  mapDims.SizeX )) ;
		unsigned nIndexY = std::min( m_quadTree.GetRasterSizeY() - 1u, unsigned(( m_cursorPos.z + mapDims.SizeY / 2.f ) * m_quadTree.GetRasterSizeY() /  mapDims.SizeY ));

		const D3DXVECTOR4& hNormal = m_mapModel.getHeightNormalMap().get( nIndexX, nIndexY );
		D3DXVECTOR3 normal( hNormal.y, hNormal.z + 1000.f, hNormal.w ); // orient the normal more upwards
		D3DXVec3Normalize( &normal, &normal );
		// offset the cursor position with the normal.. may need more precise calculation
		m_cursorSource = m_cursorPos + m_paintBrush.BrushType.Size / 4.f * normal;

		m_brushViewPoint.SetPosition( m_cursorSource );
		m_brushViewPoint.SetLookAt( m_cursorPos );
		m_brushViewPoint.GenerateViewMatrix();
		m_brushViewPoint.GenerateProjectionMatrix();
	}
	
}
//#################################################################################################

void ModelClass::setBrushSize( const unsigned size )
{
	m_paintBrush.BrushType.Size = size;
} 

void ModelClass::setBrushBias( const unsigned bias )
{
	m_paintBrush.BrushType.Bias = (float)bias;
}

void ModelClass::setBrushOpacity( const unsigned opacity )
{
	m_paintBrush.BrushType.Opacity= (float)opacity / 100.f;
}

void ModelClass::setBrushLayer( const unsigned layerId )
{
	m_paintBrush.Layer = layerId;
}
void ModelClass::setBrushEraser( const bool isErasing )
{
	m_paintBrush.BrushType.Opacity = 
		(isErasing) ? 
		(-std::fabs(m_paintBrush.BrushType.Opacity)) : 
		( std::fabs(m_paintBrush.BrushType.Opacity));
}

bool ModelClass::newTerrain( const TerrainGeneratorStruct::Data& data )
{
	for( unsigned index = 0; index < data.layerCount; ++index )
	{
		if ( !File::fileExists(data.layers[index].attributePath ) || !File::fileExists(data.layers[index].texturePath ) )
		{
			return false;
		}
	}

	m_mapModel.clearAttributes();
	for( unsigned index = 0; index < data.layerCount; ++index )
	{
		m_mapModel.addAttribute( AttributeTool::AttributeFactory::getFromImageChannel( data.layers[index].attributePath ) );
	}

	m_mapModel.generateHeightMapFromLayers();
	bool ok = m_mapModel.sendAttributesToDevice();	

	createQuadTree();
	m_shader->bindEffectVars(*this);
	m_shader->SetConstShaderParameters();

	return true;
}

//#################################################################################################

bool ModelClass::handleBrushRequest(  )
{
	// mapping to map coordinates
	// TODO : this should be brush dependant eventually
	float x = m_cursorPos.x * m_mapModel.attributeSizeX() / m_mapDiemnsions.SizeX + m_mapModel.attributeSizeX() / 2.f;
	float y = m_cursorPos.z * m_mapModel.attributeSizeY() / m_mapDiemnsions.SizeY + m_mapModel.attributeSizeY() / 2.f;
	if ( !m_paintBrushHandler.allowPaint( m_paintBrush, unsigned(x), unsigned(y) ) )
	{
		return false;
	}

	m_mapModel.paintAttributeLayer( m_paintBrush, x, y );

	// quick test for quad tree re creation
	if ( m_paintBrush.Layer == 4 )
	{
		createQuadTree();
	}
	return true;
}

//#################################################################################################

void ModelClass::onBrushRequestEnd()
{
	m_paintBrushHandler.resetPrevState();
}

//#################################################################################################

bool ModelClass::LoadTextures(ID3D10Device* device)
{
	bool result;


	// Create the texture array object.
	m_TextureArray = new TextureClass;
	if(!m_TextureArray)
	{
		return false;
	}

	// Initialize the texture array object.
	result = m_TextureArray->Initialize(device);
	if(!result)
	{
		return false;
	}

	m_TextureArray->createBrush( m_paintBrush.BrushType.Size );

	return true;
}

//#################################################################################################

void ModelClass::ReleaseTextures()
{
	// Release the texture array object.
	/* EffectVariable takes responsibility over this */

	if(m_TextureArray)
	{
		m_TextureArray->Shutdown();
		delete m_TextureArray;
		m_TextureArray = 0;
	}

	return;
}

//#################################################################################################

void ModelClass::createQuadTree()
{
	m_mapDiemnsions.SizeX = m_ha.GetSizeX()* m_mapSizeFactor;
	m_mapDiemnsions.SizeY = m_ha.GetSizeY()* m_mapSizeFactor;
	m_mapDiemnsions.SizeZ = 50.f;
	m_mapDiemnsions.MinX = m_mapDiemnsions.SizeX / 2.f - m_mapDiemnsions.SizeX;
	m_mapDiemnsions.MinY = m_mapDiemnsions.SizeY / 2.f - m_mapDiemnsions.SizeY;
	m_mapDiemnsions.MinZ =  0.f;

	CDLODQuadTree::CreateDesc createDesc;
	createDesc.pHeightmap         = &m_ha;
	createDesc.LeafRenderNodeSize = 8;
	createDesc.LODLevelCount      = 5;
	createDesc.MapDims            = m_mapDiemnsions;

	// build the quad tree based on the height information
	m_quadTree.Create( createDesc );
}
