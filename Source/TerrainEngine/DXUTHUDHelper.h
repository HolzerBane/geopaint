#pragma once

#include "DXUT.h"
#include "DXUTgui.h"
#include "DXUTsettingsDlg.h"
#include "SDKmisc.h"

struct ID3D10Device;

class DXUTHUDHelper
{
public:

	static DXUTHUDHelper& Instance();

	void Init( ID3D10Device* device );
	CDXUTTextHelper& getTextHelper() { return *pTxtHelper; }
	~DXUTHUDHelper();

	ID3DX10Font *					pFont;
	ID3DX10Sprite*					pTextSprite;

	CDXUTDialogResourceManager		DialogResourceManager; // manager for shared resources of dialogs
	CD3DSettingsDlg					SettingsDlg;          // Device settings dialog
	CDXUTTextHelper*				pTxtHelper;
	CDXUTDialog						HUD;                  // manages the 3D UI
	CDXUTDialog						ShowHUD;              // manages one lonely button
	CDXUTButton *					ShowHUDButton; // lonely button

private:
	bool						m_inited;
	DXUTHUDHelper();
};

