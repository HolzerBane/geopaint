#pragma once
#include <math.h>

namespace MathUtils
{
	struct Point2D
	{
		float x,y;
	};

	static float distanceSqr( const Point2D& a, const Point2D& b )
	{
		return std::powf(a.x-b.x, 2) + std::powf(a.y-b.y, 2); 
	}
}