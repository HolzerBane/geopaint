#pragma once

#include <windows.h>
#include <string>
#include <D3Dcommon.h>
#include <fstream>
#include <sstream>
#include <d3d10misc.h>

namespace File
{
	template< class TString >
	class FileInfo
	{
	public:
		TString Extension;
		TString Path;
		TString FullPath;
		TString DriveLetter;

		FileInfo( const TString& fullPath )
		{
		
		}
	};

	static bool fileExists( const std::wstring& filePath )
	{
		return _wfopen( filePath.c_str(), L"r") != nullptr;
	}

	static bool fileExists( const std::string& filePath )
	{
		return fopen( filePath.c_str(), "r") != nullptr;
	}

	static std::wstring getExtension( const std::wstring& fullPath )
	{
		size_t dotPos = fullPath.rfind('.');
		if ( dotPos != std::wstring::npos )
		{
			return fullPath.substr(dotPos, fullPath.size() - dotPos - 1 );
		}

		return L"";
	}

	static std::wstring getFileName( const std::wstring& fullPath )
	{
		size_t bslashPos = fullPath.rfind('\\');
		if ( bslashPos != std::wstring::npos )
		{
			return fullPath.substr(bslashPos, fullPath.size() - bslashPos - 1 );
		}

		return L"";
	}

	static std::wstring getStrippedFileName( const std::wstring& fullPath )
	{
		size_t bslashPos = fullPath.rfind('\\');
		size_t dotPos = fullPath.rfind('.');
		if ( bslashPos != std::wstring::npos && dotPos != std::wstring::npos )
		{
			return fullPath.substr(bslashPos,dotPos - bslashPos - 1 );
		}

		return L"";
	}

	static std::wstring addPrefix(const std::wstring& fullPath, const std::wstring& prefix )
	{
		std::wstring outStr = fullPath;
		
		size_t dotPos = fullPath.rfind('.');
		if ( dotPos != std::wstring::npos )
		{
			outStr.insert( dotPos, prefix );
		}
		else
		{
			outStr += prefix;
		}

		return outStr;
	}

	static bool readBLOBFromFile( const std::wstring& path, ID3D10Blob** blob )
	{
		std::ifstream file( path, std::ios::in | std::ios::binary | std::ios::ate);

		if(file.is_open())
		{
			std::ifstream::pos_type fileSize;
			fileSize = file.tellg();
			D3D10CreateBlob( (size_t)fileSize, blob );
			file.seekg(0, std::ios::beg);
			if(!file.read((char*)(*blob)->GetBufferPointer(), fileSize))
			{
				return false;
			}

			return true;
		}
		
		DWORD errorNo = GetLastError();

		return false;
	}

	template <class StringType >
	static bool readFileToString( const std::wstring& filePath, StringType& outString  )
	{
		//static_assert( std::is_same< StringType::value_type, char > || std::is_same< StringType::value_type, wchar_t >, 
		//	"only std::string and std::wstring is supported!" );

		std::ifstream file( filePath, std::ios::in );
		if(file.is_open())
		{
			std::basic_stringstream<
				StringType::value_type, 
				std::char_traits<StringType::value_type>,	
				std::allocator<StringType::value_type> > ss;

			ss << file.rdbuf();
			outString = ss.str();
			file.close();

			return true;
		}
		else
		{
			file.close();
			return false;
		}
	}
}