#pragma once
#include <string>
#include <D3D10_1.h>
#include <assert.h>
/*might be needless*/

namespace Utils
{

/**************************************************************************************************/
/* Setter interface for Effect Variables */
struct RawData
{
	RawData()
		: data(nullptr)
		, offset(0)
		, size(0)
	{ }

	RawData( void* data_, unsigned offset_, unsigned size_)
		: data(data_)
		, offset(offset_)
		, size(size_)
	{ }

	operator bool( )
	{
		return data != nullptr; 
	}

	void* data;
	unsigned offset;
	unsigned size;
};
/***************************************************************************************************/

struct ResourceArray
{
	ResourceArray( )
		: data(nullptr)
		, offset(0)
		, count(0)
	{ }

	ResourceArray( ID3D10ShaderResourceView** d, unsigned o, unsigned c)
		: data(d)
		, offset(o)
		, count(c)
	{ }

	ID3D10ShaderResourceView** data;
	unsigned offset;
	unsigned count;
};
/***************************************************************************************************/

class IEffectVariable
{
public:
	virtual void set() = 0;
	virtual void free() = 0;
	virtual ~IEffectVariable(){}
};
/***************************************************************************************************/

/* Unspecified setter */
template < class TVar, class TResource >
struct ShaderResourceSetter
{
	HRESULT set( TVar var, TResource res )
	{
		static_assert( false, "Unspecified EffectVar setter is not uspported" );
		return S_FAIL;
	}
};

/* Setter for Matrix variables */
template <  class TResource >
struct ShaderResourceSetter< ID3D10EffectMatrixVariable*, TResource >
{
	HRESULT set( ID3D10EffectMatrixVariable* var, TResource res )
	{		
		return var->SetMatrix( (float*)res );
	}
};

/* Setter for Shader Resource variables */
template <  class TResource >
struct ShaderResourceSetter< ID3D10EffectShaderResourceVariable*, TResource >
{
	HRESULT set( ID3D10EffectShaderResourceVariable* var, TResource res )
	{
		return var->SetResource( res );
	}
};

/* Setter for raw data*/
template <  >
struct ShaderResourceSetter< ID3D10EffectVariable*, RawData >
{
	HRESULT set( ID3D10EffectVariable* var, RawData& res )
	{
		return var->SetRawValue( res.data, res.offset, res.size );
	}
};

/* Setter for resource array*/
template <  >
struct ShaderResourceSetter< ID3D10EffectShaderResourceVariable, ResourceArray >
{
	HRESULT set( ID3D10EffectVariable* var, ResourceArray& res )
	{
		return var->SetRawValue( res.data, res.offset, res.count );
	}
};

/***************************************************************************************************/

template< class TVar >
struct Binder
{
	void bind( ID3D10EffectMatrixVariable** var, ID3D10Effect* effect, const std::string& name )
	{
	
	}
};

template<>
struct Binder< ID3D10EffectMatrixVariable* >
{
	void bind( ID3D10EffectMatrixVariable** var, ID3D10Effect* effect, const std::string& name )
	{
		*var = effect->GetVariableByName( name.c_str() )->AsMatrix();
	}
};

template<>
struct Binder< ID3D10EffectVariable* >
{
	void bind( ID3D10EffectVariable** var, ID3D10Effect* effect, const std::string& name )
	{
		*var = effect->GetVariableByName( name.c_str() );
	}
};

template<>
struct Binder< ID3D10EffectShaderResourceVariable* >
{
	void bind( ID3D10EffectShaderResourceVariable** var, ID3D10Effect* effect, const std::string& name )
	{
		*var = effect->GetVariableByName( name.c_str() )->AsShaderResource();
	}
};



/**************************************************************************************************/
	
template< class TResource >
struct Free
{
	void free( TResource res )
	{
		static_assert( false, "Unspecified TResource free is not uspported" );
	}
};

template<>
struct Free< ID3D10ShaderResourceView* >
{
	void free( ID3D10ShaderResourceView* res )
	{
		// this seems to get called automatically
		//if (res) { (res)->Release(); (res) = nullptr; }
	}
};

template<>
struct Free< float* >
{
	void free( float* res )
	{
		//res = nullptr;
	}
};

template<>
struct Free< RawData >
{
	void free( RawData& res )
	{
		//delete res.data;
	}
};

/**************************************************************************************************/
/* Effect variable binding to shader resource view of any type defined in the setters */

template< 
	class TVar		= ID3D10EffectShaderResourceVariable*, 
	class TResource = ID3D10ShaderResourceView* >
class EffectVariable : public IEffectVariable
{
public:
	EffectVariable( );
	~EffectVariable( );
	EffectVariable(  ID3D10Effect* effect, const std::string& name, const TResource res);
	/* binds an effect variable*/
	void bind( ID3D10Effect* effect, const std::string& name);
	/* attaches an effect variable to a resource */
	void attach( const TResource res);

	void set( ) override;
	void free( ) override;

private:
	std::string									m_varName;
	Binder< TVar >								m_binder;
	Free< TResource >							m_free;
	ShaderResourceSetter< TVar, TResource >		m_setter;
	TVar										m_var;
	TResource									m_resource;
};

/**************************************************************************************************/
// default implementation
template< class TVar, class TResource>
EffectVariable< TVar, TResource >::EffectVariable( )
{
	// forcing pointers as types
	m_var = nullptr;
	m_resource = nullptr;
}

template< class TVar, class TResource>
EffectVariable< TVar, TResource >::~EffectVariable( )
{
	free( );
	m_varName.clear();
}

template< class TVar, class TResource>
EffectVariable< TVar, TResource >::EffectVariable(  ID3D10Effect* effect, const std::string& name, const TResource res)
	:m_varName(name)
{
	bind( effect, name);
	assert(m_var);
	attach( res );
	assert(m_resource);
}


template< class TVar, class TResource >
void EffectVariable< TVar, TResource >::set(  )
{
	HRESULT res = m_setter.set(m_var, m_resource );
	assert( res == S_OK );
}

template< class TVar, class TResource >
void EffectVariable< TVar, TResource >::free(  )
{
	m_var		= nullptr;
	//m_resource  = nullptr;
	m_free.free( m_resource);
}

template< class TVar, class TResource >
void EffectVariable< TVar, TResource >::bind( ID3D10Effect* effect, const std::string& name )
{	
	/* needs pointer to interface pointer */
	m_binder.bind( &m_var, effect, name);
}

template< class TVar, class TResource >
void EffectVariable< TVar, TResource >::attach( TResource res )
{
	m_resource = res;
}


}