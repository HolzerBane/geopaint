//////////////////////////////////////////////////////////////////////
// Copyright (C) 2009 - Filip Strugar.
// Distributed under the zlib License (see readme.txt)
//////////////////////////////////////////////////////////////////////

#pragma once

//#define MY_EXTENDED_STUFF

//#include "DXUT.h"

#pragma warning ( disable: 4996 )
#pragma warning ( disable: 4995 )

#include <string>
#include <vector>
//#include "TiledBitmap/TiledBitmap.h"

struct D3DXVECTOR3;
struct D3DXPLANE;
struct D3DXMATRIX;

//////////////////////////////////////////////////////////////////////////
// Project specific settings


enum KeyCommands
{
   kcForward	= 0,
   kcBackward,
   kcLeft,
   kcRight,
   kcUp,
   kcDown,
   kcShiftKey,
   kcControlKey,
   kcViewRangeUp,
   kcViewRangeDown,
   kcToggleHelpText,
   
   kcToggleWireframe,
   kcToggleDetailmap,
   kcToggleShadowmap,
   kcToggleDebugView,

   kcToggleMovieRec,
   kcSaveViewProjOverride,
   kcOrthoViewProjOverride,
   kcToggleViewProjOverride,

   kcReloadAll,
   kcReloadShadersOnly,

   kcSaveState,
   kcLoadState,

   kcPause,
   kcOneFrameStep,

   kcLASTVALUE
};

enum ShadowMapSupport
{
   smsNotSupported      = 0,
   smsNVidiaShadows,
   smsATIShadows,
};

struct MapDimensions
{
	float                MinX;
	float                MinY;
	float                MinZ;
	float                SizeX;
	float                SizeY;
	float                SizeZ;

	float                MaxX() const   { return MinX + SizeX; }
	float                MaxY() const   { return MinY + SizeY; }
	float                MaxZ() const   { return MinZ + SizeZ; }
};


const MapDimensions &   vaGetMapDimensions();

template<class T>
inline T clamp(T const & v, T const & b, T const & c)
{
	if( v < b ) return b;
	if( v > c ) return c;
	return v;
}

// short for clamp( a, 0, 1 )
template<class T>
inline T saturate( T const & a )
{
	return ::clamp( a, (T)0.0, (T)1.0 );
}

template<class T>
inline T lerp(T const & a, T const & b, T const & f)
{
	return a + (b-a)*f;
}

template<class T>
inline void swap(T & a, T & b)
{
	T temp = b;
	b = a;
	a = temp;
}

template<class T>
inline T sqr(T & a)
{
	return a * a;
}

inline float randf( )       { return rand() / (float)RAND_MAX; }

// Get time independent lerp function. Play with different lerpRates - the bigger the rate, the faster the lerp!
inline float timeIndependentLerpF(float deltaTime, float lerpRate)
{
	return 1.0f - expf( -fabsf(deltaTime*lerpRate) );
}

inline double frac( double a )
{
	return fmod( a, 1.0 );
}

inline float frac( float a )
{
	return fmodf( a, 1.0 );
}

inline int floorLog2(unsigned int n) 
{
   int pos = 0;
   if (n >= 1<<16) { n >>= 16; pos += 16; }
   if (n >= 1<< 8) { n >>=  8; pos +=  8; }
   if (n >= 1<< 4) { n >>=  4; pos +=  4; }
   if (n >= 1<< 2) { n >>=  2; pos +=  2; }
   if (n >= 1<< 1) {           pos +=  1; }
   return ((n == 0) ? (-1) : pos);
}

std::string vaStringSimpleNarrow( const std::wstring & s );

//
std::wstring vaStringSimpleWiden( const std::string & s );

// ATI Fetch4 support

// GET4 is used both as a format for exposing Fetch4 as well as the enable value
// GET1 is used only as the disable value
#define FOURCC_GET4  MAKEFOURCC('G','E','T','4')
#define FOURCC_GET1  MAKEFOURCC('G','E','T','1')

