#pragma once

#define CONSOLE_ON
#include "DebugConsole.h"

#include <windows.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <fstream>

#ifndef _USE_OLD_IOSTREAMS

using namespace std;

#endif

#ifndef _USE_OLD_IOSTREAMS
using namespace std;
#endif

int DebugConsole::counter = 1;
std::wofstream DebugConsole::logFileOut;

// maximum mumber of lines the output console should have
void DebugConsole::RedirectIOToConsole( bool i, bool o)
{
	static const WORD MAX_CONSOLE_LINES = 500;

	int hConHandle;
	long lStdHandle;
	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	FILE *fp;
	// allocate a console for this app
	AllocConsole();
	// set the screen buffer to be big enough to let us scroll text
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),
		&coninfo);
	coninfo.dwSize.Y = MAX_CONSOLE_LINES;
	coninfo.srWindow.Top = 0;
	coninfo.srWindow.Left = 0;
	coninfo.srWindow.Right = 41;
	coninfo.srWindow.Bottom = 51;
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE),
		coninfo.dwSize);
	BOOL wtf = SetConsoleWindowInfo( GetStdHandle(STD_OUTPUT_HANDLE), true, &coninfo.srWindow );
	if ( o )
	{
		// redirect unbuffered STDOUT to the console
		lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
		hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
		fp = _fdopen( hConHandle, "w" );
		*stdout = *fp;
		setvbuf( stdout, NULL, _IONBF, 0 );
	}
	if ( i )
	{
		// redirect unbuffered STDIN to the console
		lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
		hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
		fp = _fdopen( hConHandle, "r" );
		*stdin = *fp;
		setvbuf( stdin, NULL, _IONBF, 0 );
	}
	// redirect unbuffered STDERR to the console
	lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen( hConHandle, "w" );
	*stderr = *fp;
	setvbuf( stderr, NULL, _IONBF, 0 );
	// make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog
	// point to console as well
	ios::sync_with_stdio();

	// send data to a file as well
	logFileOut.open(L"console_log.txt", ios::out );
}
//End of File