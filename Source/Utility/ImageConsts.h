#pragma once

namespace ImageConsts
{
	enum Channel
	{
		Red,
		Green,
		Blue,
		Alpha
	};

	enum Pitch
	{
		RGB = 3,
		RGBA = 4
	};
}