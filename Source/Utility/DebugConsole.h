#pragma once

//#############################################################################
//#############################################################################

#ifdef CONSOLE_ON

#include <ctime>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include <locale>

class DebugConsole
{
public:

	enum LogMode
	{
		Console			= 1,
		File			= 2,
		ConsoleAndFile	= 3
	};

	static int counter;
	static std::wofstream logFileOut;

	DebugConsole()
	{ }

	~DebugConsole()
	{ 
		logFileOut.flush();
		logFileOut.close();
	}

	static void RedirectIOToConsole( bool i, bool o);

	template< class ChType >
	static void trace( const ChType* text, const LogMode logMode = ConsoleAndFile )
	{
		if ( logMode & Console) 
			logFormat( std::wcout, text );

		if ( logMode & File )
			logFormat( logFileOut, text );	

		counter++;
	}

	template< class ChType >
	static void traceHex( const ChType* text, const long resultCode,  const LogMode logMode = ConsoleAndFile )
	{
		std::basic_stringstream<
				ChType, 
				std::char_traits<ChType>,	
				std::allocator<ChType> > ss;

		ss << text << std::hex << resultCode;

		trace( ss.str().c_str(), logMode);
	}

	template< class ChType >
	static void trace( const ChType* text, const int intValue,  const LogMode logMode = ConsoleAndFile )
	{
		std::basic_stringstream<
				ChType, 
				std::char_traits<ChType>,	
				std::allocator<ChType> > ss;
		ss << text << intValue;

		trace( ss.str().c_str(), logMode);
	}

	template< class ChType >
	static void trace( const ChType* text, const unsigned uintValue,  const LogMode logMode = ConsoleAndFile )
	{
		std::basic_stringstream<
				ChType, 
				std::char_traits<ChType>,	
				std::allocator<ChType> > ss;
		ss << text << uintValue;

		trace( ss.str().c_str(), logMode);
	}

	template< class ChType >
	static void trace( const ChType* text, const float& floatValue,  const LogMode logMode = ConsoleAndFile )
	{
		std::basic_stringstream<
				ChType, 
				std::char_traits<ChType>,	
				std::allocator<ChType> > ss;
		ss << text << std::setprecision(5) << floatValue;

		trace( ss.str().c_str(), logMode);
	}

	struct scoped_trace								
	{	
		typedef std::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > StringType; 
		StringType text; 
		clock_t startTime;
		LogMode logMode;

		scoped_trace(const wchar_t * t, const LogMode lm = ConsoleAndFile )							
		{					
			logMode = lm;
			text = t;
			startTime = clock();
			trace( (text + L" started...").c_str(), logMode );	
		}	

		~scoped_trace()							
		{								
			std::basic_stringstream<
				StringType::value_type, 
				std::char_traits<StringType::value_type>,	
				std::allocator<StringType::value_type> > ss;
			ss << text << " finished in " << ( clock() - startTime ) / 1000.f << " secs";
			trace( ss.str().c_str(), logMode );	
		}											
	};		

private:

	static void logFormat( std::wostream& output, const wchar_t* entry )
	{
		output << counter << L"\t: " << entry << std::endl;	
	}

	static void logFormat( std::wostream& output, const char* entry )
	{
		std::wstring_convert<std::codecvt<wchar_t, char, mbstate_t> > converter;
		output << counter << L"\t: " << converter.from_bytes(entry) << std::endl;	
	}
};

	#define CONSOLE_TRACE(text)	\
		std::cout << counter++ << "\t: " << text << std::endl;

	#define CONSOLE_COUNT(function) \
		{		\
			clock_t time = clock();	\
			function; \
			CONSOLE_TRACE( #function << " time: \t" << clock() - time ) \
	    }

	#define CONSOLE_SCOPED_TRACE(text)					\
		struct __scoped_trace							\
		{												\
			__scoped_trace()							\
			{											\
				CONSOLE_TRACE(text << " started...");	\
			}											\
														\
			~__scoped_trace()							\
			{											\
				CONSOLE_TRACE(text << " finished!");	\
			}											\
		};												\
		__scoped_trace __s_t;

#else
	#define CONSOLE_TRACE(text)
	#define CONSOLE_COUNT(function) function; 

namespace std
{
	class string;
}

struct DebugConsole
{
	enum LogMode
	{
		Console			= 1,
		File			= 2,
		ConsoleAndFile	= 3
	};

	DebugConsole()
	{ }

	~DebugConsole()
	{ 

	}

	static void RedirectIOToConsole( bool i, bool o);

	static void trace( const std::string& text, const LogMode logMode = ConsoleAndFile )
	{

	}


	struct scoped_trace								
	{	
		scoped_trace(const wchar_t* param, const LogMode lm = ConsoleAndFile )							
		{					

		}											

		~scoped_trace()							
		{								

		}											
	};				
};

#endif

//#############################################################################
//#############################################################################
