#include "DeltaTime.h"

#include <windows.h>
#include <map>
#include <queue>

ULONGLONG resolution = 60, lastTime = 0, frequency = 1;
double lowId = 1;
std::queue<double> usedIds;
std::map<double, ULONGLONG> markers;


double FPSTimer::getId() {
	double id;
	if (usedIds.empty()) {
		return lowId++;
	}
	else {
		id = usedIds.front();
		usedIds.pop();
		return id;
	}
}


double FPSTimer::Initialize() {
	if (QueryPerformanceFrequency((LARGE_INTEGER *)&frequency) && QueryPerformanceCounter((LARGE_INTEGER*)&lastTime)) {
		return 0.0;
	}
	else {
		return -1.0;
	}
}

 double FPSTimer::SetResolution(double res) {
	if (res <= 0 || res > frequency) {
		return -1.0;
	}
	else {
		resolution = res;
		return 0.0;
	}
}

 double FPSTimer::GetResolution() {
	return (double)resolution;
}

 double FPSTimer::GetFrequency() {
	return (double)frequency;
}

 double FPSTimer::AddMarker() {
	double id;
	ULONGLONG now;
	if (QueryPerformanceCounter((LARGE_INTEGER*)&now)) {
		id = getId();
		markers[id] = now;
		return id;
	}
	else {
		return -1.0;
	}
}

 double FPSTimer::GetMarker(double id) {
	if (markers.find(id) != markers.end()) {
		return (double)(resolution*markers[id]/frequency);
	}
	else {
		return -1.0;
	}
}

 double FPSTimer::SetMarker(double id, double time, double rel) {
	ULONGLONG now, t;
	bool neg = (time < 0);
	if (neg) { time = -time; }
	if (markers.find(id) != markers.end() && QueryPerformanceCounter((LARGE_INTEGER*)&now)) {
		if (time != 0) {
			t = (ULONGLONG)(time*frequency/resolution);
			if (rel > 0) {
				t = now + (neg ? -t : t);
			}
		}
		else {
			t = now;
		}

		markers[id] = t;
		return id;
	}
	else {
		return -1.0;
	}
}

 double FPSTimer::TimeMarker(double id) {
	ULONGLONG now;
	if (markers.find(id) != markers.end() && QueryPerformanceCounter((LARGE_INTEGER*)&now)) {
		return (double)((now - markers[id])*resolution/frequency);
	}
	else {
		return -1.0;
	}
}

 double FPSTimer::DeleteMarker(double id) {
	std::map<double, ULONGLONG>::iterator mark = markers.find(id);
	if (mark != markers.end()) {
		markers.erase(mark);
		usedIds.push(id);
		return 0.0;
	}
	else {
		return -1.0;
	}
}

 double FPSTimer::TimeNow() {
	ULONGLONG now;
	if (QueryPerformanceCounter((LARGE_INTEGER*)&now)) {
		return (double)(now*resolution/frequency);
	}
	else {
		return -1.0;
	}
}

 double FPSTimer::DeltaTime() {
	ULONGLONG now, lt;
	if (QueryPerformanceCounter((LARGE_INTEGER*)&now)) {
		lt = lastTime;
		lastTime = now;
		return (double)((now - lt)*resolution/frequency);
	}
	else {
		return -1.0;
	}
}

