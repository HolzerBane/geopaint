#pragma once 

#include <windows.h>
#include <stdio.h>

class StopWatch
{
	LARGE_INTEGER freq, startTime, endTime, thisTime, lastTime ;
	double fFreq ;

public:
	double total_time ; 

	StopWatch()
	{
		QueryPerformanceFrequency( &freq ) ;
		fFreq = (double)freq.QuadPart ;
		total_time = 0 ;

		printf( "The ffreq is %lf\n", fFreq ) ;
	}

	void start()
	{
		QueryPerformanceCounter( &startTime ) ;
		thisTime = lastTime = startTime ;
		total_time = 0.0 ;  // start counter at 0 seconds
	}

	double stop()
	{
		QueryPerformanceCounter( &endTime ) ;
		total_time = ( endTime.QuadPart - startTime.QuadPart ) / fFreq ;
		return total_time ;
	}

	void update()
	{
		lastTime = thisTime ;
		QueryPerformanceCounter( &thisTime ) ;
		total_time += ( thisTime.QuadPart - lastTime.QuadPart ) / fFreq ;
	}
} ;