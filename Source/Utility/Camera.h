#pragma once

struct D3DXVECTOR3;
struct D3DXMATRIX;

/// Basic camera interface, to be implemented by camera type classes.
class Camera
{
public:
	virtual const D3DXVECTOR3& getEyePosition() const =0;
	virtual const D3DXVECTOR3& getLookAtPosition() const =0;
	/// Returns the inverse of the view-projection matrix (without eye pos translation) to be used in shaders.
	virtual const D3DXMATRIX& getRotProjMatrixInverse() const =0;
	/// Returns view matrix to be used in shaders.
	virtual const D3DXMATRIX& getViewMatrix() const =0;
	/// Returns projection matrix to be used in shaders.
	virtual const D3DXMATRIX& getProjMatrix() const =0;

	/// Manipulates camera. To be implemented if the camera is directly controlled by the user.
	virtual void handleInput(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){}
	/// Moves camera. To be implemented if the camera has its own animation mechanism.
	virtual void animate(double dt){}
};
