#pragma once

struct ID3D10Device;
struct ID3D10InfoQueue;

class ReportError
{
public:

	static ReportError& Instance();

	void setInfoQueue( ID3D10Device* );
	void CheckError( );
	void ClearMessages( );

private:
	ReportError(  );
	ID3D10InfoQueue* m_infoQueue;
};