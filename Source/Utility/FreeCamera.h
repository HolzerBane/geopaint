#pragma once
#include "DXUT.h"
#include "Camera.h"

class CFirstPersonCamera;
struct D3DXPLANE;

/// Class for non-entity bound cameras. (Wraps a DXUT CFirstPersonCamera.)
class FreeCamera :
	public Camera
{
protected:
	/// DXUT camera instance.
	CFirstPersonCamera* camera;
public:
	/// Constructor. Initializes DXUT camera. (non-const as DXUT is not const..)
	FreeCamera( D3DXVECTOR3& eye,  D3DXVECTOR3& lookAt);
	~FreeCamera( );

	const D3DXVECTOR3&	getEyePosition() const override;
	const D3DXVECTOR3&	getLookAtPosition() const override;
	const float			getViewDistance() const;
	/// Returns the inverse of the view-projection matrix (without eye pos translation).
	const D3DXMATRIX& getRotProjMatrixInverse() const override;
	/// Returns view matrix.
	const D3DXMATRIX& getViewMatrix() const override;
	/// Returns projection matrix.
	const D3DXMATRIX& getProjMatrix() const override;
	void getUpOffsettedProjMatrix( D3DXMATRIX & mat, float zModMul, float zModAdd ) const;

	/// Returns the frustum planes
	void GetFrustumPlanes( D3DXPLANE pPlanes[6] ) const;

	/// Passes event to DXUT camera.
	void handleInput(HWND hWnd,  UINT uMsg, WPARAM wParam, LPARAM lParam);
	/// Animates DXUT camera.
	void animate(double dt);
private:

	void buildFrustumPlanes( D3DXPLANE planes[6], const D3DXMATRIX & mCameraViewProj ) const;
};
