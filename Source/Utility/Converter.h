#pragma once

namespace Converter
{
	static unsigned char floatToByte( const float input, const float scale = 1.0f )
	{
		return (unsigned char)((input / scale) * 255.f);
	}

	static unsigned char*  vector4ToByte4( const float* input, const float scale = 1.0f )
	{
		unsigned char* result =  new unsigned char[4];
		for( int i = 0; i < 4; ++i )
			result[i] = floatToByte( input[i], scale );

		return result;
	}

}