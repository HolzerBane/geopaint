//********************************************************************
//  BMP, TARGA, PCX form�tum� k�pf�jlok beolvas�sa
//
// Szirmay-Kalos L�szl� (szirmay@iit.bme.hu)
// BME, Iranyit�stechnika �s Informatika Tansz�k
// 2003. M�jus
//
// Juh�sz P�ter, 2011. November
//*********************************************************************

#include <stdio.h>
#include <windows.h>
#include <string>
#include "ImageConsts.h"

typedef unsigned char Byte;

//=============================================================
class ImageFile {        // k�pf�jl
//=============================================================
    FILE * file;              // f�jl le�r�
	Byte    * image;          // RGB pixelek
	Byte	* imageWithAlpha; // RGBA pixelek
	unsigned  size;           // m�ret byte-ban
	unsigned  m_height;
	unsigned  m_width;
	unsigned  m_pitch;

	BITMAPFILEHEADER BMPFileHeader;
	BITMAPINFOHEADER BMPInfoHeader;

	void	readBMP(FILE * file, unsigned& width, unsigned& height);
	void	readTGA(FILE * file, unsigned& width, unsigned& height);
	void	readPCX(FILE * file, unsigned& width, unsigned& height);

	// ReadBuffer takes ownership of data, does not copy it

public :

    ImageFile( const std::wstring& inputfilename, unsigned& width, unsigned& height );
	ImageFile( )
		: file(nullptr)
		, image(nullptr)
		, imageWithAlpha(nullptr)
	{ };

	void	readBuffer( Byte* data, const unsigned pitch, const unsigned width, const unsigned height );
	void	saveBMP( const std::string& fileName );

	Byte * getBuffer( ) { return image; }
	Byte * loadWithAlpha( );

	unsigned getWidth() const { return m_width; }
	unsigned getHeight() const { return m_height; }
	unsigned getNumOfChannels() const { return m_pitch; }

	Byte getChannel( const ImageConsts::Channel channel, unsigned i ) const { return image[i * 3 + channel]; }

	Byte red( unsigned i ) { return image[i * 3]; }
	Byte green( unsigned i ) { return image[i * 3+1]; }
	Byte blue( unsigned i ) { return image[i * 3+2]; }
	Byte alpha( unsigned i ) { return image[i * 3+3]; }

	~ImageFile( ) 
	{ 
		if (file) fclose(file); 
		if ( image ) delete[] image;
		if ( imageWithAlpha ) delete[] imageWithAlpha;
	}
};
