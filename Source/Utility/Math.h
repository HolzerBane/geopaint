#pragma once
#include <D3DX10math.h>
#undef min

namespace Math
{
	static float epsilon = 0.0001f;

	float static length( const D3DXVECTOR3& vec )
	{
		float l2 = powf( vec.x, 2) + powf( vec.y, 2) + powf( vec.z, 2);
		if (l2 == 0.0) return 0.0f;

		return sqrtf( l2 );
	}


	D3DXVECTOR3 static normalize( const D3DXVECTOR3 vec )
	{
		float l = length(vec);
		if (l == 0.0f) return D3DXVECTOR3(0.f, 0.f, 0.f);
		return vec / l;
	}

	inline float static mulVecVecDot( float a, float b, float c, const D3DXVECTOR3& v)
	{
		return a*v.x + b*v.y + c*v.z;
	}

	D3DXVECTOR3 static mulMatVec( const D3DXMATRIX& m, const D3DXVECTOR3& v)
	{

		return	D3DXVECTOR3(mulVecVecDot(m(0,0), m(0,1), m(0,2), v),
			mulVecVecDot(m(1,0), m(1,1), m(1,2), v),
			mulVecVecDot(m(2,0), m(2,1), m(2,2), v)
			);
	}

	D3DXMATRIX static createMatrix( const D3DXVECTOR3 i, const D3DXVECTOR3 j, const D3DXVECTOR3 k )
	{
		return D3DXMATRIX(
			i.x, i.y, i.z, 0,
			j.x, j.y, j.z, 0,
			k.x, k.y, k.z, 0,
			0,   0,   0,   0
		);
	}

	template <class TValue>
	TValue static clamp( const TValue& minVal, const TValue& maxVal, const TValue& val )
	{
		return std::min( maxVal, std::max( minVal, val ) );
	}	

	D3DXVECTOR4 static clamp( const D3DXVECTOR4& minVal, const D3DXVECTOR4& maxVal, const D3DXVECTOR4& val )
	{
		D3DXVECTOR4 res;
		for( unsigned i = 0; i < 4; ++i)
		{
			res[i] = std::min( maxVal[i], std::max( minVal[i], val[i] ) );
		}

		return res;
	}	

	D3DXVECTOR3 static clamp( const D3DXVECTOR3& minVal, const D3DXVECTOR3& maxVal, const D3DXVECTOR3& val )
	{
		D3DXVECTOR3 res;
		for( unsigned i = 0; i < 3; ++i)
		{
			res[i] = std::min( maxVal[i], std::max( minVal[i], val[i] ) );
		}

		return res;
	}	

	float static rangedScale1D( const float minSrc, const float maxSrc,  const float minDst,  const float maxDst, const float value, const float offset = 0.f )
	{
		assert( value > minSrc && value < maxSrc );
		return value * ( maxDst - minDst ) / ( maxSrc - minSrc ) + offset;
	}

	float static rangedScale1D( const float srcSize, const float dstSize, const float value, const float offset = 0.f )
	{
		return value * ( dstSize ) / ( srcSize ) + offset;
	}

	bool static feq( const float a, const float b )
	{
		return a <= b + epsilon && a >= b - epsilon;
	}
}