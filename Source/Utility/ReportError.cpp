#include "ReportError.h"
#include <d3d10_1.h>
#include <assert.h>

ReportError::ReportError(  )
{
	m_infoQueue = nullptr;
}

ReportError& ReportError::Instance()
{
	static ReportError instance;
	return instance;
}

void ReportError::setInfoQueue( ID3D10Device* device )
{
	HRESULT res = device->QueryInterface(__uuidof(ID3D10InfoQueue),  (void **)&m_infoQueue);
	assert( res == S_OK );
}

void ReportError::CheckError( )
{
	if ( m_infoQueue == nullptr) return;

	// Get the size of the message
	SIZE_T messageLength = 0;
	HRESULT hr = m_infoQueue->GetMessage(0, nullptr, &messageLength);
	int ret = -1;
	if ( messageLength > 0)
	{
		// Allocate space and get the message
		D3D10_MESSAGE * pMessage = (D3D10_MESSAGE*)malloc(messageLength);
		hr = m_infoQueue->GetMessage(0, pMessage, &messageLength);
		m_infoQueue->ClearStoredMessages();
		ret =  MessageBoxA( nullptr, pMessage->pDescription, "Error message from device", MB_ABORTRETRYIGNORE | MB_ICONERROR );
		
		switch(ret)
		{
		case IDRETRY : 
				__asm int 3;
			break;
		}
	}
}

void ReportError::ClearMessages( )
{
	if ( m_infoQueue == nullptr) return;

	m_infoQueue->ClearStoredMessages();
}



