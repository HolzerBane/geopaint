#include "Freecamera.h"
#include "DXUTcamera.h"


FreeCamera::~FreeCamera( )
{
	if ( camera )
	{
		delete camera;
	}
}


const D3DXVECTOR3& FreeCamera::getLookAtPosition() const
{
	return *camera->GetLookAtPt();
}

const D3DXVECTOR3& FreeCamera::getEyePosition() const
{
	return *camera->GetEyePt();
}

const float	FreeCamera::getViewDistance() const
{
	return camera->GetFarClip(); 
}


const D3DXMATRIX& FreeCamera::getRotProjMatrixInverse() const
{
	static D3DXMATRIX vpm;
	const D3DXVECTOR3& eyePosition = getEyePosition();
	D3DXMATRIX eyePosTranslationMatrix;
	D3DXMatrixTranslation(&eyePosTranslationMatrix, eyePosition.x, eyePosition.y, eyePosition.z);
	#pragma warning( disable : 4238 ) // 'nonstandard extension used : class rvalue used as lvalue
	D3DXMatrixInverse(&vpm, NULL, &(eyePosTranslationMatrix * *camera->GetViewMatrix() * *camera->GetProjMatrix()));
	#pragma warning( default: 4238 )
	return vpm;
}

const D3DXMATRIX& FreeCamera::getViewMatrix() const
{
	return *camera->GetViewMatrix();
}

const D3DXMATRIX& FreeCamera::getProjMatrix() const
{
	return *camera->GetProjMatrix();
}

void FreeCamera::getUpOffsettedProjMatrix( D3DXMATRIX & mat, float zModMul, float zModAdd ) const
{
}

void FreeCamera::handleInput(HWND hWnd,  UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	camera->HandleMessages(hWnd, uMsg, wParam, lParam);
}

void FreeCamera::animate(double dt)
{
	dt /= 100.;
	camera->FrameMove(dt);
}

FreeCamera::FreeCamera( D3DXVECTOR3& eye,  D3DXVECTOR3& lookAt)
{
	camera = new CFirstPersonCamera();
	D3DXVECTOR3 up( 0.0f, 1.0f, 0.0f);
	camera->SetViewParams(&eye, &lookAt, &up);
	camera->SetScalers(0.01f, 200.0f);
	camera->SetProjParams( D3DX_PI / 4, 800.f / 600.f , 1.0f, 1000.0f );
	// rotate olnly with the right mouse button down
	camera->SetRotateButtons( false, false, true, false );
}

void FreeCamera::GetFrustumPlanes( D3DXPLANE pPlanes[6] ) const
{
	
	D3DXMATRIX cameraViewProj;
	D3DXMatrixMultiply( &cameraViewProj, camera->GetViewMatrix(), camera->GetProjMatrix() );

   buildFrustumPlanes(pPlanes, cameraViewProj);
}

void FreeCamera::buildFrustumPlanes( D3DXPLANE pPlanes[6], const D3DXMATRIX & mCameraViewProj ) const
{
   // Left clipping plane
   pPlanes[0].a = mCameraViewProj(0,3) + mCameraViewProj(0,0);
   pPlanes[0].b = mCameraViewProj(1,3) + mCameraViewProj(1,0);
   pPlanes[0].c = mCameraViewProj(2,3) + mCameraViewProj(2,0);
   pPlanes[0].d = mCameraViewProj(3,3) + mCameraViewProj(3,0);

   // Right clipping plane
   pPlanes[1].a = mCameraViewProj(0,3) - mCameraViewProj(0,0);
   pPlanes[1].b = mCameraViewProj(1,3) - mCameraViewProj(1,0);
   pPlanes[1].c = mCameraViewProj(2,3) - mCameraViewProj(2,0);
   pPlanes[1].d = mCameraViewProj(3,3) - mCameraViewProj(3,0);

   // Top clipping plane
   pPlanes[2].a = mCameraViewProj(0,3) - mCameraViewProj(0,1);
   pPlanes[2].b = mCameraViewProj(1,3) - mCameraViewProj(1,1);
   pPlanes[2].c = mCameraViewProj(2,3) - mCameraViewProj(2,1);
   pPlanes[2].d = mCameraViewProj(3,3) - mCameraViewProj(3,1);

   // Bottom clipping plane
   pPlanes[3].a = mCameraViewProj(0,3) + mCameraViewProj(0,1);
   pPlanes[3].b = mCameraViewProj(1,3) + mCameraViewProj(1,1);
   pPlanes[3].c = mCameraViewProj(2,3) + mCameraViewProj(2,1);
   pPlanes[3].d = mCameraViewProj(3,3) + mCameraViewProj(3,1);

   // Near clipping plane
   pPlanes[4].a = mCameraViewProj(0,2);
   pPlanes[4].b = mCameraViewProj(1,2);
   pPlanes[4].c = mCameraViewProj(2,2);
   pPlanes[4].d = mCameraViewProj(3,2);

   // Far clipping plane
   pPlanes[5].a = mCameraViewProj(0,3) - mCameraViewProj(0,2);
   pPlanes[5].b = mCameraViewProj(1,3) - mCameraViewProj(1,2);
   pPlanes[5].c = mCameraViewProj(2,3) - mCameraViewProj(2,2);
   pPlanes[5].d = mCameraViewProj(3,3) - mCameraViewProj(3,2);
   // Normalize the plane equations, if requested

   for (int i = 0; i < 6; i++) 
      D3DXPlaneNormalize( &pPlanes[i], &pPlanes[i] );
}