//********************************************************************
//  BMP, TARGA, PCX form�tum� k�pf�jlok beolvas�sa
//
// Szirmay-Kalos L�szl� (szirmay@iit.bme.hu).  2003. M�jus
//********************************************************************
#include <iostream>
#include <fstream>
#include "ImageFile.h"
#include <assert.h>

#define CONSOLE_ON
#include "DebugConsole.h"

//-----------------------------------------------------------------------
ImageFile::ImageFile( const std::wstring& inputfilename, unsigned& width, unsigned& height ) {
	//-----------------------------------------------------------------------
	file = NULL;
	image = imageWithAlpha = NULL;
	width = height = 0;

	if ( inputfilename.size() == 0 ) return;

	file = _wfopen(inputfilename.c_str(), L"rb");
	if ( !file )
	{	
		DebugConsole::trace( (std::wstring(L"Error opening file: ") + inputfilename).c_str() );
		return;
	}
	
	std::wstring ext = inputfilename.substr(inputfilename.find_last_of(L".") + 1);

	if ( ext.compare(L"bmp") == 0 || (ext.compare(L"BMP") == 0) )
		readBMP(file, width, height);
	if ( ext.compare(L"tga") == 0 || (ext.compare(L"TGA") == 0) )
		readTGA(file, width, height);
	if ( ext.compare(L"pcx") == 0 || (ext.compare(L"PCX") == 0) )
		readPCX(file, width, height);		

	DebugConsole::trace( (inputfilename + L" is loaded ...").c_str() );

	m_height = height;
	m_width	 = width;
}

//-----------------------------------------------------------------------
void ImageFile::readBMP( FILE * file, unsigned& width, unsigned& height ) {
	//-----------------------------------------------------------------------
	BITMAPFILEHEADER  bitmapFileHeader;			// bitmap f�jlfej
	fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, file);
	if (bitmapFileHeader.bfType != 0x4D42 ) {   // m�gikus sz�m
		return;
	}
	// fej beolvas�sa es a m�retek be�ll�t�sa
	BITMAPINFOHEADER bitmapInfoHeader;

	fread(&bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, file);
	if (bitmapInfoHeader.biBitCount != 24) {
		DebugConsole::trace( L"only true color bmp files are supported!" );
		return;
	}

	m_pitch = bitmapInfoHeader.biBitCount / 8;
	width = bitmapInfoHeader.biWidth;
	height = bitmapInfoHeader.biHeight;
	size = bitmapInfoHeader.biSizeImage;

	m_height = height;
	m_width	 = width;

	fseek(file, bitmapFileHeader.bfOffBits, SEEK_SET);

	image = new Byte[ size ];

	// k�p beolvas�sa
	fread(image, 1, size, file);

	// R �s B indexek cser�ja, mivel a BMP-ben BGR sorrendben vannak a sz�nek
	for (unsigned imageIdx = 0; imageIdx < size; imageIdx += 3) {
		Byte tempRGB = image[imageIdx];
		image[imageIdx] = image[imageIdx + 2];
		image[imageIdx + 2] = tempRGB;
	}
}

//-----------------------------------------------------------------------
void ImageFile::readTGA( FILE * file, unsigned& width, unsigned& height ) {
	//-----------------------------------------------------------------------
	for(unsigned i = 0;i < 12; i++) fgetc(file);
	width = fgetc(file) + fgetc(file) * 256L;
	height = fgetc(file) + fgetc(file) * 256L;
	size = width * height * 3;
	fgetc(file); fgetc(file);

	image = new Byte[ size ];
	m_height = height;
	m_width	 = width;

	// k�p beolvas�sa
	fread(image, 1, size, file);

	// R �s B indexek cser�ja, mivel a BMP-ben BGR sorrendben vannak a sz�nek
	for (unsigned imageIdx = 0; imageIdx < size; imageIdx += 3) {
		Byte tempRGB = image[imageIdx];
		image[imageIdx] = image[imageIdx + 2];
		image[imageIdx + 2] = tempRGB;
	}
}

//-----------------------------------------------------------------------
void ImageFile::readPCX( FILE * file, unsigned& width, unsigned& height ) {
	//-----------------------------------------------------------------------
	// els� karakter k�telez�en 10
	if (fgetc(file) != 10) return;

	// m�sodik karakter k�telez�en 5
	if (fgetc(file) != 5) return;

	// k�vetkez� k�t karaktert �tugorjuk
	fgetc(file);
	fgetc(file);

	// minim�lis x koordin�ta
	unsigned xMin = fgetc(file);   // als� b�jt
	xMin |= fgetc(file) << 8; // fels� b�jt

	// minim�lis y koordin�ta
	unsigned yMin = fgetc(file);   // als� b�jt
	yMin |= fgetc(file) << 8; // fels� b�jt

	// maxim�lis x koordin�ta
	unsigned xMax = fgetc(file);   // als� b�jt
	xMax |= fgetc(file) << 8; // fels� b�jt

	// maxim�lis y koordin�ta
	unsigned yMax = fgetc(file);   // als� b�jt
	yMax |= fgetc(file) << 8; // fels� b�jt

	// sz�less�g �s magass�g sz�m�t�sa
	width = xMax - xMin + 1;
	height = yMax - yMin + 1;

	m_height = height;
	m_width	 = width;

	// LUT beolvas�sa
	Byte * LUT = new Byte[768];

	// a LUT az utols� 769 b�jt a PCX f�jlban
	fseek(file, -769, SEEK_END);

	// LUT ellen�rz�se az els� karakter k�telez�en 12
	if (fgetc(file) != 12) 
	{
		DebugConsole::trace<wchar_t>( L"Only indexed color is supported in PCX" );
		return;
	}

	// LUT beolva�sa
	fread(LUT, 1, 768, file);

	// mem�riaallok�ci� a pixelekhez
	size = width * height * 3;
	image = new Byte[size];

	// a pixelek a 128. b�jton kezd�dnek a PCX f�jlban
	fseek(file, 128, SEEK_SET);

	// RLE dek�dol�s
	unsigned pixel = 0;
	while( pixel < width * height ) 
	{
		unsigned c = fgetc(file);
		if (c > 0xbf) { // sz�ml�l�
			unsigned numRepeat = 0x3f & c;
			c = fgetc(file);
			for (unsigned i = 0; i < numRepeat; i++) 
			{
				image[3 * pixel]     = LUT[3 * c+0];
				image[3 * pixel + 1] = LUT[3 * c+1];
				image[3 * pixel + 2] = LUT[3 * c+2];
				pixel++;
			}
		} else { 					
			image[3 * pixel]     = LUT[3 * c+0];
			image[3 * pixel + 1] = LUT[3 * c+1];
			image[3 * pixel + 2] = LUT[3 * c+2];
			pixel++;
		}
	}
}

void ImageFile::readBuffer( Byte* data, const unsigned pitch, const unsigned width, const unsigned height )
{
	m_width = width;
	m_height = height;
	m_pitch = pitch;
	size = width * height * pitch;
	image = data;
}

void ImageFile::saveBMP( const std::string& fileName )
{
	assert( m_pitch >= 3 );
	// attempt to open the file specified
	std::ofstream fout;

	// attempt to open the file using binary access
	fout.open(fileName, std::ios::binary);

	unsigned int number_of_bytes(m_width * m_height * m_pitch);
	BYTE red(0), green(0), blue(0);

	if(fout.is_open())
	{
		BMPFileHeader.bfOffBits		= 0x36;
		BMPFileHeader.bfReserved1	= 0x0;
		BMPFileHeader.bfReserved2	= 0x0;
		BMPFileHeader.bfSize		= size + BMPFileHeader.bfOffBits;
		BMPFileHeader.bfType		= 0x4D42;

		BMPInfoHeader.biBitCount	= m_pitch * 8;
		BMPInfoHeader.biClrImportant= 0x0;
		BMPInfoHeader.biClrUsed		= 0x0;
		BMPInfoHeader.biCompression = 0x0;
		BMPInfoHeader.biHeight		= m_height;
		BMPInfoHeader.biWidth		= m_width;
		BMPInfoHeader.biPlanes		= 0x1;
		BMPInfoHeader.biSize		= sizeof(BITMAPINFOHEADER);
		BMPInfoHeader.biSizeImage	= size;
		BMPInfoHeader.biXPelsPerMeter= 0xB13;
		BMPInfoHeader.biYPelsPerMeter= 0xB13;

		// same as before, only outputting now
		fout.write((char *)(&BMPFileHeader), sizeof(BITMAPFILEHEADER));
		fout.write((char *)(&BMPInfoHeader), sizeof(BITMAPINFOHEADER));

		// read off the color data in the ass backwards MS way
		for(unsigned int index(0); index < number_of_bytes; index += m_pitch)
		{
			red = image[index];
			green = image[index + 1];
			blue = image[index + 2];

			// write alpha
			if ( m_pitch == 4)
				fout.write((const char *)(&image[index + 3]), sizeof(red));

			fout.write((const char *)(&blue), sizeof(blue));
			fout.write((const char *)(&green), sizeof(green));
			fout.write((const char *)(&red), sizeof(red));
		}
		// close the file
		fout.close();
	}
}

//-----------------------------------------------------------------------
Byte * ImageFile :: loadWithAlpha( ) {
	//-----------------------------------------------------------------------
	if ( image == NULL ) return NULL;

	Byte * imageWithAlpha = new Byte[ size * 4 / 3 ];

	for (unsigned src = 0, dst = 0; src < size; src += 3, dst += 4) {
		// pixelek �tm�sol�sa
		imageWithAlpha[dst]   = image[src];
		imageWithAlpha[dst+1] = image[src+1];
		imageWithAlpha[dst+2] = image[src+2];
		// ha a pixel fekete, akkor az alfa = 0, egy�bk�nt 255.
		if (image[src] == 0 && image[src+1] == 0 && image[src+2] == 0) 
			imageWithAlpha[dst+3] = 0;
		else	imageWithAlpha[dst+3] = 0xFF;
	}
	return imageWithAlpha;
}


