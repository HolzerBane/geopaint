#pragma once
#include <assert.h>

namespace Interpolate
{

struct Linear	{ };
struct Cosine	{ };
struct Sine		{ };

template < typename Type >
float interpolate( float x, float y, float t)
{
	assert( !"Unknown type");
	return 0.0;
}

template < Type = Cosine >
float interpolate( float x, float y, float t)
{
	float ft	=t * 3.1415927;
	float f		=(1.0-cos(ft))* 0.5;
	return		x*(1.0-f)+y*f;
}

template < Type = Linear >
float interpolate( float x, float y, float t)
{
	return		x*(1.0-t)+y*t;
}
	
}