#pragma once

#include <d3d10_1.h>
#include <assert.h>

template < class TVertexType >
class RegularGrid 
{
private:
	typedef TVertexType VertexType;

	ID3D10Buffer *m_indexBuffer, *m_vertexBuffer;
	int			m_dimension;
	int         m_indexEndTL;
	int         m_indexEndTR;
	int         m_indexEndBL;
	int         m_indexEndBR;

	unsigned	m_indexCount;
	unsigned    m_vertexCount;

public:
	RegularGrid(void);
	~RegularGrid(void);
	//
	unsigned                          setupGrid( unsigned gridDim, ID3D10Device* device );
	int                              GetDimensions() const         { return m_dimension; }

	//
	ID3D10Buffer * const		GetIndexBuffer() const        { return m_indexBuffer; }
	ID3D10Buffer * const		GetVertexBuffer() const       { return m_vertexBuffer; }

	unsigned	GetIndexCount() const { return m_indexCount; }				
	unsigned	GetVertexCount() const { return m_vertexCount; }				

	int						GetIndexEndTL() const         { return m_indexEndTL; }
	int						GetIndexEndTR() const         { return m_indexEndTR; }
	int						GetIndexEndBL() const         { return m_indexEndBL; }
	int						GetIndexEndBR() const         { return m_indexEndBR; }

};

// Implementation

template< class TVertexType >
RegularGrid<TVertexType>::RegularGrid(void)
{
	m_indexBuffer    = NULL;
	m_vertexBuffer   = NULL;
	m_dimension      = 0;
	m_indexEndTL = m_indexEndTR = m_indexEndBL = m_indexEndBR = 0;
}

template< class TVertexType >
RegularGrid<TVertexType>::~RegularGrid(void)
{
	//if( m_indexBuffer ) { m_indexBuffer->Release(); m_indexBuffer = nullptr;  }
	//if( m_vertexBuffer ) { m_vertexBuffer->Release(); m_vertexBuffer = nullptr;  }
	m_indexEndTL = m_indexEndTR = m_indexEndBL = m_indexEndBR = 0;
}

template< class TVertexType >
unsigned RegularGrid<TVertexType>::setupGrid( unsigned gridDim, ID3D10Device* device )
{
	m_dimension = gridDim;

	assert( m_indexBuffer == NULL );
	assert( m_vertexBuffer == NULL );

	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;

	int totalVertices = (gridDim+1) * (gridDim+1);
	assert( totalVertices <= 65535 );
	m_vertexCount = totalVertices;

	int vertDim = gridDim + 1;
	float gdFloat = (float)gridDim;
	{
		// Set up the description of the vertex buffer.
		vertexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
		vertexBufferDesc.ByteWidth = sizeof( VertexType) * totalVertices;
		vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;

		vertexBufferDesc.CPUAccessFlags = 0;
		vertexBufferDesc.MiscFlags = 0;
		// Make a grid of (gridDim+1) * (gridDim+1) vertices

		VertexType* vertices = new VertexType[totalVertices];
		if(!vertices)
		{
			return false;
		}

		float ratioW = 0.1f;
		float ratioH = 0.1f;

		for( int y = 0; y < vertDim; y++ )
			for( int x = 0; x < vertDim; x++ )
			{
				int index = y * vertDim + x;
				assert( (index) < totalVertices );
				vertices[index].pos	= D3DXVECTOR4( x / gdFloat,  0, y / gdFloat, 0 );
				vertices[index].tex	= D3DXVECTOR4( x/ gdFloat, y / gdFloat,	x / gdFloat, y / gdFloat );
			}

			// Give the subresource structure a pointer to the vertex data.
			vertexData.pSysMem = vertices;

			// Now finally create the vertex buffer.
			HRESULT result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
			delete[] vertices;
			if(FAILED(result))
			{
				return result;
			}
	} // end of vertex data


	int totalIndices = gridDim * gridDim * 2 * 3;
	m_indexCount = totalIndices;
	{
		// Make indices for the gridDim * gridDim triangle grid, but make it as a combination of 4 subquads so that they
		// can be rendered separately when needed!

		unsigned short * indices = new unsigned short[totalIndices];

		int index = 0;

		int halfd = (vertDim/2);
		int fulld = gridDim;

		// Top left part
		for( int y = 0; y < halfd; y++ )
		{
			for( int x = 0; x < halfd; x++ )
			{
				indices[index++] = (unsigned short)((x+1) + vertDim * y);     indices[index++] = (unsigned short)(x + vertDim * y);          indices[index++] = (unsigned short)(x + vertDim * (y+1));
				indices[index++] = (unsigned short)((x+1) + vertDim * (y+1)); indices[index++] = (unsigned short)((x+1) + vertDim * y);      indices[index++] = (unsigned short)(x + vertDim * (y+1));
				assert( index <totalIndices ); 
			}
		}
		m_indexEndTL = index;

		// Top right part
		for( int y = 0; y < halfd; y++ )
		{
			for( int x = halfd; x < fulld; x++ )
			{
				indices[index++] = (unsigned short)((x+1) + vertDim * y);     indices[index++] = (unsigned short)(x + vertDim * y);         indices[index++] = (unsigned short)(x + vertDim * (y+1));
				indices[index++] = (unsigned short)((x+1) + vertDim * (y+1)); indices[index++] = (unsigned short)((x+1) + vertDim * y);     indices[index++] = (unsigned short)(x + vertDim * (y+1));
				assert( index <totalIndices ); 
			}
		}
		m_indexEndTR = index;

		// Bottom left part
		for( int y = halfd; y < fulld; y++ )
		{
			for( int x = 0; x < halfd; x++ )
			{
				indices[index++] = (unsigned short)((x+1) + vertDim * y);     indices[index++] = (unsigned short)(x + vertDim * y);         indices[index++] = (unsigned short)(x + vertDim * (y+1));
				indices[index++] = (unsigned short)((x+1) + vertDim * (y+1)); indices[index++] = (unsigned short)((x+1) + vertDim * y);     indices[index++] = (unsigned short)(x + vertDim * (y+1));
				assert( index <totalIndices ); 
			}
		}
		m_indexEndBL = index;

		// Bottom right part
		for( int y = halfd; y < fulld; y++ )
		{
			for( int x = halfd; x < fulld; x++ )
			{
				indices[index++] = (unsigned short)((x+1) + vertDim * y);     indices[index++] = (unsigned short)(x + vertDim * y);         indices[index++] = (unsigned short)(x + vertDim * (y+1));
				indices[index++] = (unsigned short)((x+1) + vertDim * (y+1)); indices[index++] = (unsigned short)((x+1) + vertDim * y);     indices[index++] = (unsigned short)(x + vertDim * (y+1));
				assert( index <= totalIndices ); 
			}
		}
		m_indexEndBR = index;

		// Set up the description of the index buffer.
		indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
		indexBufferDesc.ByteWidth = sizeof(unsigned short) * totalIndices;
		indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;

		// Give the subresource structure a pointer to the index data.
		indexData.pSysMem = indices;

		// Create the index buffer.
		HRESULT result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
		delete[] indices;

		if(FAILED(result))
		{
			return false;
		}

	}

	return S_OK;
}

