#pragma once

namespace FPSTimer
{

double getId();

double Initialize();
double SetResolution(double res);
double GetResolution();
double GetFrequency();
double AddMarker();
double GetMarker(double id);
double SetMarker(double id, double time, double rel);
double TimeMarker(double id);
double DeleteMarker(double id);
double TimeNow();
double DeltaTime();

}