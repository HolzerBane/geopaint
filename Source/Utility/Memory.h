#pragma once


namespace TerrainModel
{

class Memory
{
public:

	template< class TRes >
	static void Release( TRes* res )
	{
		if ( res )
		{
			res->Release();
			res = nullptr;
		}
	}
};
}