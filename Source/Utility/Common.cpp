//////////////////////////////////////////////////////////////////////
// Copyright (C) 2009 - Filip Strugar.
// Distributed under the zlib License (see readme.txt)
//////////////////////////////////////////////////////////////////////

#include "DXUT.h"

#include "Common.h"

//#include "DxEventNotifier.h"

//#include "DxShader.h"

#pragma warning ( disable : 4996 )

using namespace std;

//
void vaGetFrustumPlanes( D3DXPLANE * pPlanes, const D3DXMATRIX & mCameraViewProj )
{
   // Left clipping plane
   pPlanes[0].a = mCameraViewProj(0,3) + mCameraViewProj(0,0);
   pPlanes[0].b = mCameraViewProj(1,3) + mCameraViewProj(1,0);
   pPlanes[0].c = mCameraViewProj(2,3) + mCameraViewProj(2,0);
   pPlanes[0].d = mCameraViewProj(3,3) + mCameraViewProj(3,0);

   // Right clipping plane
   pPlanes[1].a = mCameraViewProj(0,3) - mCameraViewProj(0,0);
   pPlanes[1].b = mCameraViewProj(1,3) - mCameraViewProj(1,0);
   pPlanes[1].c = mCameraViewProj(2,3) - mCameraViewProj(2,0);
   pPlanes[1].d = mCameraViewProj(3,3) - mCameraViewProj(3,0);

   // Top clipping plane
   pPlanes[2].a = mCameraViewProj(0,3) - mCameraViewProj(0,1);
   pPlanes[2].b = mCameraViewProj(1,3) - mCameraViewProj(1,1);
   pPlanes[2].c = mCameraViewProj(2,3) - mCameraViewProj(2,1);
   pPlanes[2].d = mCameraViewProj(3,3) - mCameraViewProj(3,1);

   // Bottom clipping plane
   pPlanes[3].a = mCameraViewProj(0,3) + mCameraViewProj(0,1);
   pPlanes[3].b = mCameraViewProj(1,3) + mCameraViewProj(1,1);
   pPlanes[3].c = mCameraViewProj(2,3) + mCameraViewProj(2,1);
   pPlanes[3].d = mCameraViewProj(3,3) + mCameraViewProj(3,1);

   // Near clipping plane
   pPlanes[4].a = mCameraViewProj(0,2);
   pPlanes[4].b = mCameraViewProj(1,2);
   pPlanes[4].c = mCameraViewProj(2,2);
   pPlanes[4].d = mCameraViewProj(3,2);

   // Far clipping plane
   pPlanes[5].a = mCameraViewProj(0,3) - mCameraViewProj(0,2);
   pPlanes[5].b = mCameraViewProj(1,3) - mCameraViewProj(1,2);
   pPlanes[5].c = mCameraViewProj(2,3) - mCameraViewProj(2,2);
   pPlanes[5].d = mCameraViewProj(3,3) - mCameraViewProj(3,2);
   // Normalize the plane equations, if requested

   for (int i = 0; i < 6; i++) 
      D3DXPlaneNormalize( &pPlanes[i], &pPlanes[i] );
}

std::string vaStringSimpleNarrow( const std::wstring & s ) {
   std::string ws;
   ws.resize(s.size());
   for( size_t i = 0; i < s.size(); i++ ) ws[i] = (char)s[i];
   return ws;
}
//
std::wstring vaStringSimpleWiden( const std::string & s ) {
   std::wstring ws;
   ws.resize(s.size());
   for( size_t i = 0; i < s.size(); i++ ) ws[i] = s[i];
   return ws;
}