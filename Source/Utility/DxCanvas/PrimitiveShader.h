#pragma once

//////////////
// INCLUDES //
//////////////
#include <d3d10_1.h>
#include <d3dx10.h>

#include <fstream>

class PrimitiveShader
{
public:

	enum ShaderType
	{
		TwoD,
		ThreeD
	};

	PrimitiveShader();
	PrimitiveShader(const PrimitiveShader&);
	~PrimitiveShader();

	bool Initialize(ID3D10Device*, HWND);
	void Shutdown();
	void Render3D(
		ID3D10Device* device, 
		int indexCount, 
		D3DXMATRIX worldMatrix, 
		D3DXMATRIX viewMatrix,
		D3DXMATRIX projectionMatrix, 
		D3DXVECTOR4 pixelColor);
	
	void Render2D(
		ID3D10Device* device, 
		int indexCount, 
		D3DXVECTOR4 pixelColor);

private:
	bool InitializeShader(ID3D10Device*, HWND, const std::wstring&);
	void ShutdownShader();
	void OutputShaderErrorMessage(ID3D10Blob*, HWND, const std::wstring&);

	void SetShaderParameters(D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DXVECTOR4);
	void RenderShader(ID3D10Device*, const int, const ShaderType sType);

private:
	ID3D10Effect* m_effect;
	ID3D10EffectTechnique* m_technique3d;
	ID3D10EffectTechnique* m_technique2d;

	ID3D10InputLayout* m_layout2d;
	ID3D10InputLayout* m_layout3d;

	ID3D10EffectMatrixVariable* m_worldMatrixPtr;
	ID3D10EffectMatrixVariable* m_viewMatrixPtr;
	ID3D10EffectMatrixVariable* m_projectionMatrixPtr;
	ID3D10EffectVectorVariable* m_pixelColorPtr;
};
