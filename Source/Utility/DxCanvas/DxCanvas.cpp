#include "DXUT.h"
#include "SDKmisc.h"

//#include "CDLOD\Common.h"

#include <stdio.h>
#include <stdarg.h>
#include <string>
#include <vector>

#include "DxCanvas.h"
#include "PrimitiveShader.h"
#include "TerrainEngine\d3dclass.h" // take this out
//#include "DXUTHUDHelper.h"
#include "..\Common.h"

using namespace std;

static D3DCOLORVALUE U32_TO_D3DCOLORVALUE( unsigned int c )
{
	D3DCOLORVALUE cv = { ( ( c >> 16 ) & 0xFF ) / 255.0f,
		( ( c >> 8 ) & 0xFF ) / 255.0f,
		( c & 0xFF ) / 255.0f,
		( ( c >> 24 ) & 0xFF ) / 255.0f };
	return cv;
}

static D3DXVECTOR4 U32_TO_D3DXVECTOR4( unsigned int c )
{
	D3DXVECTOR4 v( ( ( c >> 16 ) & 0xFF ) / 255.0f,
		( ( c >> 8 ) & 0xFF ) / 255.0f,
		( c & 0xFF ) / 255.0f,
		( ( c >> 24 ) & 0xFF ) / 255.0f );

	return v;
}


class Canvas2DDX10 : public ICanvas2D
{
	struct DrawStringItem
	{
		int         x, y;
		unsigned int penColor;
		wstring      text;
		DrawStringItem( int x, int y, unsigned int penColor, const wstring & text ) : x(x), y(y), penColor(penColor), text(text) {}
	};
	
	struct DrawLineItem
	{
		float x0;
		float y0;
		float x1;
		float y1;
		unsigned int penColor;

		DrawLineItem( float x0, float y0, float x1, float y1, unsigned int penColor )
			: x0(x0), y0(y0), x1(x1), y1(y1), penColor(penColor) { }
	};

	struct ColoredVertex
	{
		float x,y,z,w;
		DWORD    diffuse;
		ColoredVertex( float x, float y, float z, float rhw, DWORD diffuse )   
			: x(x), y(y), z(z), w(rhw), diffuse( diffuse )   { }

		ColoredVertex() 
		{ }

	};

public:

	Canvas2DDX10()
		: m_rasterizerState(nullptr)
		, m_blendState(nullptr)
		, m_lineHeight(10)
	{ }

	virtual void DrawString( const char * text, ... )
	{
		DrawString(10, m_actualLine * m_lineHeight, text );
		m_actualLine++;
	}

	virtual void DrawString( int x, int y, const wchar_t * text, ... )
	{
		va_list args;
		va_start(args, text);

		int nBuf;
		wchar_t szBuffer[1024];

		nBuf = _vsnwprintf(szBuffer, sizeof(szBuffer) / sizeof(wchar_t), text, args);
		assert(nBuf < sizeof(szBuffer));//Output truncated as it was > sizeof(szBuffer)

		va_end(args);

		m_DrawStringLines.push_back( DrawStringItem(x, y, 0xFFFFFF00, wstring(szBuffer) ) );      
	}

	virtual void         DrawString( int x, int y, unsigned int penColor, const wchar_t * text, ... )
	{
		va_list args;
		va_start(args, text);

		int nBuf;
		wchar_t szBuffer[1024];

		nBuf = _vsnwprintf(szBuffer, sizeof(szBuffer) / sizeof(wchar_t), text, args);
		assert(nBuf < sizeof(szBuffer));//Output truncated as it was > sizeof(szBuffer)

		va_end(args);

		m_DrawStringLines.push_back( DrawStringItem(x, y, penColor, wstring(szBuffer) ) );
	}
	
	virtual void DrawString( int x, int y, const char * text, ... )
	{
		va_list args;
		va_start(args, text);

		int nBuf;
		char szBuffer[1024];

		nBuf = _vsnprintf(szBuffer, sizeof(szBuffer) / sizeof(char), text, args);
		assert(nBuf < sizeof(szBuffer));//Output truncated as it was > sizeof(szBuffer)

		va_end(args);

		m_DrawStringLines.push_back( DrawStringItem(x, y, 0xFFFFFF00, vaStringSimpleWiden( string(szBuffer) ) ) );      
	}

	virtual void DrawString( int x, int y, unsigned int penColor, const char * text, ... )
	{
		va_list args;
		va_start(args, text);

		int nBuf;
		char szBuffer[1024];

		nBuf = _vsnprintf(szBuffer, sizeof(szBuffer) / sizeof(char), text, args);
		assert(nBuf < sizeof(szBuffer));//Output truncated as it was > sizeof(szBuffer)

		va_end(args);

		m_DrawStringLines.push_back( DrawStringItem(x, y, penColor, vaStringSimpleWiden( string(szBuffer) ) ) );
	}
	
	virtual void DrawLine( float x0, float y0, float x1, float y1, unsigned int penColor )
	{
		m_drawLines.push_back( DrawLineItem( x0, y0, x1, y1, penColor ) );
	}
	
	virtual void DrawRectangle( float x0, float y0, float width, float height, unsigned int penColor )
	{
		DrawLine( x0, y0, x0 + width, y0, penColor );
		DrawLine( x0 + width, y0, x0 + width, y0 + height, penColor );
		DrawLine( x0 + width, y0 + height, x0, y0 + height, penColor );
		DrawLine( x0, y0 + height, x0, y0, penColor );
	}
	
protected:
	virtual HRESULT   OnResetDevice(const D3DSURFACE_DESC* pBackBufferSurfaceDesc)
	{ 
		m_backBufferSurfaceDesc = *pBackBufferSurfaceDesc;
		return S_OK; 
	}

public:
	void CleanQueued()
	{
		m_drawLines.clear();
		m_DrawStringLines.clear();
	}
	
	void Initialize( ID3D10Device* device )
	{
		m_primitiveShader.Initialize( device, nullptr );

		m_vertexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
		m_vertexBufferDesc.ByteWidth = sizeof( ColoredVertex);
		m_vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
		m_vertexBufferDesc.CPUAccessFlags = 0;
		m_vertexBufferDesc.MiscFlags = 0;
		
		m_indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
		m_indexBufferDesc.ByteWidth = sizeof(unsigned short);
		m_indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
		m_indexBufferDesc.CPUAccessFlags = 0;
		m_indexBufferDesc.MiscFlags = 0;
	}
	
	void Deinitialize()
	{
	}
	
	void Draw( D3DClass& deviceWrapper, CDXUTTextHelper& textHelper  )
	{
		m_actualLine = 0;

		textHelper.Begin();
		textHelper.SetInsertionPos( 5, 5 );
		textHelper.SetForegroundColor( D3DXCOLOR( 1.0f, 1.0f, 0.0f, 1.0f ) );
		textHelper.DrawTextLine( DXUTGetFrameStats( DXUTIsVsyncEnabled() ) );
		textHelper.DrawTextLine( DXUTGetDeviceStats() );

		textHelper.SetForegroundColor( D3DXCOLOR( 0.0f, 0.0f, 0.0f, 0.6f ) );

		// horrifying shadow effect
		for( size_t i = 0; i < m_DrawStringLines.size(); i++ )
		{
			textHelper.SetInsertionPos( m_DrawStringLines[i].x+1, m_DrawStringLines[i].y+1 );
			textHelper.DrawTextLine( m_DrawStringLines[i].text.c_str() );
		}

		for( size_t i = 0; i < m_DrawStringLines.size(); i++ )
		{
			textHelper.SetInsertionPos( m_DrawStringLines[i].x, m_DrawStringLines[i].y );
			textHelper.SetForegroundColor( D3DXCOLOR( m_DrawStringLines[i].penColor ) );
			textHelper.DrawTextLine( m_DrawStringLines[i].text.c_str() );
		}

		textHelper.End();

		m_DrawStringLines.clear();

		if (m_drawLines.size() == 0 )
		{
			return;
		}

		// Buffers //
		D3D10_SUBRESOURCE_DATA vertexData, indexData;
		ID3D10Buffer *vertexBuffer, *indexBuffer;

		ColoredVertex*	vertices;
		unsigned short* indices;

		const unsigned stride = sizeof(ColoredVertex); 
		const unsigned offset = 0;
		// Buffers end //

		deviceWrapper.TurnZBufferOff();
		deviceWrapper.EnableAlphaBlending();
		deviceWrapper.TurnZBufferOff();
		ID3D10Device* device = deviceWrapper.GetDevice();

		{
			const unsigned short vertexCount = m_drawLines.size() * 2;
			const unsigned short indexCount  = m_drawLines.size();

			vertices = new ColoredVertex[vertexCount];
			indices = new unsigned short[indexCount];

			for( size_t i = 0; i < m_drawLines.size(); i++ )
			{
				indices[i]		= i;
				vertices[i++]	= ColoredVertex( m_drawLines[i].x0, m_drawLines[i].y0, 0.0f, 0.5f, m_drawLines[i].penColor );
				indices[i]		= i;				
				vertices[i]		= ColoredVertex( m_drawLines[i].x1, m_drawLines[i].y1, 0.0f, 0.5f, m_drawLines[i].penColor );
			}
			// TODO: sort by color... or use vertex input. even better
			vertexData.pSysMem = vertices;
			indexData.pSysMem = indices;
			HRESULT result = device->CreateBuffer(&m_vertexBufferDesc, &vertexData, &vertexBuffer);
			result = device->CreateBuffer(&m_indexBufferDesc, &indexData, &indexBuffer);
			device->IASetIndexBuffer( indexBuffer, DXGI_FORMAT_R16_UINT, 0 );
			device->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);

			m_primitiveShader.Render2D( device, indexCount, U32_TO_D3DXVECTOR4(m_drawLines[0].penColor));

			delete[] vertices;
			delete[] indices;

		}

		deviceWrapper.EnableCulling();
		deviceWrapper.TurnZBufferOn();

		m_drawLines.clear( );
	}
	
	virtual int GetWidth( )
	{
		return m_backBufferSurfaceDesc.Width;
	}
	
	virtual int GetHeight( )
	{
		return m_backBufferSurfaceDesc.Height;
	}

public:	
	vector<DrawStringItem>  m_DrawStringLines;
	vector<DrawLineItem>    m_drawLines;
	
	D3DSURFACE_DESC         m_backBufferSurfaceDesc;
	ID3D10RasterizerState*	m_rasterizerState;
	ID3D10BlendState*		m_blendState;
	ID3D10DepthStencilState*m_depthStencilState;
	PrimitiveShader			m_primitiveShader;
	int						m_lineHeight;
	int						m_actualLine;
	D3D10_BUFFER_DESC		m_vertexBufferDesc;
	D3D10_BUFFER_DESC		m_indexBufferDesc;
};

class Canvas3DDX10 : public ICanvas3D
{
	enum DrawItemType
	{
		Triangle,
		Box,
	};
	
	struct DrawItem
	{
			D3DXVECTOR3     v0;
			D3DXVECTOR3     v1;
			D3DXVECTOR3     v2;
			unsigned int    penColor;
			unsigned int    brushColor;

			unsigned int	indexCount;	
		
		DrawItemType	type;
		DrawItem( const D3DXVECTOR3 & v0, const D3DXVECTOR3 & v1, const D3DXVECTOR3 & v2, unsigned int penColor, unsigned int brushColor, DrawItemType type ) 
			: v0(v0), v1(v1), v2(v2), penColor(penColor), brushColor(brushColor), type(type) { }
		
		void Render( ID3D10Device* device )
		{
			D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
			D3D10_SUBRESOURCE_DATA vertexData, indexData;
			ID3D10Buffer *vertexBuffer, *indexBuffer;

			D3DXVECTOR3*	vertices;
			unsigned short* indices;
			
			const unsigned stride = sizeof(D3DXVECTOR3); 
			const unsigned offset = 0;

			vertexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
			vertexBufferDesc.ByteWidth = sizeof( D3DXVECTOR3);
			vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
			vertexBufferDesc.CPUAccessFlags = 0;
			vertexBufferDesc.MiscFlags = 0;

			indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
			indexBufferDesc.ByteWidth = sizeof(unsigned short);
			indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
			indexBufferDesc.CPUAccessFlags = 0;
			indexBufferDesc.MiscFlags = 0;

			if( type == Triangle )
			{
				
				if( (penColor & 0xFF000000) != 0 )
				{
					const unsigned vs = 3; 
					indexCount = 6;

					vertices = new D3DXVECTOR3[vs];
					vertices[0] = v0;
					vertices[1] = v1;
					vertices[2] = v2;
					indices = new unsigned short[indexCount];
					indices[0] = 0;
					indices[1] = 1;
					indices[2] = 1;
					indices[3] = 2;
					indices[4] = 2;
					indices[5] = 0;

					vertexBufferDesc.ByteWidth *= vs;
					indexBufferDesc.ByteWidth  *= indexCount;

					device->IASetPrimitiveTopology( D3D10_PRIMITIVE_TOPOLOGY_LINELIST );
				}
			}
			else
			if( type == Box )
			{

				const D3DXVECTOR3 & boxMin = v0;
				const D3DXVECTOR3 & boxMax = v1;

				D3DXVECTOR3 a0(boxMin.x, boxMin.y, boxMin.z);
				D3DXVECTOR3 a1(boxMax.x, boxMin.y, boxMin.z);
				D3DXVECTOR3 a2(boxMax.x, boxMax.y, boxMin.z);
				D3DXVECTOR3 a3(boxMin.x, boxMax.y, boxMin.z);
				D3DXVECTOR3 b0(boxMin.x, boxMin.y, boxMax.z);
				D3DXVECTOR3 b1(boxMax.x, boxMin.y, boxMax.z);
				D3DXVECTOR3 b2(boxMax.x, boxMax.y, boxMax.z);
				D3DXVECTOR3 b3(boxMin.x, boxMax.y, boxMax.z);

				
				if( (penColor & 0xFF000000) != 0 )
				{
					const unsigned vs = 24;
					indexCount = 24;

					vertices = new D3DXVECTOR3[vs];
					indices = new unsigned short[indexCount];

					int index = 0;				indices[index] = index;

					vertices[index++] = a0;		indices[index] = index;
					vertices[index++] = a1;		indices[index] = index;
					vertices[index++] = a1;		indices[index] = index;
					vertices[index++] = a2;		indices[index] = index;
					vertices[index++] = a2;		indices[index] = index;
					vertices[index++] = a3;		indices[index] = index;
					vertices[index++] = a3;		indices[index] = index;
					vertices[index++] = a0;		indices[index] = index;
					vertices[index++] = a0;		indices[index] = index;
					vertices[index++] = b0;		indices[index] = index;
					vertices[index++] = a1;		indices[index] = index;
					vertices[index++] = b1;		indices[index] = index;
					vertices[index++] = a2;		indices[index] = index;
					vertices[index++] = b2;		indices[index] = index;
					vertices[index++] = a3;		indices[index] = index;
					vertices[index++] = b3;		indices[index] = index;
					vertices[index++] = b0;		indices[index] = index;
					vertices[index++] = b1;		indices[index] = index;
					vertices[index++] = b1;		indices[index] = index;
					vertices[index++] = b2;		indices[index] = index;
					vertices[index++] = b2;		indices[index] = index;
					vertices[index++] = b3;		indices[index] = index;
					vertices[index++] = b3;		indices[index] = index;
					vertices[index++] = b0;		

					vertexBufferDesc.ByteWidth *= vs;
					indexBufferDesc.ByteWidth  *= indexCount;

					device->IASetPrimitiveTopology( D3D10_PRIMITIVE_TOPOLOGY_LINELIST );
				}
			}

			vertexData.pSysMem = vertices;
			indexData.pSysMem = indices;
			HRESULT result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &vertexBuffer);
			result = device->CreateBuffer(&indexBufferDesc, &indexData, &indexBuffer);
			device->IASetIndexBuffer( indexBuffer, DXGI_FORMAT_R16_UINT, 0 );
			device->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		
			// something new had to happen!
			delete[] indices;
			delete[] vertices;
		}
	};

	vector<DrawItem>			m_drawItems;
	ID3D10RasterizerState*		m_rasterizerState;
	ID3D10BlendState*			m_blendState;
	PrimitiveShader				m_primitiveShader;
	
public:
	Canvas3DDX10()
	{ }
	
	void Initialize( ID3D10Device* device)
	{
		m_primitiveShader.Initialize( device, nullptr );
	}
	
	void Deinitialize()
	{
	}
	
	void Render( D3DClass& deviceWrapper, const D3DXMATRIX& viewMatrix, const D3DXMATRIX & projMatrix )
	{

		if ( m_drawItems.size() == 0 )
		{
			return;
		}

		ID3D10Device* device = deviceWrapper.GetDevice(); 

		D3DXMATRIX world;
		D3DXMatrixIdentity( &world ); // this is the world matrix.. as long as it is the identity...

		deviceWrapper.DisableCulling();
		deviceWrapper.EnableAlphaBlending(); // is this needed?
		for( size_t i = 0; i < m_drawItems.size(); i++ )
		{
			DrawItem & item = m_drawItems[i];
			item.Render( device );
			m_primitiveShader.Render3D( device, item.indexCount, world, viewMatrix, projMatrix,  U32_TO_D3DXVECTOR4( item.penColor ));
		}

		deviceWrapper.EnableCulling();

		m_drawItems.erase(m_drawItems.begin(), m_drawItems.end());
	}

	
	virtual void   DrawBox( const D3DXVECTOR3 & v0, const D3DXVECTOR3 & v1, unsigned int penColor, unsigned int brushColor, const D3DXMATRIX * transform )
	{
		assert( transform == NULL );
		m_drawItems.push_back( DrawItem( v0, v1, D3DXVECTOR3(0,0,0), penColor, brushColor, Box ) );
	}

	virtual void   DrawTriangle( const D3DXVECTOR3 & v0, const D3DXVECTOR3 & v1, const D3DXVECTOR3 & v2, unsigned int penColor, unsigned int brushColor, const D3DXMATRIX * transform )
	{
		assert( transform == NULL );
		m_drawItems.push_back( DrawItem( v0, v1, v2, penColor, brushColor, Triangle ) );
	}
	
	virtual void   DrawQuad( const D3DXVECTOR3 & v0, const D3DXVECTOR3 & v1, const D3DXVECTOR3 & v2, const D3DXVECTOR3 & v3, unsigned int penColor, unsigned int brushColor, const D3DXMATRIX * transform )
	{
		DrawTriangle(v0, v1, v2, penColor, brushColor, transform);
		DrawTriangle(v2, v1, v3, penColor, brushColor, transform);
	}
	
};
 
Canvas2DDX10 g_Canvas2D;
Canvas3DDX10 g_Canvas3D;

ICanvas2D* DxCanvas::GetCanvas2D()
{
	return &g_Canvas2D;
}

ICanvas3D* DxCanvas::GetCanvas3D()
{
	return &g_Canvas3D;
}

void DxCanvas::vaDrawCanvas2D( D3DClass& deviceWrapper, CDXUTTextHelper& textHelper )
{
	g_Canvas2D.Draw( deviceWrapper, textHelper );
}

void DxCanvas::vaDrawCanvas3D( D3DClass& deviceWrapper, const D3DXMATRIX& viewMatrix, const D3DXMATRIX& projMatrix )
{
	g_Canvas3D.Render( deviceWrapper, viewMatrix, projMatrix );
}
