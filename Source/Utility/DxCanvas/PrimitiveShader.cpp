////////////////////////////////////////////////////////////////////////////////
// Filename: PrimitiveShader.cpp
////////////////////////////////////////////////////////////////////////////////
#include "PrimitiveShader.h"
#include "..\ReportError.h"
#include "..\..\TerrainEngine\Paths.h"
#include "..\File.h"
#include <sstream>
#include <assert.h>

PrimitiveShader::PrimitiveShader()
{
	m_effect = 0;
	m_technique2d = 0;
	m_technique3d = 0;
	m_layout2d = 0;
	m_layout3d = 0;

	m_worldMatrixPtr = 0;
	m_viewMatrixPtr = 0;
	m_projectionMatrixPtr = 0;
	m_pixelColorPtr = 0;
}


PrimitiveShader::PrimitiveShader(const PrimitiveShader& other)
{
}


PrimitiveShader::~PrimitiveShader()
{
}


bool PrimitiveShader::Initialize(ID3D10Device* device, HWND hwnd)
{
	bool result;


	// Initialize the shader that will be used to draw the triangle.
	result = InitializeShader(device, hwnd, Paths::Shaders + L"primitiveShader.hlsl_o");
	if(!result)
	{
		ReportError::Instance().CheckError();
		return false;
	}

	return true;
}


void PrimitiveShader::Shutdown()
{
	// Shutdown the shader effect.
	ShutdownShader();

	return;
}


void PrimitiveShader::Render3D(ID3D10Device* device, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
							 D3DXMATRIX projectionMatrix, D3DXVECTOR4 pixelColor)
{
	// Set the shader parameters that it will use for rendering.
	SetShaderParameters(worldMatrix, viewMatrix, projectionMatrix, pixelColor);

	// Now render the prepared buffers with the shader.
	RenderShader(device, indexCount, ThreeD);

	return;
}

void PrimitiveShader::Render2D(ID3D10Device* device, int indexCount, D3DXVECTOR4 pixelColor)
{
	// Set the pixel color variable inside the shader with the pixelColor vector.
	m_pixelColorPtr->SetFloatVector((float*)&pixelColor);

	// Now render the prepared buffers with the shader.
	RenderShader(device, indexCount, TwoD );

	return;
}


bool PrimitiveShader::InitializeShader(ID3D10Device* device, HWND hwnd, const std::wstring& filename)
{
	HRESULT result;
	ID3D10Blob* errorMessage;
	D3D10_INPUT_ELEMENT_DESC polygonLayout[2];
	unsigned int numElements;
    D3D10_PASS_DESC passDesc;


	// Initialize the error message.
	errorMessage = 0;

	// Load the shader in from the file.
	//result = D3DX10CreateEffectFromFile(filename, NULL, NULL, "fx_4_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, 
	//									device, NULL, NULL, &m_effect, &errorMessage, NULL);
	
	ID3D10Blob* blob;
	bool ok = File::readBLOBFromFile( filename, &blob);
	assert( ok && (std::wstring(L"File not found!") + filename).c_str() );
	result = D3D10CreateEffectFromMemory( blob->GetBufferPointer(), blob->GetBufferSize(), 0, device, nullptr, &m_effect );

	if(FAILED(result))
	{
		// If the shader failed to compile it should have writen something to the error message.
		if(errorMessage)
		{

			OutputShaderErrorMessage(errorMessage, hwnd, filename);
		}
		// If there was  nothing in the error message then it simply could not find the shader file itself.
		else
		{
			MessageBox(hwnd, filename.c_str(), L"Missing Shader File", MB_OK);
		}

		return false;
	}

	// Get a pointer to the technique inside the shader.
	m_technique3d = m_effect->GetTechniqueByName("Technique3D");
	m_technique2d = m_effect->GetTechniqueByName("Technique2D");
	if(!m_technique3d || !m_technique2d)
	{
		return false;
	}

	// Now setup the layout of the data that goes into the shader.
	// This setup needs to match the VertexType stucture in the ModelClass and in the shader.
	// watch out... check the whole function when changing the polygonLayout
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D10_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D10_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D10_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	// Get a count of the elements in the layout.
    numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	// Get the description of the first pass described in the shader technique.
    m_technique3d->GetPassByIndex(0)->GetDesc(&passDesc);

	// Create the input layout.
    result = device->CreateInputLayout(polygonLayout, 2, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, 
									   &m_layout3d);
	if(FAILED(result))
	{
		ReportError::Instance().CheckError();
		return false;
	}
	m_technique2d->GetPassByIndex(0)->GetDesc(&passDesc);

	// Create the input layout.
    result = device->CreateInputLayout(polygonLayout, 1, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, 
									   &m_layout2d);

	if(FAILED(result))
	{
		ReportError::Instance().CheckError();
		return false;
	}


	// Get pointers to the three matrices inside the shader so we can update them from this class.
    m_worldMatrixPtr = m_effect->GetVariableByName("worldMatrix")->AsMatrix();
	m_viewMatrixPtr = m_effect->GetVariableByName("viewMatrix")->AsMatrix();
    m_projectionMatrixPtr = m_effect->GetVariableByName("projectionMatrix")->AsMatrix();

	// Get pointer to the pixel color variable inside the shader.
	m_pixelColorPtr = m_effect->GetVariableByName("pixelColor")->AsVector();

	return true;
}


void PrimitiveShader::ShutdownShader()
{
	// Release the pointer to the pixel color of the font.
	m_pixelColorPtr = 0;

	// Release the pointers to the matrices inside the shader.
	m_worldMatrixPtr = 0;
	m_viewMatrixPtr = 0;
	m_projectionMatrixPtr = 0;

	// Release the pointer to the shader layout.
	if(m_layout2d)
	{
		m_layout2d->Release();
		m_layout2d = 0;
	}

	if(m_layout3d)
	{
		m_layout3d->Release();
		m_layout3d = 0;
	}

	// Release the pointer to the shader technique.
	m_technique2d = 0;
	m_technique3d = 0;

	// Release the pointer to the shader.
	if(m_effect)
	{
		m_effect->Release();
		m_effect = 0;
	}

	return;
}


void PrimitiveShader::OutputShaderErrorMessage(ID3D10Blob* errorMessage, HWND hwnd,const std::wstring& shaderFilename)
{
	char* compileErrors;
	unsigned long bufferSize, i;
	std::ofstream fout;


	// Get a pointer to the error message text buffer.
	compileErrors = (char*)(errorMessage->GetBufferPointer());

	// Get the length of the message.
	bufferSize = errorMessage->GetBufferSize();

	// Open a file to write the error message to.
	fout.open("shader-error.txt");

	// Write out the error message.
	for(i=0; i<bufferSize; i++)
	{
		fout << compileErrors[i];
	}

	std::string message( compileErrors, bufferSize );
	// Close the file.
	fout.close();

	// Release the error message.
	errorMessage->Release();
	errorMessage = 0;

	// Pop a message up on the screen to notify the user to check the text file for compile errors.
	std::wstringstream ws;
	ws << L"Error compiling shader.  Check shader-error.txt for message.\n";
	ws << L"Or read this:\n";
	ws << message.c_str();

	MessageBox(hwnd, ws.str().c_str(), L"Error Compiling Shader", MB_OK);

	return;
}


void PrimitiveShader::SetShaderParameters(D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,
										  D3DXVECTOR4 pixelColor)
{
	// Set the world matrix variable inside the shader.
    m_worldMatrixPtr->SetMatrix((float*)&worldMatrix);

	// Set the view matrix variable inside the shader.
	m_viewMatrixPtr->SetMatrix((float*)&viewMatrix);

	// Set the projection matrix variable inside the shader.
    m_projectionMatrixPtr->SetMatrix((float*)&projectionMatrix);

	// Set the pixel color variable inside the shader with the pixelColor vector.
	m_pixelColorPtr->SetFloatVector((float*)&pixelColor);

	return;
}


void PrimitiveShader::RenderShader(ID3D10Device* device, const int indexCount, const ShaderType sType)
{
    D3D10_TECHNIQUE_DESC techniqueDesc;
	unsigned int i;
	
	ID3D10EffectTechnique* technique;
	ID3D10InputLayout* layout;

	switch(sType)
	{
		case TwoD: 
			technique	= m_technique2d;
			layout		= m_layout2d;
		break;

		case ThreeD: 
			technique	= m_technique3d;		
			layout		= m_layout3d;
		break;
	}

	// Set the input layout.
	device->IASetInputLayout(layout);

	// Get the description structure of the technique from inside the shader so it can be used for rendering.
    technique->GetDesc(&techniqueDesc);

    // Go through each pass in the technique (should be just one currently) and renders the triangles.
	for(i=0; i<techniqueDesc.Passes; ++i)
    {
        technique->GetPassByIndex(i)->Apply(0);
        device->DrawIndexed(indexCount, 0, 0);
    }

	return;
}