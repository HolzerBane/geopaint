
/////////////
// GLOBALS //
/////////////
matrix worldMatrix;
matrix viewMatrix;
matrix projectionMatrix;
Texture2D shaderTexture;
float4 pixelColor;


///////////////////
// SAMPLE STATES //
///////////////////
SamplerState SampleType
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};


/////////////////////
// BLENDING STATES //
/////////////////////
BlendState AlphaBlendingState
{
	BlendEnable[0] = TRUE;
	DestBlend = INV_SRC_ALPHA;
};


//////////////
// TYPEDEFS //
//////////////
struct VertexInputType
{
    float3 position : POSITION;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
};

struct VertexInputTexType
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
};

struct PixelInputTexType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
};


////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelInputTexType PrimitiveVertexTexShader(VertexInputTexType input)
{
    PixelInputTexType output;
    
    
	// Change the position vector to be 4 units for proper matrix calculations.
	// Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(input.position, worldMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);
    
	// Store the texture coordinates for the pixel shader.
    output.tex = input.tex;
    
	return output;
}

PixelInputType PrimitiveVertexShader(VertexInputType input)
{
    PixelInputType output;
    
    
	// Change the position vector to be 4 units for proper matrix calculations.
    float4 positionw = float4(input.position, 1.0f);

	// Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(positionw, worldMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);
    
	return output;
}

PixelInputType PrimitiveVertexShader2D(VertexInputType input)
{
    PixelInputType output;    
    output.position = float4(input.position, 1.0f);

	return output;
}


////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 PrimitivePixelTexShader(PixelInputTexType input) : SV_Target
{
	float4 color;
	
	// Sample the texture pixel at this location.
	color = shaderTexture.Sample(SampleType, input.tex);
	
	// If the color is black on the texture then treat this pixel as transparent.
	if(color.r == 0.0f)
	{
		color.a = 0.0f;
	}
	
	// If the color is other than black on the texture then this is a pixel in the font so draw it using the font pixel color.
	else
	{
		color.a = 1.0f;
		color = color * pixelColor;
	}

    return color;
}

float4 PrimitivePixelShader(PixelInputType input) : SV_Target
{
    return pixelColor;
}

////////////////////////////////////////////////////////////////////////////////
// States
////////////////////////////////////////////////////////////////////////////////
RasterizerState SolidRS
{
    FillMode = Solid;
	MultisampleEnable = TRUE;
};

RasterizerState WireRS
{
    FillMode = Wireframe;
	MultisampleEnable = TRUE;
};

////////////////////////////////////////////////////////////////////////////////
// Technique
////////////////////////////////////////////////////////////////////////////////
technique10 Technique3D
{
    pass pass0
    {
   		SetBlendState(AlphaBlendingState, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
        SetVertexShader(CompileShader(vs_4_0, PrimitiveVertexShader()));
        SetPixelShader(CompileShader(ps_4_0, PrimitivePixelShader()));
        SetGeometryShader(NULL);
		SetRasterizerState(SolidRS);
    }
}

technique10 Technique2D
{
    pass pass0
    {
   		SetBlendState(AlphaBlendingState, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
        SetVertexShader(CompileShader(vs_4_0, PrimitiveVertexShader2D()));
        SetPixelShader(CompileShader(ps_4_0, PrimitivePixelShader()));
        SetGeometryShader(NULL);
		SetRasterizerState(WireRS);
    }
}