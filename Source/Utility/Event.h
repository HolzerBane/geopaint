#pragma once
#include <functional>
#include <algorithm>
#include <vector>

struct Empty
{};

// very lazy event handling...
template< class TObject = Empty >
class Event
{
public:
		typedef std::function< void(TObject) >  Delegate;
		typedef std::vector< Delegate >			Subscribers;

	void operator+=( const Delegate& d )
	{
		m_subscribers.push_back(d);
	}

	void operator()(const TObject& params )
	{
		std::for_each( m_subscribers.begin(), m_subscribers.end(), 
			[&params](const Delegate& e) { e(params); }
		);
	}

private:
	Subscribers	m_subscribers;
};