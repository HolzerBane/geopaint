////////////////////////////////////////////////////////////////////////////////
// Filename: main.cpp
////////////////////////////////////////////////////////////////////////////////
#include "DXUT/Core/DXUT.h"
#include "DXUT/Core/DXUTmisc.h"
#include "DXUT/optional/DXUTcamera.h"
#define CONSOLE_ON
#include "DebugConsole.h"

#include "systemclass.h"
#include "..\WinGUI\WinGuiFactory.h"
#include "..\WinGUI\IWinGUI.h"
#include "..\WinGUI\resource.h"
#include "..\winGUI\NewTerrainReturnStruct.h"

#pragma comment(linker, \
  "\"/manifestdependency:type='Win32' "\
  "name='Microsoft.Windows.Common-Controls' "\
  "version='6.0.0.0' "\
  "processorArchitecture='*' "\
  "publicKeyToken='6595b64144ccf1df' "\
  "language='*'\"")

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{

	SystemClass* System;
	bool result;
	RedirectIOToConsole( false, true );

	HRSRC hSrc = NULL;
	HMODULE lib =::LoadLibraryW(L"WinGUI.dll");
	HMODULE hMod = GetModuleHandle(L"WinGUI.dll");
	hSrc = ::FindResourceW(hMod, MAKEINTRESOURCE(IDD_DIALOG1), MAKEINTRESOURCE(RT_DIALOG));

	::FindResourceW(nullptr, MAKEINTRESOURCE(101), MAKEINTRESOURCE(RT_DIALOG) );
	IWinGUI* winGui = WinGuiFactory::CreateWinGUI();

	
	DXUTInit( true, true ); // Parse the command line and show msgboxes

	// Create the system object.
	System = new SystemClass;
	if(!System)
	{
		return 0;
	}

	// Initialize and run the system object.
	result = System->Initialize();
	if(result)
	{
		System->Run();
	}

	// Shutdown and release the system object.
	System->Shutdown();
	delete System;
	System = 0;

	return 0;
}